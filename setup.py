#!/usr/bin/python3
#
# Copyright (c) 2018 Thomas Weber <pokehaxco@gmail.com>
#
# SPDX-License-Identifier: MIT
#

from setuptools import setup

setup(
    name='uitransform',
    version='0',
    author='Thomas Weber',
    author_email='pokehaxco@gmail.com',
    license='MIT',

    packages=['uitransform'],
    classifiers=[
        'Development Status :: 3 - Alpha',
        'Environment :: Console',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: MIT License',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
        'Topic :: Software Development :: Code Generators'
    ]
)
