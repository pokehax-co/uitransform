#!/usr/bin/python3
#
# Copyright (c) 2017-2018 Thomas Weber <pokehaxco@gmail.com>
#
# SPDX-License-Identifier: MIT
#

import argparse
import errno
import functools
import multiprocessing
import os
import sys
import shutil
import time

from . import inputparsers
from . import transformations
from . import generators
from . import configurables as config
from .tools import log_error, log_warning, log_note


class PerformanceObserver(object):
    """
    This class stores the time required for the three stages of the
    transformation process for each input layout.
    """

    def __init__(self):
        self._dict = {}

    def _dict_ident(self, input_file, name):
        return "%s@PERFOBSV@%s" % (input_file, name)

    def new_layout(self, input_file, name):
        key = self._dict_ident(input_file, name)
        self._dict[key] = 3 * [-1.0]

    def record_analysis_time(self, input_file, name, time):
        """
        Records the time (in milliseconds) required for parsing the layout from
        a description file (this is identical for all layouts from the same
        input).
        """

        assert time >= 0.0
        key = self._dict_ident(input_file, name)
        self._dict[key][0] = time

    def record_transformation_time(self, input_file, name, time):
        """
        Records the time (in milliseconds) required for transforming the layout
        from legacy to grid.
        """

        assert time >= 0.0
        key = self._dict_ident(input_file, name)
        self._dict[key][1] = time

    def record_generator_time(self, input_file, name, time):
        """
        Records the time (in milliseconds) required for generating the HTML
        code for the grid layout.
        """

        assert time >= 0.0
        key = self._dict_ident(input_file, name)
        self._dict[key][2] = time

    def export_all(self):
        """
        Exports the list of recorded times to CSV format with semicolon as
        column separators ("European CSV") to standard output.
        """

        sys.stdout.write("======== PERFORMANCE MEASUREMENT ========\n")
        for key, times in self._dict.items():
            s = key.replace("@PERFOBSV@", ";")
            sys.stdout.write(
                "%s;%f;%f;%f\n" % (s, times[0], times[1], times[2])
            )


class UITransform(object):
    """
    This class implements the command-line frontend of UITransform. It accepts
    a source directory containing supported descriptions of legacy layout
    instances, and an output directory in which generated grid layout instances
    are saved as HTML files.
    """

    #
    # These strings can be quite long and are therefore separated from main()
    #
    UITRANSFORM_DESCRIPTION = (
        "Transforms legacy layouts into HTML grid layouts through "
        "optimization."
    )
    ARG_SOURCE_DIR_HELP = (
        "Directory where supported legacy layout descriptions are found"
    )
    ARG_DEST_DIR_HELP = (
        "Directory where generated HTML grid layouts are saved to"
    )
    ARG_SEQUENTIAL_HELP = (
        "Disables parallel processing of legacy layouts (only one layout will "
        "be processed at a time), for debugging only"
    )
    ARG_NATIVE_HELP = (
        "Uses the experimental, fast optimizer written in C instead of the "
        "feature-complete, slow optimizer written in Python"
    )
    ARG_TARGET_HELP = (
        "Optimizes layouts against this target function (multiple functions "
        "can be defined using multiple --target options)"
    )
    ARG_EXPORT_BENCHMARKS_HELP = (
        "Prints a CSV table of benchmark values after all inputs have been "
        "transformed"
    )
    ARG_ITERATIONS_HELP = (
        "Number of iterations to perform in the optimization"
    )

    #
    # Dictionary to translate function names to class types
    #
    SUPPORTED_TARGET_FUNCTIONS = {
        'whitespace': transformations.WhitespaceRatioMetric,
        'density-herf': transformations.HerfindahlDensityMetric,
        'density-gini': transformations.GiniDensityMetric,
        'order': transformations.ControlOrderMetric,
        'alignment': transformations.ControlAlignmentMetric,
        'sizeloss': transformations.SizeLossMetric
    }

    def __init__(self):
        """
        Creates a new program instance.
        """
        self._dest_dir = None
        self._no_subprocesses = None
        self._use_native_optimizer = None
        self._target_functions = None
        self._export_benchmarks = None
        self._optimizer_iterations = None

        self._input_layouts_total = None
        self._transformed_layouts_total = None
        self._transformed_layouts_fail = None
        self._performance = PerformanceObserver()

    def transform_and_generate_single(self, input_file, legacy_layout,
                                      silent=True):
        """
        Processes a single legacy layout instance through the transformation
        and generation processes. When run in a separate process due to
        parallelization, no printing is allowed.

        Arguments:
        input_file -- Name of file legacy_layout was parsed from
        legacy_layout -- Legacy layout instance to transform
        silent -- False if the transformation process is allowed to print
        debug information to standard output, True otherwise. Should stay at
        True when processing multiple layouts in parallel to avoid clashes.

        Return value:
        * (transformation time, generation time) if this instance could
          be transformed, which should always be the case because there is only
          one return point
        """

        #
        # Run the simple layout transformation on the legacy layout instance.
        # It is both used as seed for the optimizer's population, as well as
        # an output (to compare with the optimizer's output).
        # The simple layout transformation is fast, but its results may not
        # be optimal.
        #
        transformer = transformations.SimpleLayoutTransformation()
        time_start = time.perf_counter()
        initial_grid_layout = transformer.transform(legacy_layout)
        time_end = time.perf_counter()

        # Transformation time is composed of (time for simple transformation +
        # time for optimization) without the preparations in-between.
        transformation_time = (time_end * 1000.0) - (time_start * 1000.0)

        # The command-line frontend translated the function strings into the
        # class types for us.

        # Print the metrics for the initial solution
        if not silent:
            for m in self._target_functions:
                log_note(
                    "Metric of initial solution {0}: {1}",
                    initial_grid_layout.name,
                    m.calculate(legacy_layout, initial_grid_layout,
                                debug=False)
                )

        # Initialize the optimizer. Use EvolutionaryOptimizer for the slow, but
        # functioning pure-Python optimizer, and NativeEvolutionaryOptimizer
        # for the unfinished, but fast C optimizer.
        if self._use_native_optimizer:
            optimizer_class = transformations.NativeEvolutionaryOptimizer
        else:
            optimizer_class = transformations.EvolutionaryOptimizer
        optimizer = optimizer_class(
            self._target_functions, legacy_layout,
            population_seed=initial_grid_layout
        )

        # Perform the optimization. Treat it as if you're compiling a large
        # project: https://xkcd.com/303/
        time_start = time.perf_counter()
        optimized_grid_layout = optimizer.optimize(
            self._optimizer_iterations, silent=silent
        )
        time_end = time.perf_counter()

        transformation_time += (time_end * 1000.0) - (time_start * 1000.0)

        # Print the metrics for the optimized solution. These values tend
        # to be worse if the initial solution is not included by the
        # optimizer, but better otherwise.
        if not silent:
            for m in self._target_functions:
                log_note(
                    "Metric of grid layout {0}: {1}",
                    optimized_grid_layout.name,
                    m.calculate(legacy_layout, optimized_grid_layout,
                                debug=False)
                )

        #
        # Export grid layout instances to HTML (layout generation)
        #
        generator = generators.BootstrapHTMLGenerator()
        generator.generate(initial_grid_layout, self._dest_dir,
                           name_suffix='_initial')
        time_start = time.perf_counter()
        generator.generate(optimized_grid_layout, self._dest_dir)
        time_end = time.perf_counter()

        # Time for HTML code generation only takes the optimized layout into
        # account, since it's the one we're interested in.
        generator_time = (time_end * 1000.0) - (time_start * 1000.0)

        # Legacy layout instance successfully transformed :)
        return (transformation_time, generator_time)

    def transform_and_generate_single_mp(self, input_file, legacy_layout,
                                         silent=True):
        """
        Wrapper around transform_and_generate_single() to silently catch
        KeyboardInterrupts.

        Arguments:
        input_file -- Name of file legacy_layout was parsed from
        legacy_layout -- Legacy layout instance to transform
        silent -- False if the transformation process is allowed to print
        debug information to standard output, True otherwise. Should stay at
        True when processing multiple layouts in parallel to avoid clashes.

        Return value:
        * (transformation time, generation time) if this instance could
          be transformed
        * None otherwise
        """

        try:
            return self.transform_and_generate_single(
                input_file, legacy_layout, silent
            )
        except KeyboardInterrupt:
            # KeyboardInterrupts are sent to the subprocesses as well, but are
            # not captured by the parent process. Silence those and return a
            # failure.
            return None

    def print_progress(self):
        """
        Prints the current progress of the transformation process.
        """
        sys.stdout.write(
            "layout transformation in progress: {0}/{1} completed (with {2} "
            "failures)\r".format(
                self._transformed_layouts_total,
                self._input_layouts_total,
                self._transformed_layouts_fail
            )
        )

    def future_done(self, input_file, layout, result):
        """
        Callback which is called if a layout transformation completed with a
        True or False result code.

        Arguments:
        input_file -- Name of file legacy_layout was parsed from
        layout -- Legacy layout instance which has been transformed
        result -- True if a layout instance has been successfully transformed,
        False if not
        """

        # Update counters and print current progress
        if result is None:
            self._transformed_layouts_fail += 1
        self._transformed_layouts_total += 1
        self.print_progress()

        # Record transformation and generation times
        if result is not None:
            self._performance.record_transformation_time(
                input_file, layout.name, result[0]
            )
            self._performance.record_generator_time(
                input_file, layout.name, result[1]
            )

    def future_error(self, layout, e):
        """
        Callback which is called if a layout transformation terminated with
        an exception.

        Arguments:
        layout -- Legacy layout instance which has been transformed
        e -- Exception raised by a subprocess
        """

        # Update counters and print current progress
        log_error("Transformation of {0} failed: {1}", layout.name, e)
        self._transformed_layouts_fail += 1
        self._transformed_layouts_total += 1
        self.print_progress()

    def transform_and_generate(self, legacy_layouts):
        """
        Given a list of legacy layout instances, processes each of them
        individually through the transformation and generation processes.

        Arguments:
        legacy_layouts -- List of legacy layout instances to transform

        Return value:
        * True if all legacy layout instances could be transformed
        * False otherwise
        """

        # Print initial progress
        self._input_layouts_total = len(legacy_layouts)
        self._transformed_layouts_total = 0
        self._transformed_layouts_fail = 0
        self.print_progress()

        if self._no_subprocesses:
            #
            # Operate on a single legacy layout instance at once. This mode
            # allows debug prints to be written to standard output, but is only
            # intended for debugging because all transformations are performed
            # on a single core.
            #
            for input_file, layout in legacy_layouts:
                try:
                    result = self.transform_and_generate_single(
                        input_file, layout, silent=False
                    )

                    # We need to call the completion callback ourselves
                    self.future_done(input_file, layout, result)
                except KeyboardInterrupt as e:
                    # Re-raise keyboard interrupts, we don't want to catch them
                    raise e
                except Exception as e:
                    self.future_error(layout, e)
        else:
            #
            # Operate on multiple legacy layout instances in parallel. Use
            # process-based execution because Python's global interpreter lock
            # worsens any attempt at parallel computations inside the optimizer
            # (that one can only be circumvented by re-implementing the
            # optimizer in native code, which has been partially done, but
            # cannot be finished by the time the thesis text is submitted).
            #

            # Create a process pool
            pool = multiprocessing.Pool()

            for input_file, layout in legacy_layouts:
                # The callbacks are class methods. Create a partial function
                # which only has the result / exception as its parameter
                callback = functools.partial(self.future_done, input_file,
                                             layout)
                error_callback = functools.partial(self.future_error, layout)

                # Queue each legacy layout instance individually
                pool.apply_async(
                    self.transform_and_generate_single_mp,
                    args=(input_file, layout),
                    callback=callback,
                    error_callback=error_callback
                )

            # These are the only tasks to perform, we can close the pool
            pool.close()

            # Wait for the transformations to complete. If the process has
            # been interrupted, kill all subprocesses of the pool, and
            # re-raise the exception for the top-level KeyboardInterrupt
            # handler outside this class.
            try:
                pool.join()
            except KeyboardInterrupt as e:
                pool.terminate()
                raise e

        # Final progress print. Add a newline because print_progress() uses
        # carriage returns and we don't want this line to be overwritten
        # with the shell prompt.
        self.print_progress()
        sys.stdout.write('\n')

        # UITransform exits with code 0 if no transformation failed
        if self._export_benchmarks:
            self._performance.export_all()
        return self._transformed_layouts_fail == 0

    def transform_descriptions(self, input_files):
        """
        Reads a list of description files, extracts instances of legacy layouts
        out of these, and transforms them into instances of grid layouts.

        Arguments:
        input_files -- List of description files to read

        Return value:
        * True if all legacy layout instances could be transformed
        * False otherwise
        """

        #
        # The parsing process, also referred to as 'layout analysis', is
        # performed sequentially, as it shouldn't be computationally complex.
        #
        parser_failures = 0
        extracted_legacy_instances = []
        for input_file in input_files:
            # Select parser which supports the current file
            parser = inputparsers.get_parser_for_extension(
                os.path.splitext(input_file)[1]
            )

            # Parse the content. Should the parser throw an exception, then
            # it's likely a syntax error somewhere in the content. Skip the
            # file and parse the remaining ones.
            try:
                time_start = time.perf_counter()
                parser.parse_file(input_file)
                time_end = time.perf_counter()
            except Exception as e:
                log_error("parsing input file '{0}' failed: {1}", input_file,
                          str(e))
                parser_failures += 1
                continue

            # The parser holds a list of legacy layout instances (a single file
            # is allowed to contain multiple descriptions of such instances).
            input_file_layouts = parser.get_layout_models()
            if len(input_file_layouts) > 0:
                log_note("parsed {0} legacy layout(s) from '{1}'",
                         len(input_file_layouts), input_file)

                for layout in input_file_layouts:
                    # Track the filename for performance measurement reasons
                    extracted_legacy_instances.append((input_file, layout))

                    # Use the same time for all layouts from the current file
                    self._performance.new_layout(input_file, layout.name)
                    self._performance.record_analysis_time(
                        input_file,
                        layout.name,
                        (time_end * 1000.0) - (time_start * 1000.0)
                    )

        # Print statistics
        log_note("files which could not be parsed: {0}", parser_failures)
        log_note("total legacy layouts parsed: {0} from {1} file(s)",
                 len(extracted_legacy_instances),
                 len(input_files) - parser_failures)

        # Pass the extracted instances individually to the layout
        # transformation and generation processes
        return self.transform_and_generate(extracted_legacy_instances)

    def collect_files_and_transform(self, input_dir):
        """
        Collects supported description files in 'input_dir' and adds those to
        the transformation process, continuing with the process afterwards.

        Arguments:
        input_dir -- Directory where layout descriptions are found

        Return value:
        * True if all legacy layout instances could be transformed
        * False otherwise
        """

        # Collect files in 'input_dir' with their full path
        all_input_files = [
            f
            for f in map(
                lambda x: os.path.join(input_dir, x),
                os.listdir(input_dir)
            )
            if os.path.isfile(f)
        ]

        # Go through the collected files and filter those with unsupported file
        # extensions.
        supported_input_files = [
            f for f in all_input_files
            if os.path.splitext(f)[1] in
            inputparsers.get_supported_extensions()
        ]

        # Print unsupported files
        for f in set(all_input_files) - set(supported_input_files):
            log_warning("skipping unsupported input file '{0}'", f)

        # Got any files?
        if len(supported_input_files) == 0:
            log_error("no supported input files found in '{0}'", input_dir)
            return False

        # Operate on supported files
        return self.transform_descriptions(supported_input_files)

    def main(self, argv):
        """
        Entry point of the UITransform command-line frontend.

        Arguments:
        argv -- Options passed to the program from the command prompt or
                terminal

        Return value:
        0 if the program terminated successfully
        1 if the program terminated with an error
        """

        # Create a new argument parser accepting two positional arguments and
        # no optional ones
        parser = argparse.ArgumentParser(
            description=UITransform.UITRANSFORM_DESCRIPTION,
            formatter_class=argparse.ArgumentDefaultsHelpFormatter
        )
        parser.add_argument('source_dir', metavar='SOURCE_DIR', type=str,
                            help=UITransform.ARG_SOURCE_DIR_HELP)
        parser.add_argument('dest_dir', metavar='DEST_DIR', type=str,
                            help=UITransform.ARG_DEST_DIR_HELP)
        parser.add_argument('--sequential', dest='sequential',
                            action='store_true',
                            help=UITransform.ARG_SEQUENTIAL_HELP)
        parser.add_argument('--native', dest='use_native_optimizer',
                            action='store_true',
                            help=UITransform.ARG_NATIVE_HELP)

        target_choices = [x for x in self.SUPPORTED_TARGET_FUNCTIONS.keys()]
        parser.add_argument('--target', dest='target_function_strings',
                            action='append', choices=target_choices,
                            help=UITransform.ARG_TARGET_HELP)
        parser.add_argument('--export-benchmarks', dest='export_benchmarks',
                            action='store_true',
                            help=UITransform.ARG_EXPORT_BENCHMARKS_HELP)
        parser.add_argument('--iterations', dest='iterations', type=int,
                            default=config.OPTIMIZE_ITERATIONS,
                            help=UITransform.ARG_ITERATIONS_HELP)

        # Process arguments
        args = parser.parse_args()

        # Check source directory availability
        if not os.path.exists(args.source_dir):
            log_error("source directory '{0}' does not exist", args.source_dir)
            return 1

        # Destination directory should be created, and emptied if there are
        # files in it
        try:
            os.makedirs(args.dest_dir)
            log_note("created destination directory '{0}'", args.dest_dir)
        except OSError as e:
            if e.errno == errno.EEXIST:
                for root, dirs, files in os.walk(args.dest_dir):
                    for name in dirs:
                        shutil.rmtree(os.path.join(root, name))
                    for name in files:
                        os.remove(os.path.join(root, name))
            else:
                log_error("cannot create/clean destination directory '{0}': "
                          "{1}", args.dest_dir, str(e))
                return 1

        # Save arguments in instance
        self._dest_dir = args.dest_dir
        self._no_subprocesses = args.sequential
        self._use_native_optimizer = args.use_native_optimizer
        self._export_benchmarks = args.export_benchmarks
        self._optimizer_iterations = args.iterations

        # Check if target functions have been specified
        if args.target_function_strings is not None:
            target_function_strings = args.target_function_strings
        else:
            log_warning(
                "no target functions given on command line, falling back to "
                "default configuration"
            )
            target_function_strings = config.OPTIMIZE_DEFAULT_TARGETS

        # Map the target functions specified on the command line to the
        # corresponding classes. argparse as well as the programmer (in case of
        # config.OPTIMIZE_DEFAULT_TARGETS) ensure that only the strings in the
        # mapping dictionary are accepted.
        log_note(
            "using the following target functions: {0}",
            " ".join(target_function_strings)
        )
        self._target_functions = tuple(
            self.SUPPORTED_TARGET_FUNCTIONS[target]
            for target in target_function_strings
        )

        # Pass options to core functionality
        return 0 if self.collect_files_and_transform(args.source_dir) else 1


# Export the UITransform program class and the logging helpers
__all__ = [
    'UITransform',
    'log_error',
    'log_note',
    'log_warning'
]
