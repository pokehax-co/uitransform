#
# Copyright (c) 2018 Thomas Weber <pokehaxco@gmail.com>
#
# SPDX-License-Identifier: MIT
#

#
# Category: Metrics -- Whitespace ratio, Density
#
# Size of the destination viewport of grid layouts. This is used to calculate
# the area occupied by controls in a grid layout.
#
# 1920x1080 is the second most popular desktop screen resolution according to
# StatCounter in January 2018:
# http://gs.statcounter.com/screen-resolution-stats/desktop/worldwide
#
GRID_LAYOUT_VIEWPORT_SIZE = (1920, 1080)

#
# Category: Metrics -- Density
#
# To measure the density of a layout, its area is divided into a number of
# equally sized parts (which must be a power of 2 larger than 1) first before
# measuring the whitespace ratio in each part.
#
# The default value is 16 parts.
#
DENSITY_LAYOUT_DIVISIONS = 16

#
# Category: Metrics -- Order of controls
#
# Weights of the eight possible control order iterations, required for
# calculating the weighted average over the Levenshtein distances. Higher
# weights shift the average towards the corresponding values.
#
CONTROL_ORDER_METRIC_WEIGHTS = (
    50,     # Western (left-to-right horizontal, from top)
    2,      # Left-to-right vertical, from top
    10,     # Arabic (right-to-left horizontal, from top)
    5,      # East Asian (right-to-left vertical, from top)
    1,      # Left-to-right horizontal, from bottom
    1,      # Left-to-right vertical, from bottom
    1,      # Right-to-left horizontal, from bottom
    1       # Right-to-left vertical, from bottom
)

#
# Category: Evolutionary optimization
#
# Number of iterations to perform during the optimization process. The default
# value is 200 for the Python optimizer, but can be much higher than in the
# C version.
#
OPTIMIZE_ITERATIONS = 200

#
# Category: Evolutionary optimization
#
# Target functions to optimize when no --target argument is specified.
#
OPTIMIZE_DEFAULT_TARGETS = ['order', 'alignment']

#
# Category: Evolutionary optimization
#
# Size of the population evolved during the optimization process.
#
OPTIMIZE_POPULATION_SIZE = 30

#
# Category: Evolutionary optimization
#
# Size of the mating pool created for the reproduction of a new population
# during the optimization process.
#
OPTIMIZE_MATING_POOL_SIZE = 22

#
# Category: Evolutionary optimization
#
# Chance between 0 and 1 that an individual from the mating pool is selected
# for recombination. Encourages searches within the individuals' vicinity.
#
OPTIMIZE_RECOMBINATION_CHANCE = 0.5

#
# Category: Evolutionary optimization
#
# Chance between 0 and 1 that an individual from the mating pool or the new
# population is selected for mutation. Encourages searches over greater
# distances.
#
OPTIMIZE_MUTATION_CHANCE = 0.15

#
# Category: Concepts
#
# In the whitespace ratio measurement, the total area occupied by controls in
# a layout is calculated. Usually, a control draws its visuals on the whole
# area, but the group box only draws a small border near its area's edges. The
# undrawn area of the group box should be treated as unoccupied. This option
# configures the width of the group box's border.
#
# The default value is 4 pixels.
#
GROUPBOX_BORDER_WIDTH = 4

#
# Category: Concepts
#
# A grid layout has a fixed number of columns, at least in this application.
#
# The default value is 12 columns, for compatibility with the Bootstrap
# framework.
#
GRID_LAYOUT_COLUMN_COUNT = 12

#
# Category: Layout generation
#
# André Firchow developed a CSS stylesheet which visualizes the structure of
# Bootstrap grids (http://firchow.net/bootstrap3-grid-debug/) for debugging
# purposes. This option allows to enable or disable this stylesheet.
#
BOOTSTRAP_GRID_DEBUGGING = False

#
# Category: Layout analysis
#
# Shows the depth and number of controls in each layout.
#
LAYOUT_ANALYSIS_STATISTICS = False
