#
# Copyright (c) 2018 Thomas Weber <pokehaxco@gmail.com>
#
# SPDX-License-Identifier: MIT
#

import copy
import ctypes
import numpy


class Rectangle(object):
    """
    This class implements a rectangle. A rectangle, when placed into a
    2-dimensional canvas, defines a portion of the canvas using 4 properties:
    a horizontal start position, a vertical start position, a horizontal span,
    and a vertical span.
    """

    def __setattr__(self, name, value):
        """
        Controls the behavior of setting attributes for this instance.
        """

        # Ensure that size and positional properties are None, integers, or
        # floating-point numbers. XXX: Should it be necessary to accept other
        # number implementations (numpy/scipy), add them.
        if name in ('h_start', 'v_start', 'h_span', 'v_span'):
            allowed_types = (int, float, numpy.float64)
            assert value is None or type(value) in allowed_types

        super().__setattr__(name, value)

    def __init__(self, h_start, v_start, h_span, v_span):
        """
        Initializes a new rectangle with the specified coordinates and size.
        """

        self.h_start = h_start
        self.v_start = v_start
        self.h_span = h_span
        self.v_span = v_span

    def __copy__(self):
        """
        Callback for the copy.copy() method, which performs shallow copies of
        objects.
        """
        rc = self.__class__.__new__(self.__class__)

        setattr(rc, 'h_start', self.h_start)
        setattr(rc, 'v_start', self.v_start)
        setattr(rc, 'h_span', self.h_span)
        setattr(rc, 'v_span', self.v_span)
        return rc

    def __deepcopy__(self, memo):
        """
        Callback for the copy.deepcopy() method, which performs deep copies of
        objects.
        """
        rc = self.__class__.__new__(self.__class__)
        memo[id(self)] = rc

        setattr(rc, 'h_start', copy.deepcopy(self.h_start, memo))
        setattr(rc, 'v_start', copy.deepcopy(self.v_start, memo))
        setattr(rc, 'h_span', copy.deepcopy(self.h_span, memo))
        setattr(rc, 'v_span', copy.deepcopy(self.v_span, memo))
        return rc

    def __str__(self):
        """
        Returns a human-readable representation of the rectangle stored in
        this instance.
        """

        fmt = ("[Rectangle: position {h_start}, {v_start}; "
               "size {h_span}, {v_span}]")
        return fmt.format(**self.__dict__)

    def __repr__(self):
        """
        Returns a representation which allows recreation of the object
        instance.
        """

        fmt = "Rectangle({h_start}, {v_start}, {h_span}, {v_span})"
        return fmt.format(**self.__dict__)

    @property
    def h_end(self):
        """
        Returns the first horizontal coordinate after the last coordinate
        covered by this rectangle.
        """
        return self.h_start + self.h_span

    @property
    def v_end(self):
        """
        Returns the first vertical coordinate after the last coordinate covered
        by this rectangle.
        """
        return self.v_start + self.v_span

    def contains(self, rc):
        """
        Checks whether rc is contained entirely in this rectangle.
        """

        return self.h_start <= rc.h_start and self.v_start <= rc.v_start and \
            (self.h_start + self.h_span) >= (rc.h_start + rc.h_span) and \
            (self.v_start + self.v_span) >= (rc.v_start + rc.v_span)


class NativeCodeRectangle(ctypes.Structure):
    """
    This class represents Rectangles in native code. Native code only supports
    rectangles with integer coordinates.

    Please keep this definition in sync with the actual C code, otherwise
    crashes will be inevitable!
    """

    _fields_ = [
        ('h_start', ctypes.c_int),
        ('v_start', ctypes.c_int),
        ('h_span', ctypes.c_int),
        ('v_span', ctypes.c_int)
    ]

    @staticmethod
    def convert_to(rc):
        """
        Converts a Rectangle to a NativeCodeRectangle.

        Arguments:
        rc -- Rectangle to convert

        Returns:
        A new NativeCodeRectangle representing the original rectangle
        """

        assert type(rc) is Rectangle
        assert type(rc.h_start) is int
        assert type(rc.v_start) is int
        assert type(rc.h_span) is int
        assert type(rc.v_span) is int

        return NativeCodeRectangle(
            h_start=ctypes.c_int(rc.h_start),
            v_start=ctypes.c_int(rc.v_start),
            h_span=ctypes.c_int(rc.h_span),
            v_span=ctypes.c_int(rc.v_span)
        )

    @staticmethod
    def convert_from(rc):
        """
        Converts a NativeCodeRectangle to a Rectangle.

        Arguments:
        rc -- NativeCodeRectangle to convert

        Returns:
        A new Rectangle representing the original rectangle
        """

        assert type(rc) is NativeCodeRectangle

        return Rectangle(
            rc.h_start, rc.v_start, rc.h_span, rc.v_span
        )
