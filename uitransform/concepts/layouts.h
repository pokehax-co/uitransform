/*
 * Copyright (c) 2018 Thomas Weber <pokehaxco@gmail.com>
 *
 * SPDX-License-Identifier: MIT
 */

#ifndef CONCEPTS_LAYOUTS_H
#define CONCEPTS_LAYOUTS_H

#include "controls.h"

#include <stdbool.h>

/*
 * This class implements a model to describe two-dimensional layouts. Layouts
 * contain controls and may be restricted by derivative classes. Each control
 * is placed on a layout using two properties for the position, and two
 * properties for the size (all properties are non-negative integers).
 *
 * 
 */
struct Layout
{
    const char *name;
    const char *title;

    /* for legacy layouts only; all values are 0 for grid layouts */
    struct Rectangle rc;
    unsigned int line_height;

    unsigned int control_count;
    struct Control *controls;
};

static inline bool layout_is_grid(const struct Layout *restrict layout)
{
    /* Legacy layouts must have a non-zero line height */
    return layout->line_height == 0;
}

bool layout_copy_no_strings(struct Layout *restrict dest,
    const struct Layout *restrict source);

struct Layout *layout_duplicate_no_strings(
    const struct Layout *restrict layout);

void layout_free_no_strings(struct Layout *restrict dest);

void layout_free_duplicate_no_strings(struct Layout *restrict layout);

void layout_str(const struct Layout *restrict layout);

struct Control *layout_find_control_by_id(
    const struct Layout *restrict layout, unsigned int id);

struct Control *layout_find_control_by_id_binary(
    const struct Layout *restrict layout, unsigned int id);

#endif
