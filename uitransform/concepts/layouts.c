/*
 * Copyright (c) 2018 Thomas Weber <pokehaxco@gmail.com>
 *
 * SPDX-License-Identifier: MIT
 */

#include "layouts.h"

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

/*
 * bsearch() callback for layout_find_control_by_id_binary().
 */
static int _search_layout_control_id(const void *_key, const void *_element)
{
    unsigned int key = (unsigned int)(uintptr_t) _key;
    const struct Control *element = _element;

    /* Don't overflow. https://stackoverflow.com/a/27284248 */
    return (key > element->internal_id) - (key < element->internal_id);
}

/*
 * Copies the layout 'source' to 'dest' without copying strings. The control
 * list is duplicated, also without strings.
 */
bool layout_copy_no_strings(struct Layout *restrict dest,
    const struct Layout *restrict source)
{
    /* We copy string pointers, so this is sufficient */
    *dest = *source;

    /* Copy the controls */
    dest->controls = malloc(dest->control_count *
        sizeof(*dest->controls));
    if (!dest->controls) {
        fprintf(stderr, "error: out of memory\n");
        return false;
    }

    /* Copy the controls */
    for (unsigned int i = 0; i < dest->control_count; ++i)
        control_copy_no_strings(&dest->controls[i], &source->controls[i]);
    return true;
}

/*
 * Duplicates the specified layout, without copying strings.
 */
struct Layout *layout_duplicate_no_strings(
    const struct Layout *restrict layout)
{
    /* Allocate base structure */
    struct Layout *layout_copy = malloc(sizeof(*layout_copy));
    if (!layout_copy) {
        fprintf(stderr, "error: out of memory\n");
        return NULL;
    }

    /* Perform a no-strings copy */
    if (!layout_copy_no_strings(layout_copy, layout))
        free(layout_copy), layout_copy = NULL;

    return layout_copy;
}

/*
 * Frees the content of a layout copied using layout_copy_no_strings().
 */
void layout_free_no_strings(struct Layout *restrict dest)
{
    free(dest->controls);
    dest->controls = NULL;
}

/*
 * Frees the content of a layout copied using layout_duplicate_no_strings().
 */
void layout_free_duplicate_no_strings(struct Layout *restrict layout)
{
    layout_free_no_strings(layout);
    free(layout);
}

/*
 * Prints the layout to standard output.
 */
void layout_str(const struct Layout *restrict layout)
{
    printf("  name: %p '%s'\n", layout->name, layout->name);
    printf("  title: %p '%s'\n", layout->title, layout->title);

    // XXX: use rectangle_str()
    printf("  rc: {%d,%d,%d,%d}\n", layout->rc.h_start,
        layout->rc.v_start, layout->rc.h_span,
        layout->rc.v_span);
    printf("  line_height: %u\n", layout->line_height);

    printf("  control_count: %u\n", layout->control_count);
    printf("  controls: %p\n", layout->controls);
    for (unsigned int i = 0; i < layout->control_count; ++i)
        control_str(&layout->controls[i]);
}

/*
 * Finds a control in the layout based on the internal ID.
 */
struct Control *layout_find_control_by_id(
    const struct Layout *restrict layout, unsigned int id)
{
    /* Here, we do not assume that the control list is sorted */
    for (unsigned int i = 0; i < layout->control_count; ++i) {
        struct Control *c = &layout->controls[i];
        if (c->internal_id == id)
            return c;
    }
    return NULL;
}

/*
 * Finds a control in the layout based on the internal ID using binary search.
 * This requires the control list to be sorted incrementally by their internal
 * ID.
 */
struct Control *layout_find_control_by_id_binary(
    const struct Layout *restrict layout, unsigned int id)
{
    /* Here, we assume that the control list is sorted */
    return bsearch((const void *)(uintptr_t) id, layout->controls,
        layout->control_count, sizeof(struct Control),
        _search_layout_control_id);
}
