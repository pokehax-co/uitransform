/*
 * Copyright (c) 2018 Thomas Weber <pokehaxco@gmail.com>
 *
 * SPDX-License-Identifier: MIT
 */

#ifndef CONCEPTS_RECTANGLE_H
#define CONCEPTS_RECTANGLE_H

#include <stdbool.h>

/*
 * A rectangle, when placed into a 2-dimensional canvas, defines a portion of
 * the canvas using 4 properties: a horizontal start position, a vertical start
 * position, a horizontal span, and a vertical span.
 *
 * Unlike the Python implementation, all parameters must be integers; floating
 * point numbers are disallowed.
 */
struct Rectangle
{
    int h_start, v_start;
    int h_span, v_span;
};

/* XXX: rectangle_str() */

/*
 * Returns the first horizontal coordinate after the last coordinate covered by
 * this rectangle.
 */
static inline int rectangle_h_end(const struct Rectangle *restrict rc)
{
    return rc->h_start + rc->h_span;
}

/*
 * Returns the first vertical coordinate after the last coordinate covered by
 * this rectangle.
 */
static inline int rectangle_v_end(const struct Rectangle *restrict rc)
{
    return rc->v_start + rc->v_span;
}

/*
 * Checks whether rc2 is contained entirely in rc1.
 */
static inline bool rectangle_contains(const struct Rectangle *restrict rc1,
    const struct Rectangle *restrict rc2)
{
    return (
        rc1->h_start <= rc2->h_start &&
        rc1->v_start <= rc2->v_start &&
        rectangle_h_end(rc1) >= rectangle_h_end(rc2) &&
        rectangle_v_end(rc1) >= rectangle_v_end(rc2)
    );
}

#endif
