/*
 * Copyright (c) 2018 Thomas Weber <pokehaxco@gmail.com>
 *
 * SPDX-License-Identifier: MIT
 */

#include "controls.h"

#include <stdio.h>

/*
 * Prints the control to standard output.
 */
void control_str(const struct Control *restrict control)
{
    printf("  - internal_id: %u\n", control->internal_id);
    printf("    ctype: %u\n", control->ctype);

    printf("    original_id: %p '%s'\n",
        control->original_id, control->original_id);
    printf("    class_string: %p '%s'\n",
        control->class_string, control->class_string);

    // XXX: use rectangle_str()
    printf("    rc: {%d,%d,%d,%d}\n", control->rc.h_start, control->rc.v_start,
        control->rc.h_span, control->rc.v_span);
    printf("    parent_control: %d\n", control->parent_control);

    printf("    property_alignment: %d\n",
        control->property_alignment);
    printf("    property_button_content: %d\n",
        control->property_button_content);
    printf("    property_textbox_content: %d\n",
        control->property_textbox_content);
    printf("    property_caption_value: %p '%s'\n",
        control->property_caption_value, control->property_caption_value);
    printf("    property_text_value: %p '%s'\n",
        control->property_text_value, control->property_text_value);
    printf("    property_is_default: %s\n",
        control->property_is_default ? "true" : "false");
    printf("    property_is_disabled: %s\n",
        control->property_is_disabled ? "true" : "false");
    printf("    property_is_tristate: %s\n",
        control->property_is_tristate ? "true" : "false");
    printf("    property_is_multiline: %s\n",
        control->property_is_multiline ? "true" : "false");
}
