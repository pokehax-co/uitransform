#
# Copyright (c) 2017-2018 Thomas Weber <pokehaxco@gmail.com>
#
# SPDX-License-Identifier: MIT
#

import copy
import ctypes

from enum import Enum
from .rectangle import Rectangle, NativeCodeRectangle


class ControlType(Enum):
    """
    This enumeration contains types of controls which are recognized by the
    application stack.
    """

    UNDEFINED = 0
    PLAINTEXT = 1
    GROUPBOX = 2
    BUTTON = 3
    CHECKBOX = 4
    RADIOBUTTON = 5
    IMAGE = 6
    TEXTBOX = 7
    COMBOBOX = 8
    PLACEHOLDER = 255


class ButtonContent(Enum):
    """
    This enumation contains types of content which can be displayed inside a
    button.
    """

    TEXT = 0
    BITMAP = 1
    ICON = 2


class TextboxContent(Enum):
    """
    This enumation contains types of content which can be displayed inside a
    textbox.
    """

    TEXT = 0
    NUMBER = 1
    PASSWORD = 2


class HorizontalAlignment(Enum):
    """
    This enumeration defines horizontal alignments. Mainly intended for text;
    it can be aligned to the left, to the right, or to the center of the block
    it is contained in.
    """

    UNDEFINED = 0
    LEFT = 1
    CENTER = 2
    RIGHT = 3


class Control(object):
    """
    A control is a user interface element which can be interacted with. Its
    type defines its function: it can act as a button, input field, etc.
    It contains various identification-related properties for both internal and
    external use. Contains may have a relation to a parent control, while some
    types of controls act as container for further controls.

    (This class only contains properties unrelated to positioning and sizing.)
    """

    def __init__(self, internal_id, original_id, class_string,
                 parent_control=None, ctype=ControlType.UNDEFINED,
                 properties={}):
        """
        Initializes a new control.

        Arguments:
        internal_id -- Identifier used internally for referencing it. This must
        be a non-negative integer.
        original_id -- A string which identifies the control in a source
        description of a layout instance.
        class_string -- Replacement text for the layout generator if the
        control type is ControlType.PLACEHOLDER.
        parent_control -- Identifier of the control this control is placed in.
        It cannot be equal to the current internal ID. If the control is not
        placed inside a control, but its parent is the layout itself, set this
        value to None.
        ctype -- Type of control to create
        properties -- Additional properties which are relevant for the layout
        generator.
        """

        # Perform type and value checking in form of assertions, so we can skip
        # them for performance reasons in production
        assert type(internal_id) is int and internal_id >= 0
        assert type(original_id) is str
        assert parent_control is None or (
            type(parent_control) is int and parent_control >= 0 and
            parent_control != internal_id
        )
        assert type(ctype) is ControlType
        assert type(properties) is dict

        self._internal_id = internal_id
        self._original_id = original_id
        self._class_string = class_string
        self._parent_control = parent_control
        self._ctype = ctype
        self._properties = properties

    def __copy__(self):
        """
        Callback for the copy.copy() method, which performs shallow copies of
        objects.
        """
        rc = self.__class__.__new__(self.__class__)

        setattr(rc, '_internal_id', self._internal_id)
        setattr(rc, '_original_id', self._original_id)
        setattr(rc, '_class_string', self._class_string)
        setattr(rc, '_parent_control', self._parent_control)
        setattr(rc, '_ctype', self._ctype)
        setattr(rc, '_properties', self._properties)
        return rc

    def __deepcopy__(self, memo):
        """
        Callback for the copy.deepcopy() method, which performs deep copies of
        objects.
        """
        rc = self.__class__.__new__(self.__class__)
        memo[id(self)] = rc

        setattr(rc, '_internal_id',
                copy.deepcopy(self._internal_id, memo))
        setattr(rc, '_original_id',
                copy.deepcopy(self._original_id, memo))
        setattr(rc, '_class_string',
                copy.deepcopy(self._class_string, memo))
        setattr(rc, '_parent_control',
                copy.deepcopy(self._parent_control, memo))
        setattr(rc, '_ctype',
                copy.deepcopy(self._ctype, memo))
        setattr(rc, '_properties',
                copy.deepcopy(self._properties, memo))
        return rc

    def __str__(self):
        """
        Returns a human-readable representation of the control stored in this
        instance.
        """

        # Determine properties
        if not self._properties:
            property_dump = "   <No properties>"
        else:
            property_dump = "\n".join(
                ["   Property: %s = '%s'" % (x, self._properties[x])
                 for x in self._properties])

        # Begin with type, original ID, class, and properties
        line1 = (" - Control type: %s, internal ID: %u, original ID: '%s', "
                 "class: '%s', parent control: %s\n%s")
        return line1 % (self._ctype.name, self._internal_id,
                        self._original_id, self._class_string,
                        self._parent_control, property_dump)

    @property
    def internal_id(self):
        """
        Returns the identifier of this control as used internally.
        """
        return self._internal_id

    @property
    def original_id(self):
        """
        Returns the identifier of this control as specified in the original
        layout file.
        """
        return self._original_id

    @property
    def class_string(self):
        """
        Returns the class string for this control. In a Win32 resource script,
        for example, this is the window class representing the control in the
        Windows API.
        """
        return self._class_string

    @property
    def parent_control(self):
        """
        Returns the internal ID of the parent of this control, or None if the
        control's parent is the layout itself.
        """
        return self._parent_control

    @property
    def ctype(self):
        """
        Returns the type of this control.
        """
        return self._ctype

    @property
    def properties(self):
        """
        Returns the properties attached to this control.
        """
        return self._properties

    def is_container(self):
        """
        Returns whether this control can act as a container.
        """
        return self.ctype == ControlType.GROUPBOX


class PositionedControl(Control):
    """
    Represents a control which is placed at a particular coordinate and has a
    particular size.

    In a legacy layout, the position is defined by X and Y coordinates, and the
    size by width and height; all properties are measured in pixels for
    simplicity. In a grid layout, the position is defined by row and column
    number, and the size is defined by row span and column span.

    The rectangle of the control, which incorporates both position and size,
    can be modified.
    """

    def __init__(self, internal_id, original_id, class_string, rc,
                 parent_control=None, ctype=ControlType.UNDEFINED,
                 properties={}):
        """
        Initializes a new control.

        Arguments:
        internal_id -- Identifier used internally for referencing it. This must
        be a non-negative integer.
        original_id -- A string which identifies the control in a source
        description of a layout instance.
        class_string -- Replacement text for the layout generator if the
        control type is ControlType.PLACEHOLDER.
        rc -- Rectangle defining the control's boundary within its parent.
        parent_control -- Identifier of the control this control is placed in.
        It cannot be equal to the current internal ID. If the control is not
        placed inside a control, but its parent is the layout itself, set this
        value to None.
        ctype -- Type of control to create
        properties -- Additional properties which are relevant for the layout
        generator.
        """

        assert type(rc) is Rectangle

        self.rc = rc
        super().__init__(internal_id, original_id, class_string,
                         parent_control, ctype, properties)

    def __copy__(self):
        """
        Callback for the copy.copy() method, which performs shallow copies of
        objects.
        """
        rc = self.__class__.__new__(self.__class__)

        setattr(rc, '_internal_id', self._internal_id)
        setattr(rc, '_original_id', self._original_id)
        setattr(rc, '_class_string', self._class_string)
        setattr(rc, '_parent_control', self._parent_control)
        setattr(rc, '_ctype', self._ctype)
        setattr(rc, '_properties', self._properties)
        setattr(rc, 'rc', self.rc)
        return rc

    def __deepcopy__(self, memo):
        """
        Callback for the copy.deepcopy() method, which performs deep copies of
        objects.
        """
        rc = self.__class__.__new__(self.__class__)
        memo[id(self)] = rc

        setattr(rc, '_internal_id',
                copy.deepcopy(self._internal_id, memo))
        setattr(rc, '_original_id',
                copy.deepcopy(self._original_id, memo))
        setattr(rc, '_class_string',
                copy.deepcopy(self._class_string, memo))
        setattr(rc, '_parent_control',
                copy.deepcopy(self._parent_control, memo))
        setattr(rc, '_ctype',
                copy.deepcopy(self._ctype, memo))
        setattr(rc, '_properties',
                copy.deepcopy(self._properties, memo))
        setattr(rc, 'rc',
                copy.deepcopy(self.rc, memo))
        return rc

    def __str__(self):
        """
        Returns a human-readable representation of the control stored in this
        instance. Extend the Control __str__() routine with the rectangle.
        """
        return "%s\n   %s" % (super().__str__(), self.rc)


class NativeCodeControl(ctypes.Structure):
    """
    This class implements a ctypes structure definition to represent a control
    in native code, as well as methods to translate from/to PositionedControls.

    UITransform contains an alternative layout optimization implementation
    written in C instead of Python. It is faster, but less stable due to the
    code complexity. Also, as some Python features such as dictionaries are
    unavailable (the C code does *not* require linking against Python),
    property values have to be added directly to the structure.

    Please keep this definition in sync with the actual C code, otherwise
    crashes will be inevitable!
    """

    _fields_ = [
        ('internal_id', ctypes.c_uint),
        ('ctype', ctypes.c_int),
        ('original_id', ctypes.c_char_p),
        ('class_string', ctypes.c_char_p),
        ('rc', NativeCodeRectangle),
        ('parent_control', ctypes.c_int),

        ('property_alignment', ctypes.c_int),
        ('property_button_content', ctypes.c_int),
        ('property_textbox_content', ctypes.c_int),
        ('property_caption_value', ctypes.c_char_p),
        ('property_text_value', ctypes.c_char_p),
        ('property_is_default', ctypes.c_bool),
        ('property_is_disabled', ctypes.c_bool),
        ('property_is_tristate', ctypes.c_bool),
        ('property_is_multiline', ctypes.c_bool)
    ]

    @staticmethod
    def convert_to(c):
        """
        Converts a PositionedControl to a NativeCodeControl.

        Arguments:
        c -- PositionedControl to convert

        Returns:
        A new NativeCodeControl representing the original control
        """

        assert type(c) is PositionedControl

        # Convert strings to UTF-8
        arg_original_id = ctypes.c_char_p(c.original_id.encode('UTF-8'))
        arg_class_string = ctypes.c_char_p(c.class_string.encode('UTF-8'))

        # Convert the rectangle
        arg_control_rc = NativeCodeRectangle(
            h_start=ctypes.c_int(c.rc.h_start),
            v_start=ctypes.c_int(c.rc.v_start),
            h_span=ctypes.c_int(c.rc.h_span),
            v_span=ctypes.c_int(c.rc.v_span)
        )

        # Convert parent control
        arg_parent_control = (
            c.parent_control if c.parent_control is not None else -1
        )

        #
        # Translate currently implemented properties
        #
        arg_prop_alignment = (
            c.properties['alignment']
            if 'alignment' in c.properties
            else HorizontalAlignment.UNDEFINED
        ).value

        arg_prop_button_content = (
            c.properties['buttonContent']
            if 'buttonContent' in c.properties
            else ButtonContent.TEXT
        ).value

        arg_prop_textbox_content = (
            c.properties['textboxContent']
            if 'textboxContent' in c.properties
            else TextboxContent.TEXT
        ).value

        arg_prop_caption_value = ctypes.c_char_p(
            c.properties['captionValue'].encode('UTF-8')) \
            if 'captionValue' in c.properties else None

        arg_prop_text_value = ctypes.c_char_p(
            c.properties['textValue'].encode('UTF-8')) \
            if 'textValue' in c.properties else None

        arg_prop_is_default = ctypes.c_bool(c.properties['isDefault']) \
            if 'isDefault' in c.properties else False
        arg_prop_is_disabled = ctypes.c_bool(c.properties['isDisabled']) \
            if 'isDisabled' in c.properties else False
        arg_prop_is_tristate = ctypes.c_bool(c.properties['isTriState']) \
            if 'isTriState' in c.properties else False
        arg_prop_is_multiline = ctypes.c_bool(c.properties['isMultiline']) \
            if 'isMultiline' in c.properties else False

        # Create the structure
        return NativeCodeControl(
            internal_id=ctypes.c_uint(c.internal_id),
            original_id=arg_original_id,
            class_string=arg_class_string,
            ctype=ctypes.c_int(c.ctype.value),
            rc=arg_control_rc,
            parent_control=ctypes.c_int(arg_parent_control),

            property_alignment=ctypes.c_int(arg_prop_alignment),
            property_button_content=ctypes.c_int(arg_prop_button_content),
            property_textbox_content=ctypes.c_int(arg_prop_textbox_content),
            property_caption_value=arg_prop_caption_value,
            property_text_value=arg_prop_text_value,
            property_is_default=arg_prop_is_default,
            property_is_disabled=arg_prop_is_disabled,
            property_is_tristate=arg_prop_is_tristate,
            property_is_multiline=arg_prop_is_multiline
        )

    @staticmethod
    def convert_from(c):
        """
        Converts a NativeCodeControl to a PositionedControl.

        Arguments:
        c -- NativeCodeControl to convert

        Returns:
        A new PositionedControl representing the original control
        """

        assert type(c) is NativeCodeControl

        # Decode UTF-8 strings
        arg_original_id = (
            c.original_id.decode('UTF-8')
            if c.original_id is not None else '!NOORIGID!'
        )
        arg_control_class = (
            c.class_string.decode('UTF-8')
            if c.class_string is not None else '!NOORIGID!'
        )

        # Convert parent control and rectangle
        arg_parent_control = (
            c.parent_control if c.parent_control > -1 else None
        )
        arg_control_rc = NativeCodeRectangle.convert_from(c.rc)

        #
        # Translate properties
        #
        arg_properties = {}

        if c.property_alignment != HorizontalAlignment.UNDEFINED.value:
            arg_properties['alignment'] = HorizontalAlignment(
                c.property_alignment
            )

        arg_properties['buttonContent'] = ButtonContent(
            c.property_button_content
        )

        arg_properties['textboxContent'] = TextboxContent(
            c.property_textbox_content
        )

        if c.property_caption_value is not None:
            arg_properties['captionValue'] = (
                c.property_caption_value.decode('UTF-8')
            )

        if c.property_text_value is not None:
            arg_properties['textValue'] = (
                c.property_text_value.decode('UTF-8')
            )

        arg_properties['isDefault'] = c.property_is_default
        arg_properties['isDisabled'] = c.property_is_disabled
        arg_properties['isTriState'] = c.property_is_tristate
        arg_properties['isMultiline'] = c.property_is_multiline

        # Create the object instance
        return PositionedControl(
            c.internal_id, arg_original_id, arg_control_class,
            arg_control_rc, arg_parent_control, ControlType(c.ctype),
            arg_properties
        )
