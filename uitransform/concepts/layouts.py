#
# Copyright (c) 2017-2018 Thomas Weber <pokehaxco@gmail.com>
#
# SPDX-License-Identifier: MIT
#

import abc
import copy
import ctypes
import textwrap

from .controls import PositionedControl, NativeCodeControl
from .rectangle import Rectangle, NativeCodeRectangle


class LayoutModel(abc.ABC):
    """
    This class implements a model to describe two-dimensional layouts. Layouts
    contain controls and may be restricted by derivative classes. Each control
    is placed on a layout using two properties for the position, and two
    properties for the size (all properties are non-negative integers).

    This abstract class does not define how the controls are managed
    internally, other than the requirement that they can be retrieved, or the
    units of measurement for the position and size properties.
    """

    _HUMAN_READABLE_LAYOUT_TYPE = "layout"

    def __init__(self, name, title):
        """
        Initializes a new layout which contains no controls initially. The
        'name' attribute specifies an internal identifier which can be used to
        refer to it (instead of referencing it directly using the object
        instance), while the 'title' attribute assigns a window title or
        heading to the layout.
        """

        self._name = name
        self._title = title

    @property
    def name(self):
        """
        Returns the name of this layout.
        """
        return self._name

    @property
    def title(self):
        """
        Returns the window title of this layout.
        """
        return self._title

    @property
    @abc.abstractmethod
    def controls(self):
        """
        Method implemented by derivatives of LayoutModel to return the controls
        contained in this layout.
        """
        raise NotImplementedError("Method called from the LayoutModel "
                                  "abstract class")

    def _dump_extra_properties(self):
        """
        Method overridden by derivatives of LayoutModel to print additional
        properties of a layout in the __str__() method.
        """
        return ""

    def __copy__(self):
        """
        Callback for the copy.copy() method, which performs shallow copies of
        objects.
        """
        layout = self.__class__.__new__(self.__class__)

        setattr(layout, '_name', self._name)
        setattr(layout, '_title', self._title)
        return layout

    def __deepcopy__(self, memo):
        """
        Callback for the copy.deepcopy() method, which performs deep copies of
        objects.
        """
        layout = self.__class__.__new__(self.__class__)
        memo[id(self)] = layout

        setattr(layout, '_name', copy.deepcopy(self._name, memo))
        setattr(layout, '_title', copy.deepcopy(self._title, memo))
        return layout

    def __str__(self):
        """
        Returns a human-readable representation of this layout.
        """

        # Before dumping the control list, we check the type of the first
        # non-None entry in the layout's control tuple. If it is another tuple,
        # then we have a grid layout, and the control dump is adjusted
        # appropriately.
        control_list = self.controls
        if len(control_list) == 0:
            controls_dump = '<No controls>'
        else:
            is_grid = False
            for entry in control_list:
                if isinstance(entry, tuple):
                    is_grid = True
                    break

            # Dump accordingly
            if is_grid:
                controls_dump = ''
                for i, row in enumerate(control_list):
                    controls_dump += ' - Row %u:' % i
                    if row is None:
                        controls_dump += ' <empty>\n'
                    else:
                        _ = textwrap.indent('\n'.join(map(str, row)), '  ')
                        controls_dump += '\n' + _ + '\n'
            else:
                controls_dump = '\n'.join(map(str, control_list))

        # Indent the output by one level over the initial dump entries
        controls_dump = textwrap.indent(controls_dump, '  ')

        return (
            "==== Begin " + self._HUMAN_READABLE_LAYOUT_TYPE + " dump ====\n"
            " - Name: %s\n"
            " - Window title: %s\n"
            "%s%s\n"
            "==== End " + self._HUMAN_READABLE_LAYOUT_TYPE + " dump ====\n"
        ) % (self.name, self.title, self._dump_extra_properties(),
             controls_dump)

    @abc.abstractmethod
    def remove_control(self, control):
        """
        Method implemented by derivatives of LayoutModel to remove a control
        from the layout.
        """
        raise NotImplementedError("Method called from the LayoutModel "
                                  "abstract class")

    @abc.abstractmethod
    def add_control(self, control):
        """
        Method implemented by derivatives of LayoutModel to add a new control
        to the layout.
        """
        raise NotImplementedError("Method called from the LayoutModel "
                                  "abstract class")

    @abc.abstractmethod
    def get_control_by_id(self, control_id):
        """
        Method implemented by derivatives of LayoutModel to find a control in
        the layout based on the internal ID.
        """
        raise NotImplementedError("Method called from the LayoutModel "
                                  "abstract class")

    def _positive_to_absolute(self, control):
        """
        Converts a control's relative position to an absolute position.
        """

        (x, y) = (control.rc.h_start, control.rc.v_start)
        while control.parent_control is not None:
            control = self.get_control_by_id(control.parent_control)
            x += control.rc.h_start
            y += control.rc.v_start
        return (x, y)

    def get_control_rectangles(self):
        """
        Collects all rectangles of the controls inside the layout. If the
        control is part of a container, its position is restored to absolute
        coordinates.
        """

        # Determine container controls' absolute positions first
        container_xy = {}
        for c in self._controls:
            if c.is_container():
                container_xy[c.internal_id] = self._positive_to_absolute(c)

        # Iterate through the controls. Take rectangles of controls without
        # a parent as-is, and add the parent container's position otherwise
        control_rcs = []
        for c in self._controls:
            if c.parent_control is None:
                added_rc = c.rc
            else:
                starts = container_xy[c.parent_control]
                added_rc = Rectangle(
                    c.rc.h_start + starts[0], c.rc.v_start + starts[1],
                    c.rc.h_span, c.rc.v_span
                )
            control_rcs.append((c, added_rc))

        return control_rcs

    def controls_by_parent(self, expected_parent):
        """
        This generator selects controls in the layout which are placed inside
        the specified parent.

        Arguments:
        expected_parent -- The expected parent, which is either a control's
        internal ID or None for the layout directly.

        Yields:
        A control which is placed inside the expected parent
        """
        for c in self._controls:
            if expected_parent == c.parent_control:
                yield c


class SizeRestrictedLayoutModel(LayoutModel):
    """
    This class extends the basic layout model to describe a layout which itself
    is restricted to a fixed size. It does not define how controls are
    organized internally.

    It is used by the legacy and the hybrid layout.
    """

    _HUMAN_READABLE_LAYOUT_TYPE = "size-restricted layout"

    def __init__(self, name, title, rc):
        """
        Initializes a new size-restricted layout which contains no controls
        initially. It has a fixed size given by the rectangle 'rc'. The 'name'
        attribute specifies an internal identifier which can be used to refer
        to it (instead of referencing it directly using the object instance),
        while the 'title' attribute assigns a window title or heading to the
        layout.
        """

        self._rc = rc
        super().__init__(name, title)

    @property
    def rc(self):
        """
        Returns the size of this layout as a rectangle.
        """
        return self._rc

    def _dump_extra_properties(self):
        """
        Method implemented by derivatives of LayoutModel to print additional
        properties of a layout in the __str__() method.
        """
        return " - Rectangle dimensions: %s\n" % self._rc

    def __copy__(self):
        """
        Callback for the copy.copy() method, which performs shallow copies of
        objects.
        """
        layout = self.__class__.__new__(self.__class__)

        setattr(layout, '_name', self._name)
        setattr(layout, '_title', self._title)
        setattr(layout, '_rc', self._rc)
        return layout

    def __deepcopy__(self, memo):
        """
        Callback for the copy.deepcopy() method, which performs deep copies of
        objects.
        """
        layout = self.__class__.__new__(self.__class__)
        memo[id(self)] = layout

        setattr(layout, '_name', copy.deepcopy(self._name, memo))
        setattr(layout, '_title', copy.deepcopy(self._title, memo))
        setattr(layout, '_rc', copy.deepcopy(self._rc, memo))
        return layout


class UnorganizedLayoutModel(LayoutModel):
    """
    This class extends the basic layout model to describe a layout in which the
    controls are stored in a single list without any defined order over them.

    It is used by the legacy layout.
    """

    _HUMAN_READABLE_LAYOUT_TYPE = "unorganized layout"

    def __init__(self, name, title):
        """
        Initializes a new layout which contains no controls initially. The
        'name' attribute specifies an internal identifier which can be used to
        refer to it (instead of referencing it directly using the object
        instance), while the 'title' attribute assigns a window title or
        heading to the layout.
        """

        self._controls = []
        super().__init__(name, title)

    @property
    def controls(self):
        """
        Returns a read-only tuple of controls contained in this layout.
        """
        return tuple(self._controls)

    def __copy__(self):
        """
        Callback for the copy.copy() method, which performs shallow copies of
        objects.
        """
        layout = self.__class__.__new__(self.__class__)

        setattr(layout, '_name', self._name)
        setattr(layout, '_title', self._title)
        setattr(layout, '_controls', self._controls)
        return layout

    def __deepcopy__(self, memo):
        """
        Callback for the copy.deepcopy() method, which performs deep copies of
        objects.
        """
        layout = self.__class__.__new__(self.__class__)
        memo[id(self)] = layout

        setattr(layout, '_name', copy.deepcopy(self._name, memo))
        setattr(layout, '_title', copy.deepcopy(self._title, memo))
        setattr(layout, '_controls', copy.deepcopy(self._controls, memo))
        return layout

    def remove_control(self, control):
        """
        Removes a new control from the layout.
        """

        # Only allow positioned controls before appending to the control list
        if not isinstance(control, PositionedControl):
            raise TypeError("control must be a PositionedControl")

        self._controls.remove(control)

    def add_control(self, control):
        """
        Adds a new control to the layout.
        """

        # Only allow positioned controls before appending to the control list
        if not isinstance(control, PositionedControl):
            raise TypeError("control must be a PositionedControl")

        self._controls.append(control)

    def get_control_by_id(self, control_id):
        """
        Finds a control in the layout based on the internal ID.
        """

        result = [
            c for c in self._controls
            if c.internal_id == control_id
        ]
        return None if len(result) == 0 else result[0]


class LegacyLayoutModel(SizeRestrictedLayoutModel, UnorganizedLayoutModel):
    """
    This class extends the basic layout model to describe legacy layouts. In
    a legacy layout, the layout itself is restricted to a fixed size.

    The controls are positioned and sized according to the properties X
    coordinate, Y coordinate, width, and height. For simplicity reasons, all
    those properties are measured in pixels, although certain legacy layouts
    may use other units of measurements. To aid the optimization process, the
    height of a "line" in a legacy layout is provided.

    Controls are stored in a list without any particular order.
    """

    _HUMAN_READABLE_LAYOUT_TYPE = "legacy layout"

    def __init__(self, name, title, rc, line_height):
        """
        Initializes a new legacy layout which contains no controls initially.
        It has a fixed size given by the rectangle 'rc'. The 'name' attribute
        specifies an internal identifier which can be used to refer to it
        (instead of referencing it directly using the object instance), while
        the 'title' attribute assigns a window title or heading to the layout.
        """

        self._line_height = line_height
        super().__init__(name, title, rc)

    @property
    def line_height(self):
        """
        Returns the height of a line in this legacy layout.
        """
        return self._line_height

    def _dump_extra_properties(self):
        """
        Method implemented by derivatives of LayoutModel to print additional
        properties of a layout in the __str__() method.
        """
        return "%s - Line height: %u\n" % (super()._dump_extra_properties(),
                                           self._line_height)

    def __copy__(self):
        """
        Callback for the copy.copy() method, which performs shallow copies of
        objects.
        """
        layout = self.__class__.__new__(self.__class__)

        setattr(layout, '_name', self._name)
        setattr(layout, '_title', self._title)
        setattr(layout, '_rc', self._rc)
        setattr(layout, '_controls', self._controls)
        return layout

    def __deepcopy__(self, memo):
        """
        Callback for the copy.deepcopy() method, which performs deep copies of
        objects.
        """
        layout = self.__class__.__new__(self.__class__)
        memo[id(self)] = layout

        setattr(layout, '_name', copy.deepcopy(self._name, memo))
        setattr(layout, '_title', copy.deepcopy(self._title, memo))
        setattr(layout, '_rc', copy.deepcopy(self._rc, memo))
        setattr(layout, '_controls', copy.deepcopy(self._controls, memo))
        return layout


class GridLayoutModel(UnorganizedLayoutModel):
    """
    This class extends the basic layout model to describe grid layouts. A grid
    layout has a fixed number of columns and an unlimited number of rows.

    In the horizontal dimension, controls are placed in a column by its
    0-index, and sized using the column span -- the number of columns occupied
    by the same control over all rows it occupies.

    In the vertical dimension, controls are placed in a row by its 0-index, and
    sized using the row span -- the number of rows occupied by the same
    control over all columns it occupies.

    Controls are stored in a list of rows, which in turn are lists of controls
    sorted incrementally by their starting column.
    """

    _HUMAN_READABLE_LAYOUT_TYPE = "grid layout"


class NativeCodeLayout(ctypes.Structure):
    """
    This class implements a ctypes structure definition to represent a layout
    (legacy or grid, the storage is identical between them) in native code, as
    well as methods to translate from/to LegacyLayouts and GridLayouts.

    UITransform contains an alternative layout optimization implementation
    written in C instead of Python. It is faster, but less stable due to the
    code complexity. Also, as some Python features such as dictionaries are
    unavailable (the C code does *not* require linking against Python),
    property values have to be added directly to the structure.

    Please keep this definition in sync with the actual C code, otherwise
    crashes will be inevitable!
    """

    _fields_ = [
        ('name', ctypes.c_char_p),
        ('title', ctypes.c_char_p),
        ('rc', NativeCodeRectangle),
        ('line_height', ctypes.c_uint),
        ('control_count', ctypes.c_uint),
        ('controls', ctypes.POINTER(NativeCodeControl)),
    ]

    @staticmethod
    def convert_to(l):
        """
        Converts a {Legacy,Grid}LayoutModel to a NativeCodeLayout.

        Arguments:
        l -- {Legacy,Grid}LayoutModel to convert

        Returns:
        A new NativeCodeLayout representing the original layout
        """

        assert type(l) is LegacyLayoutModel or type(l) is GridLayoutModel

        # Encode strings as UTF-8
        arg_name = ctypes.c_char_p(l.name.encode('UTF-8'))
        arg_title = ctypes.c_char_p(l.title.encode('UTF-8'))

        # Translate size and line height
        if type(l) is LegacyLayoutModel:
            assert l.rc.h_start == 0 and l.rc.v_start == 0

            arg_rc = NativeCodeRectangle.convert_to(l.rc)
            arg_line_height = l.line_height
        else:
            arg_rc = NativeCodeRectangle(
                h_start=0, v_start=0, h_span=0, v_span=0
            )
            arg_line_height = 0

        # Translate controls...
        controls = [
            NativeCodeControl.convert_to(c)
            for c in l.controls
        ]

        # ...to an array of NativeCodeControls
        control_count = len(l.controls)
        control_pointer_type = NativeCodeControl * control_count
        arg_control_list = (control_pointer_type)(*controls)

        return NativeCodeLayout(
            name=arg_name,
            title=arg_title,
            rc=arg_rc,
            line_height=arg_line_height,
            control_count=ctypes.c_uint(control_count),
            controls=arg_control_list
        )

    @staticmethod
    def convert_from(l, make_grid):
        """
        Converts a NativeCodeLayout to a {Legacy,Grid}LayoutModel.

        Arguments:
        c -- NativeCodeLayout to convert
        make_grid -- True to create a GridLayoutModel, False to create a
        LegacyLayoutModel

        Returns:
        A new {Legacy,Grid}LayoutModel representing the original layout
        """

        assert type(l) is NativeCodeLayout
        assert type(make_grid) is bool

        # Decode strings from UTF-8
        arg_layout_name = (
            l.name.decode('UTF-8') if l.name is not None else '!NONAME!'
        )
        arg_layout_title = (
            l.title.decode('UTF-8') if l.title is not None else '!NOTITLE!'
        )

        # Create the layout without controls
        if make_grid:
            new_layout = GridLayoutModel(arg_layout_name, arg_layout_title)
            # rc and line_height are ignored
        else:
            arg_rc = NativeCodeRectangle.convert_from(l.rc)
            new_layout = LegacyLayoutModel(
                arg_layout_name, arg_layout_title, arg_rc, l.line_height
            )

        # Recreate the controls
        for i in range(0, l.control_count):
            control = l.controls[i]
            new_layout.add_control(NativeCodeControl.convert_from(control))

        return new_layout
