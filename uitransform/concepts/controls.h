/*
 * Copyright (c) 2018 Thomas Weber <pokehaxco@gmail.com>
 *
 * SPDX-License-Identifier: MIT
 */

#ifndef CONCEPTS_CONTROLS_H
#define CONCEPTS_CONTROLS_H

#include "rectangle.h"

/*
 * Types of controls which are recognized by the application stack.
 */
enum ControlType
{
    CONTROL_TYPE_UNDEFINED = 0,
    CONTROL_TYPE_PLAINTEXT = 1,
    CONTROL_TYPE_GROUPBOX = 2,
    CONTROL_TYPE_BUTTON = 3,
    CONTROL_TYPE_CHECKBOX = 4,
    CONTROL_TYPE_RADIOBUTTON = 5,
    CONTROL_TYPE_IMAGE = 6,
    CONTROL_TYPE_TEXTBOX = 7,
    CONTROL_TYPE_COMBOBOX = 8,
    CONTROL_TYPE_PLACEHOLDER = 255
};

/*
 * Chooses what is displayed inside a button.
 */
enum ButtonContent
{
    BUTTON_CONTENT_TEXT = 0,
    BUTTON_CONTENT_BITMAP = 1,
    BUTTON_CONTENT_ICON = 2
};

/*
 * Chooses what is displayed inside a textbox.
 */
enum TextboxContent
{
    TEXTBOX_CONTENT_TEXT = 0,
    TEXTBOX_CONTENT_NUMBER = 1,
    TEXTBOX_CONTENT_PASSWORD = 2
};

/*
 * Horizontal alignment. Mainly intended for text; it can be aligned to the
 * left, to the right, or to the center of the block it is contained in.
 */
enum HorizontalAlignment
{
    HORIZONTAL_ALIGNMENT_UNDEFINED = 0,
    HORIZONTAL_ALIGNMENT_LEFT = 1,
    HORIZONTAL_ALIGNMENT_CENTER = 2,
    HORIZONTAL_ALIGNMENT_RIGHT = 3
};

/*
 * A control is a user interface element which can be interacted with. Its type
 * defines its function: it can act as a button, input field, etc.
 * It contains various identification-related properties for both internal and
 * external use. Contains may have a relation to a parent control, while some
 * types of controls act as container for further controls.
 *
 * In a legacy layout, the position is defined by X and Y coordinates, and the
 * size by width and height; all properties are measured in pixels for
 * simplicity. In a grid layout, the position is defined by row and column
 * number, and the size is defined by row span and column span.
 */
struct Control
{
    unsigned int internal_id;
    enum ControlType ctype;
    const char *original_id;
    const char *class_string;
    struct Rectangle rc;
    int parent_control;

    /* Only known properties are supported because there is no dictionary */
    enum HorizontalAlignment property_alignment;
    enum ButtonContent property_button_content;
    enum TextboxContent property_textbox_content;
    const char *property_caption_value;
    const char *property_text_value;
    bool property_is_default;
    bool property_is_disabled;
    bool property_is_tristate;
    bool property_is_multiline;
};

/* XXX: control_copy_deep() */

/*
 * Copies the control 'source' to 'dest' without copying strings.
 */
static inline void control_copy_no_strings(struct Control *restrict dest,
    const struct Control *restrict source)
{
    /* We copy string pointers, so this is sufficient */
    *dest = *source;
}

/* XXX: control_free() */

void control_str(const struct Control *restrict control);

static inline bool control_is_container(const struct Control *restrict c)
{
    return c->ctype == CONTROL_TYPE_GROUPBOX;
}

#endif
