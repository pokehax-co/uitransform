/*
 * Copyright (c) 2018 Thomas Weber <pokehaxco@gmail.com>
 *
 * SPDX-License-Identifier: MIT
 */

#ifndef CONFIGURABLES_H
#define CONFIGURABLES_H

/*
 * Category: Metrics -- Order of controls
 *
 * Weights of the eight possible control order iterations, required for
 * calculating the weighted average over the Levenshtein distances. Higher
 * weights shift the average towards the corresponding values.
 */
#define CONFIG_CONTROL_ORDER_METRIC_WEIGHTS \
    50,     /* Western (left-to-right horizontal, from top) */ \
    2,      /* Right-to-left horizontal, from top */ \
    10,     /* Arabic (right-to-left horizontal, from top) */ \
    5,      /* East Asian (right-to-left vertical, from top) */ \
    1,      /* Left-to-right horizontal, from bottom */ \
    1,      /* Left-to-right vertical, from bottom */ \
    1,      /* Right-to-left horizontal, from bottom */ \
    1       /* Right-to-left vertical, from bottom */

/*
 * Category: Evolutionary optimization
 *
 * Size of the population evolved during the optimization process.
 */
#define CONFIG_OPTIMIZE_POPULATION_SIZE 30

/*
 * Category: Evolutionary optimization
 *
 * Size of the mating pool created for the reproduction of a new population
 * during the optimization process.
 */
#define CONFIG_OPTIMIZE_MATING_POOL_SIZE 22

/*
 * Category: Evolutionary optimization
 *
 * Chance between 0 and 1 that an individual from the mating pool is selected
 * for recombination. Encourages searches within the individuals' vicinity.
 */
#define CONFIG_OPTIMIZE_RECOMBINATION_CHANCE 0.5

/*
 * Category: Evolutionary optimization
 *
 * Chance between 0 and 1 that an individual from the mating pool or the new
 * population is selected for mutation. Encourages searches over greater
 * distances.
 */
#define CONFIG_OPTIMIZE_MUTATION_CHANCE 0.15

/*
 * Category: Concepts
 *
 * A grid layout has a fixed number of columns, at least in this application.
 *
 * The default value is 12 columns, for compatibility with the Bootstrap
 * framework.
 */
#define CONFIG_GRID_LAYOUT_COLUMN_COUNT 12

#endif
