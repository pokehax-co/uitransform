/*
 * Copyright (c) 2018 Thomas Weber <pokehaxco@gmail.com>
 *
 * SPDX-License-Identifier: MIT
 */

#ifndef TOOLS_H
#define TOOLS_H

#include <stdbool.h>

/*
 * Initializes the random number generator.
 */
void tools_initialize_random(void);

/*
 * Generates a random boolean value (true or false).
 */
bool tools_randbool(void);

/*
 * Generates a random integer between low and high.
 */
int tools_randint(int low, int high);

/*
 * Generates a random floating-point value between 0 and 1.
 */
float tools_random(void);

#endif
