#
# Copyright (c) 2018 Thomas Weber <pokehaxco@gmail.com>
#
# SPDX-License-Identifier: MIT
#

import os
import sys


def log_error(s, *args):
    """
    Prints an error message to standard error with an appropriate prefix.

    Arguments:
    s -- String to be written, it will be terminated with a newline character
    *args -- Any additional arguments to be passed to a format() call.
    """
    sys.stderr.write(
        "%s: error: %s\n" % (os.path.basename(sys.argv[0]),
                             s if len(args) == 0 else s.format(*args))
    )


def log_warning(s, *args):
    """
    Prints a warning message to standard error with an appropriate prefix.

    Arguments:
    s -- String to be written, it will be terminated with a newline character
    *args -- Any additional arguments to be passed to a format() call.
    """
    sys.stderr.write(
        "%s: warning: %s\n" % (os.path.basename(sys.argv[0]),
                               s if len(args) == 0 else s.format(*args))
    )


def log_note(s, *args):
    """
    Prints a note message to standard output with an appropriate prefix.

    Arguments:
    s -- String to be written, it will be terminated with a newline character
    *args -- Any additional arguments to be passed to a format() call.
    """
    sys.stderr.write(
        "%s: note: %s\n" % (os.path.basename(sys.argv[0]),
                            s if len(args) == 0 else s.format(*args))
    )
