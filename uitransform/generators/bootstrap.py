#
# Copyright (c) 2018 Thomas Weber <pokehaxco@gmail.com>
#
# SPDX-License-Identifier: MIT
#

import os
import uitransform.configurables as config

from uitransform.concepts import Rectangle, ControlType
from uitransform.tools import log_note

from sortedcontainers import SortedDict, SortedList

# Rename the imports so the imported components can be distinguished from
# all classes provided by UITransform
from mako.lookup import TemplateLookup as MakoTemplateLookup

# HTML beautification is optional
try:
    from bs4 import BeautifulSoup
except ImportError:
    BeautifulSoup = None


# The HTML templates are located in this directory. Initialize template lookup
# from all available templates.
_templates_path = os.path.dirname(__file__) + "/bootstrap_templates"
_templates_lookup = MakoTemplateLookup(directories=[_templates_path],
                                       input_encoding='UTF-8')


class BootstrapHTMLControl(object):
    """
    This class contains static methods to generate HTML code for controls,
    based on the Bootstrap framework (https://getbootstrap.com/).

    (In control templates, the class properties must be referenced with the
    underscore prefix, as the property methods in the class do not work.=
    """

    @staticmethod
    def not_container(control, basename):
        """
        Generates placeholder HTML code for a non-container control.

        Arguments:
        control -- Control to generate HTML code for
        basename -- Name of the template to apply
        """

        page_template = _templates_lookup.get_template(
            '/control_%s.html' % basename)
        return page_template.render(**control.__dict__)

    @staticmethod
    def placeholder(control):
        """
        Generates placeholder HTML code for the specified control.

        Arguments:
        control -- Control to generate HTML code for
        """

        page_template = _templates_lookup.get_template(
            '/control_placeholder.html')
        return page_template.render(**control.__dict__)

    @staticmethod
    def groupbox(layout, control):
        """
        Generates HTML code for the specified groupbox control.

        Arguments:
        layout -- Parent layout of this control
        control -- Control to generate HTML code for
        """

        # A group box is a container control. Use the internal ID of the box
        # to add controls as a new grid to the box.
        generator = BootstrapHTMLGenerator()
        sublayout_text = generator.generate_grid(layout, control.internal_id)

        # Load template and render it
        page_template = _templates_lookup.get_template(
            '/control_groupbox.html')
        return page_template.render(sublayout=sublayout_text,
                                    **control.__dict__)

    @staticmethod
    def plaintext(control):
        """
        Generates HTML code for simple text.

        Arguments:
        control -- Control to generate HTML code for
        """

        page_template = _templates_lookup.get_template(
            '/control_plaintext.html')
        return page_template.render(**control.__dict__)

    @staticmethod
    def button(control):
        """
        Generates HTML code for the specified button control.

        Arguments:
        layout -- Parent layout of this control
        control -- Control to generate HTML code for
        """

        page_template = _templates_lookup.get_template(
            '/control_button.html')
        return page_template.render(**control.__dict__)

    @staticmethod
    def checkbox(control):
        """
        Generates HTML code for the specified checkbox control.

        Arguments:
        layout -- Parent layout of this control
        control -- Control to generate HTML code for
        """

        page_template = _templates_lookup.get_template(
            '/control_checkbox.html')
        return page_template.render(**control.__dict__)

    @staticmethod
    def radiobutton(control):
        """
        Generates HTML code for the specified radio button control.

        Arguments:
        layout -- Parent layout of this control
        control -- Control to generate HTML code for
        """

        page_template = _templates_lookup.get_template(
            '/control_radiobutton.html')
        return page_template.render(**control.__dict__)

    @staticmethod
    def textbox(control):
        """
        Generates HTML code for the specified textbox control.

        Arguments:
        layout -- Parent layout of this control
        control -- Control to generate HTML code for
        """

        page_template = _templates_lookup.get_template(
            '/control_textbox.html')
        return page_template.render(**control.__dict__)

    @staticmethod
    def image(control):
        """
        Generates HTML code for the specified image control.

        Arguments:
        layout -- Parent layout of this control
        control -- Control to generate HTML code for
        """

        page_template = _templates_lookup.get_template(
            '/control_image.html')
        return page_template.render(**control.__dict__)

    @staticmethod
    def generate(layout, control):
        """
        Generates HTML code for the specified control.

        Arguments:
        control -- Control to generate HTML code for
        """

        # Decide by control type
        if control.ctype == ControlType.PLAINTEXT:
            return BootstrapHTMLControl.plaintext(control)
        elif control.ctype == ControlType.GROUPBOX:
            return BootstrapHTMLControl.groupbox(layout, control)
        elif control.ctype == ControlType.BUTTON:
            return BootstrapHTMLControl.button(control)
        elif control.ctype == ControlType.CHECKBOX:
            return BootstrapHTMLControl.checkbox(control)
        elif control.ctype == ControlType.RADIOBUTTON:
            return BootstrapHTMLControl.radiobutton(control)
        elif control.ctype == ControlType.TEXTBOX:
            return BootstrapHTMLControl.textbox(control)
        elif control.ctype == ControlType.IMAGE:
            return BootstrapHTMLControl.image(control)
        elif control.ctype == ControlType.COMBOBOX:
            return BootstrapHTMLControl.not_container(control, 'combobox')

        # Must not be undefined
        elif control.ctype == ControlType.UNDEFINED:
            raise RuntimeError("attempt to generate control of type UNDEFINED")

        # Fall back on placeholder
        else:
            return BootstrapHTMLControl.placeholder(control)


class BootstrapHTMLGenerator(object):
    """
    This class implements a layout generator which exports a grid layout to
    HTML code. The grid layout is supported through the Bootstrap framework
    (https://getbootstrap.com/).
    """

    def __init__(self):
        """
        Initializes a new generator which exports a grid layout to Bootstrap
        based HTML code.
        """

        self._current_position = None

    def generate_control(self, layout, control):
        """
        Generates the core HTML source for a control and returns it. This is
        just a wrapper around BootstrapHTMLControl.generate() because of a
        conflict between line limit and forced indent by flake8.

        Arguments:
        layout -- The grid layout we're acting on
        control -- Control to generate code for

        Returns:
        String containing HTML code for the control
        """

        return BootstrapHTMLControl.generate(layout, control)

    def generate_grid_column(self, layout, layout_column):
        """
        Generates the core HTML source for a grid layout column and returns it.
        The HTML code works with Bootstrap's grid layout implementation. As the
        original legacy layouts were intended for desktops only, the 'col-md'
        width classes (which are intended for desktops) are used for layouting.

        Arguments:
        layout -- The grid layout we're acting on
        layout_cell -- Grid cell to generate code for

        Returns:
        String containing HTML code for the entire cell (and its contents)
        """

        # Select a "first" control if there are multiple controls in the same
        # column.
        first_control = layout_column[0]

        # Determine distance between the current column and the current
        # control's column. If positive, we need to insert an offset class for
        # the control to place it at the right position.
        cell_offset = first_control.rc.h_start - self._current_position.h_start

        # Use the control's column span as the cell width
        cell_width = first_control.rc.h_span

        # Generate code for the controls
        control_text = '\n'.join(map(
            lambda x: self.generate_control(layout, x),
            layout_column
        ))

        # Wrap it into the grid column template
        page_template = _templates_lookup.get_template('/grid_column.html')
        output_text = page_template.render(width=cell_width,
                                           offset=cell_offset,
                                           control_content=control_text)

        # Advance cursor
        self._current_position.h_start = first_control.rc.h_start + cell_width

        return output_text

    def generate_grid_row(self, layout, layout_row):
        """
        Generates the core HTML source for a grid layout row and returns it.
        The HTML code works with Bootstrap's grid layout implementation.

        Arguments:
        layout -- The grid layout we're acting on
        layout_row -- Grid line to generate code for

        Returns:
        String containing HTML code for the entire row (and its contents)
        """

        # Iterate controls in line. They are guaranteed to be sorted by their
        # starting column, so that the following code just works.
        column_code = [
            self.generate_grid_column(layout, layout_row[column])
            for column in layout_row.keys()
        ]

        # Concatenate columns
        layout_text = '\n'.join(column_code)

        # Wrap into a grid row
        page_template = _templates_lookup.get_template('/grid_row.html')
        output_text = page_template.render(grid_columns=layout_text)

        # Concatenate rows
        return output_text

    def generate_grid(self, layout, parent_id=None):
        """
        Generates the core HTML source for a grid layout and returns it. The
        HTML code works with Bootstrap's grid layout implementation.
        """

        #
        # Bootstrap makes it difficult to simulate row span with its grid
        # system (which would be easy with CSS Grid Layout). In particular, to
        # make a cell span two rows, we would need to insert additional cells
        # in non-rowspan controls by nesting a row.
        #
        # Input example:
        # 101 DIALOG 0, 0, 225, 192
        # CAPTION "Shifted lines test"
        # {
        #    CONTROL "", 201, BUTTON, BS_GROUPBOX | WS_CHILD | WS_VISIBLE,
        #      120, 50, 100, 100
        #    CONTROL "", 202, BUTTON, BS_GROUPBOX | WS_CHILD | WS_VISIBLE,
        #      5, 5, 100, 100
        # }
        #
        # Required HTML code to restore the row overlap:
        # <div class="row-fluid">
        #   <div class="col-md-6">
        #     <div class="row">
        #       Left Block<br/>Multiline
        #     </div>
        #   </div>
        #   <div class="col-md-6" style="padding:0">
        #     &nbsp;<br/>
        #     Right Block<br/>Multiline
        #   </div>
        # </div>
        #
        # What would have been three lines in the row divider, is now a single
        # line where the left column is declared normally, broken over two
        # lines, and the right column begins with an empty line before the
        # actual control starts. When the columns are collapsed on low width,
        # the empty lines remain visible.
        #
        # Compare with CSS Grid Layout, which produces a correct result:
        # .grid-container {
        #     display: grid;
        #     grid-template-columns: repeat(12, 1fr);
        #     grid-template-rows: repeat(3, auto);
        # }
        # .grid-a { grid-column: 1 / 7; grid-row: 1 / 3; }
        # .grid-b { grid-column: 7 / 13; grid-row: 2 / 4; }
        # <div class="grid-container">
        #     <div class="grid-a replacement-control">Left block</div>
        #     <div class="grid-b replacement-control">Right block</div>
        # </div>
        #
        # With some more code, the input above can also be replicated with
        # HTML tables. Both CSS Grid and HTML tables have the disadvantage that
        # custom CSS code has to be produced in order to adapt to different
        # devices.
        #
        # To avoid technology changes, the row span is ignored entirely and is
        # assumed to be 1 (don't span more than a single row). Container
        # controls will have a larger height because they contain controls.
        #

        # Filter the control grid according to the visual parent
        filtered_controls = layout.controls_by_parent(parent_id)

        #
        # Organize the controls in multidimensional dictionaries:
        # - A dictionary to store all rows sorted by row number
        # - For each row, a dictionary to store all columns sorted by column
        #   number
        # - For each cell, a list of controls sorted by size
        #
        organized_controls = SortedDict()
        for c in filtered_controls:
            row_key = c.rc.v_start

            # If the row doesn't exist, we need to create everything
            if row_key not in organized_controls:
                controls_in_cell = SortedList(
                    [c], key=lambda x: x.rc.h_span * x.rc.v_span
                )
                cells_in_row = SortedDict()
                cells_in_row[c.rc.h_start] = controls_in_cell
                organized_controls[row_key] = cells_in_row

            # The row exists, check for the column
            else:
                column_key = c.rc.h_start

                # If the column doesn't exist, we need to create the list
                if column_key not in organized_controls[row_key]:
                    controls_in_cell = SortedList(
                        [c], key=lambda x: x.rc.h_span * x.rc.v_span
                    )
                    organized_controls[row_key][column_key] = controls_in_cell

                # Add the control to the existing cell
                else:
                    organized_controls[row_key][column_key].add(c)

        # We need to track the current position. Start at the first row and
        # the first column.
        self._current_position = Rectangle(0, 0, None, None)

        # Iterate grid layout lines
        row_code = []
        for row in organized_controls.keys():
            # Adjust cursor
            self._current_position.h_start = 0
            self._current_position.v_start = row

            assert len(organized_controls[row]) > 0

            # Generate code for this row
            row_code.append(self.generate_grid_row(
                layout, organized_controls[row]
            ))

        # Concatenate rows
        layout_text = '\n'.join(row_code) if len(row_code) > 0 else ''

        # Wrap into a grid template
        page_template = _templates_lookup.get_template('/grid.html')
        output_text = page_template.render(grid_rows=layout_text)

        # Concatenate rows
        return output_text

    def generate(self, grid_layout, destination_dir, name_suffix=''):
        """
        Generates HTML code for the specified layout, and writes it into the
        specified destination directory.
        """

        # Descend into the grid layout. Start with those controls which do not
        # have a parent container, i.e. the layout itself is their parent.
        layout_text = self.generate_grid(grid_layout, None)

        # Wrap the grid layout into a HTML page and export it
        page_template = _templates_lookup.get_template('/page.html')
        output_text = page_template.render(
            page_title=grid_layout.title,
            grid_layout=layout_text,
            debug=config.BOOTSTRAP_GRID_DEBUGGING)

        # Beautify the output if Beautiful Soup is installed
        if BeautifulSoup is not None:
            bs = BeautifulSoup(output_text, 'html5lib')
            output_text = bs.prettify()

        # Export to a HTML file:
        output_file = os.path.join(destination_dir,
                                   grid_layout.name + name_suffix + '.html')
        with open(output_file, "wb") as f:
            f.write(output_text.encode('UTF-8'))

        log_note("exported grid layout '{0}' to '{1}'", grid_layout.name,
                 output_file)
