#
# Copyright (c) 2018 Thomas Weber <pokehaxco@gmail.com>
#
# SPDX-License-Identifier: MIT
#

from .bootstrap import BootstrapHTMLGenerator

# Export all classes from this module
__all__ = [
    'BootstrapHTMLGenerator'
]
