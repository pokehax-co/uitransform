#
# Copyright (c) 2018 Thomas Weber <pokehaxco@gmail.com>
#
# SPDX-License-Identifier: MIT
#

#
# This module contains algorithms to measure various aspects of a layout. To
# maintain a high detail of information, they operate on legacy layouts only.
# However, grid layouts are also supported when given the maximum number of
# rows in the layout.
#

import abc
import math
import operator
import numpy
import uitransform.configurables as config

from uitransform.concepts import (
    Rectangle, ControlType, LegacyLayoutModel, GridLayoutModel
)


class OptimizationMeasurement(abc.ABC):
    """
    This abstract class is the base of all measurement algorithms.
    """

    @staticmethod
    def argument_check(layout, legacy_layout_ref=None):
        """
        Verifies the input arguments before beginning a measurement.

        Arguments:
        layout -- Layout to measure. Must be a legacy or a grid layout.
        legacy_layout_ref -- A reference legacy layout used to span the grid
        layout over the area of the legacy layout. Required if a grid layout is
        to be measured.
        """

        assert type(layout) in (LegacyLayoutModel, GridLayoutModel)
        if type(layout) is GridLayoutModel:
            assert type(legacy_layout_ref) is LegacyLayoutModel

    @staticmethod
    @abc.abstractmethod
    def measure(layout, legacy_layout_ref=None):
        """
        Measures something in the specified layout. If the layout is a grid
        layout, the maximum number of rows is required. In derived classes, the
        measurement must begin with
            OptimizationMeasurement.argument_check(layout, legacy_layout_ref)
        to verify the input arguments.

        Arguments:
        layout -- Layout to measure. Must be a legacy or a grid layout.
        legacy_layout_ref -- A reference legacy layout used to span the grid
        layout over the area of the legacy layout. Required if a grid layout is
        to be measured.

        Returns:
        The value of the measurement
        """
        raise NotImplementedError(
            "Method called from the OptimizationMeasurement abstract class"
        )


def rcs_in_viewport(layout, legacy_layout_ref=None):
    """
    Scales all control rectangles in grid layouts so that the layout fits
    entirely in its viewport.

    Arguments:
    layout -- Layout to project. If this parameter is a legacy layout, only
    information extraction will be performed, no scaling.
    legacy_layout_ref -- If the input is a grid layout, this is the legacy
    layout onto which the grid layout is projected.

    Returns:
    Tuple (layout_width, layout_height, list_of_control_rectangles)

    Remarks:
    For group box controls, 4 rectangles covering only its border are created.
    The projected layout has a height of a multiple of the legacy layout's line
    height.
    """

    if type(layout) is LegacyLayoutModel:
        layout_width = layout.rc.h_span
        layout_height = layout.rc.v_span

        horizontal_scale = 1
        vertical_scale = 1
    else:
        # Formula used initially to derive the grid layout's dimensions from
        # the original legacy layout
        row_count = int(numpy.ceil(
            numpy.float64(legacy_layout_ref.rc.v_span) /
            numpy.float64(legacy_layout_ref.line_height)
        ))

        # Use the viewport size configurable
        layout_width, layout_height = config.GRID_LAYOUT_VIEWPORT_SIZE
        horizontal_scale = (
            numpy.float64(layout_width) / config.GRID_LAYOUT_COLUMN_COUNT
        )
        vertical_scale = (
            numpy.float64(layout_height) / row_count
        )

    # Iterate over the rectangles and scale them
    control_rcs = []
    t = config.GROUPBOX_BORDER_WIDTH
    for c, absolute_rc in layout.get_control_rectangles():
        scaled_rc = Rectangle(
            absolute_rc.h_start * horizontal_scale,
            absolute_rc.v_start * vertical_scale,
            absolute_rc.h_span * horizontal_scale,
            absolute_rc.v_span * vertical_scale
        )

        # Handle group boxes. If the area is large enough so that a border can
        # be created by using 4 rectangles, do so.
        if c.ctype == ControlType.GROUPBOX and \
           absolute_rc.h_span >= 2 * t and absolute_rc.h_span >= 2 * t:
            # Create four rectangles instead of one
            control_rcs.append(Rectangle(
                scaled_rc.h_start,
                scaled_rc.v_start,
                scaled_rc.h_span, t
            ))
            control_rcs.append(Rectangle(
                scaled_rc.h_start,
                scaled_rc.v_start + scaled_rc.v_span - t,
                scaled_rc.h_span, t
            ))
            control_rcs.append(Rectangle(
                scaled_rc.h_start,
                scaled_rc.v_start + t,
                t, scaled_rc.v_span - 2 * t
            ))
            control_rcs.append(Rectangle(
                scaled_rc.h_start + scaled_rc.h_span - t,
                scaled_rc.v_start + t,
                t, scaled_rc.v_span - 2 * t
            ))
        else:
            # Only add the calculated rectangle
            control_rcs.append(scaled_rc)

    return (layout_width, layout_height, control_rcs)


class RectangleAreaOccupation(object):
    """
    This class implements algorithms to calculate the total area occupied by
    rectangles, which are allowed to overlap, inside a bounding box. This
    implementation is derived from Python code on https://redd.it/zaa0v, which
    uses a different rectangle specification than the Rectangle class, namely a
    tuple like:
        (h-min-coord, v-min-coord, h-max-coord, v-max-coord)
    """

    def __init__(self, input_rects):
        """
        Initializes the algorithm with a list of 4-tuples declaring rectangles.
        The implementation is set up this way to allow calling the algorithm
        with different bounding boxes without recomputing the rectangles each
        time.

        Arguments:
        input_rects -- List of 4-tuple rectangles to calculate the occupied
        area for
        """

        # Check whether input_rects can be iterated. It will throw a TypeError
        # if it isn't.
        iter(input_rects)

        # Handle lists of Rectangles. They have to be translated into the
        # format accepted by the algorithm.
        try:
            # Assume that input_rects[0] is a Rectangle
            self._input_rects = [
                (rc.h_start, rc.v_start, rc.h_end, rc.v_end)
                for rc in input_rects
            ]
        except AttributeError:
            # It's a 4-tuple, the format we expect
            self._input_rects = input_rects

    @staticmethod
    def _compute_bounding_box(rects):
        """
        Calculates the bounding box which fits all rectangles given as input.
        The bounding box has the smallest left and top, and the largest right
        and bottom values among all rectangles.

        Arguments:
        rects -- Rectangles to calculate bounding box for

        Returns:
        The bounding box (left, top, right, bottom)
        """
        return (
            min(v[0] for v in rects), min(v[1] for v in rects),
            max(v[2] for v in rects), max(v[3] for v in rects)
        )

    @staticmethod
    def _clip(bounding_box, rects):
        """
        Clips the list of rectangles against the bounding box, and returns a
        new list of rectangles.

        Arguments:
        bounding_box -- Rectangle to clip the list of rectangles against
        rects -- List of rectangles to clip

        Returns:
        List of rectangles which at least partially overlap with the bounding
        box, with coordinates clipped to the bounding box's area
        """

        # Unpack bounding box coordinates
        (a1, b1, a2, b2) = bounding_box

        # Go over each rectangle, and clip each of the coordinates against the
        # target box as long as the rectangle still remains inside bounding_box
        # (otherwise it is excluded).
        return [
            (max(a1, x1), max(b1, y1), min(a2, x2), min(b2, y2))
            for (x1, y1, x2, y2) in rects
            if a1 < x2 and a2 > x1 and y1 < b2 and y2 > b1
        ]

    @staticmethod
    def _cover(bounding_box, rects):
        """
        Calculates the area occupied by the rectangles 'rects'. The rectangles
        all fit in the rectangle 'bounding_box'.

        Arguments:
        bounding_box -- Rectangle specifying maximum limits of the calculation
        area
        rects -- List of rectangles to calculate occupied area for

        Returns:
        Area occupied by the list of rectangles
        """

        # Have rectangles? If not, then the area is 0
        if not rects:
            return 0

        # Pop the first rectangle off
        first_rc = rects[0]
        remaining_rcs = rects[1:]

        # Unpack bounding box and first rectangle
        (a1, b1, a2, b2) = bounding_box
        (x1, y1, x2, y2) = first_rc

        #
        # Create four new rectangles (bounding box is a, the rectangle is b):
        #                      +-----------+
        #                      |ttttttttttt|
        #                      |lll+---+rrr|
        #                      |lll|   |rrr|
        #                      |lll+---+rrr|
        #                      |bbbbbbbbbbb|
        #                      +-----------+
        #
        first_rc_inversion_parts = [
            (a1, y2, a2, b2),   # b
            (a1, b1, a2, y1),   # t
            (a1, y1, x1, y2),   # l
            (x2, y1, a2, y2),   # r
        ]

        # The area is the area of the first rectangle, plus the total area of
        # all remaining rectangles clipped against the four rectangles created
        # above.
        return (
            (first_rc[2] - first_rc[0]) * (first_rc[3] - first_rc[1]) +
            sum(
                RectangleAreaOccupation._cover(
                    x,
                    RectangleAreaOccupation._clip(x, remaining_rcs)
                )
                for x in first_rc_inversion_parts
            )
        )

    def calculate(self, bounding_box=None):
        """
        Calculates the area occupied by the rectangles input_rects passed to
        the class initializer within the specified bounding box. If no bounding
        box is specified, a new bounding box spanning all input_rects will be
        created so that just the total occupied area is calculated.

        Arguments:
        bounding_box -- Bounding box to clip rectangles against

        Returns:
        Number of pixels occupied by the rectangles
        """

        # Compute the bounding box if not specified
        if bounding_box is None:
            bounding_box = RectangleAreaOccupation._compute_bounding_box(
                self._input_rects
            )

        # Translate the bounding box format, if necessary
        elif isinstance(bounding_box, Rectangle):
            bounding_box = (
                bounding_box.h_start, bounding_box.v_start,
                bounding_box.h_start + bounding_box.h_span,
                bounding_box.v_start + bounding_box.v_span
            )

        # Calculate the area occupied by the rectangles within the bounding box
        return RectangleAreaOccupation._cover(
            bounding_box,
            RectangleAreaOccupation._clip(bounding_box, self._input_rects)
        )


class WhitespaceRatioMeasurement(OptimizationMeasurement):
    """
    This class measures the whitespace ratio of a layout. The whitespace ratio
    is the ratio of the total area occupied by all controls against the total
    area of the layout. It takes overlapping controls into account.
    """

    @staticmethod
    def measure(layout, legacy_layout_ref=None):
        """
        Measures the whitespace ratio in the specified layout.

        Arguments:
        layout -- Layout to measure. Must be a legacy or a grid layout.
        legacy_layout_ref -- A reference legacy layout used to span the grid
        layout over the area of the legacy layout. Required if a grid layout is
        to be measured.

        Returns:
        Whitespace ratio of the measurement, between 0 and 1
        """

        # Check arguments
        OptimizationMeasurement.argument_check(layout, legacy_layout_ref)

        # Take legacy layouts as-is, but scale the grid layout to the size of
        # its viewport
        layout_width, layout_height, control_rcs = rcs_in_viewport(
            layout, legacy_layout_ref
        )
        calculator = RectangleAreaOccupation(control_rcs)

        # Calculate the area occupied by the controls in the entire layout
        entire_layout_control_occupation = calculator.calculate(
            (0, 0, layout_width, layout_height)
        )

        # Divide it by the total area of the layout
        return (
            numpy.float64(entire_layout_control_occupation) /
            numpy.float64(layout_width * layout_height)
        )


class DensityMeasurement(OptimizationMeasurement):
    """
    This class contains common definitions and method for measuring the density
    of a layout, which involves partitioning the layout in several parts of
    equal size.
    """

    @staticmethod
    def _divide_and_measure_controls(layout, legacy_layout_ref):
        """
        Divides the input layout into a number of equally-sized parts and
        calculates the area occupied by controls in each part.

        Arguments:
        layout -- Layout to measure. Must be a legacy or a grid layout.
        legacy_layout_ref -- A reference legacy layout used to span the grid
        layout over the area of the legacy layout. Required if a grid layout is
        to be measured.

        Returns:
        A tuple (entire_occupation, part_occupation). entire_occupation is the
        total area occupied by controls in the layout, while part_occupation
        is a list of areas occupied by controls in each part of the layout.
        """

        # Take legacy layouts as-is, but scale the grid layout to the size of
        # its viewport
        layout_width, layout_height, control_rcs = rcs_in_viewport(
            layout, legacy_layout_ref
        )
        calculator = RectangleAreaOccupation(control_rcs)

        # We need to calculate the total occupied area in the whole layout
        entire_layout_control_occupation = calculator.calculate(
            (0, 0, layout_width, layout_height)
        )

        # The number of segments must be a square number, so that we can divide
        # the layout into sqrt(DENSITY_LAYOUT_DIVISIONS)^2 parts.
        # Also, for the measurements to be effective, more than a single part
        # has to be created.
        segment_count = config.DENSITY_LAYOUT_DIVISIONS
        segment_count_sqrt = int(math.sqrt(segment_count) + 0.5)
        assert segment_count > 1 and segment_count_sqrt ** 2 == segment_count

        # Divide the layout area into the specified number of parts. Each part
        # has exactly the same size.
        part_width, part_height = (
            numpy.float64(layout_width) / segment_count_sqrt,
            numpy.float64(layout_height) / segment_count_sqrt
        )
        layout_area_parts = [
            Rectangle((i % 4) * part_width, (i // 4) * part_height,
                      part_width, part_height)
            for i in range(segment_count)
        ]

        # Calculate the area occupied by controls in each part
        part_control_occupations = [
            calculator.calculate(part) for part in layout_area_parts
        ]

        # Measurement is complete
        return (entire_layout_control_occupation, part_control_occupations)


class HerfindahlDensityMeasurement(DensityMeasurement):
    """
    This class measures the density of a layout. The layout is divided into
    equally-sized parts. For each part, the whitespace ratio (or rather the
    occupied area before the final division) is calculated and used as input
    to determine the Herfindahl index, the value of this measurement.
    """

    @staticmethod
    def measure(layout, legacy_layout_ref=None):
        """
        Measures the density of the specified layout by dividing the layout in
        smaller regions, calculating the control-occupied area, and calculating
        the Herfindahl index for those.

        Arguments:
        layout -- Layout to measure. Must be a legacy or a grid layout.
        legacy_layout_ref -- A reference legacy layout used to span the grid
        layout over the area of the legacy layout. Required if a grid layout is
        to be measured.

        Returns:
        Normalized Herfindahl index between 0 and 1

        Remarks:
        Compare with https://en.wikipedia.org/wiki/Herfindahl_index
        """

        # Check arguments
        OptimizationMeasurement.argument_check(layout, legacy_layout_ref)

        # Divide and measure occupied areas
        (entire_layout_control_occupation, control_occupations) = \
            DensityMeasurement._divide_and_measure_controls(
                layout, legacy_layout_ref
            )
        assert len(control_occupations) == config.DENSITY_LAYOUT_DIVISIONS

        #
        # The layout parts are treated as firms, which in total produce 100%
        # of the area occupied by controls in the layout. The percentage of
        # each part is used as input for the index.
        #
        herfindahl = numpy.sum([
            numpy.square(part_occupation / entire_layout_control_occupation)
            for part_occupation in control_occupations
        ])

        # The Herfindahl index is in the range [1/N, N] where N is the number
        # of firms accounted for in the index calculation. Normalize it to
        # [0, 1], which is the range I prefer for evaluating differences
        # between layouts.
        reciprocal_of_parts = numpy.reciprocal(
            numpy.float64(config.DENSITY_LAYOUT_DIVISIONS)
        )
        return (
            (herfindahl - reciprocal_of_parts) /
            (numpy.float64(1) - reciprocal_of_parts)
        )


class GiniDensityMeasurement(DensityMeasurement):
    """
    This class measures the density of a layout. The layout is divided into
    equally-sized parts. For each part, the whitespace ratio (or rather the
    occupied area before the final division) is calculated and used as input
    to determine the Gini coefficient, the value of this measurement.
    """

    @staticmethod
    def measure(layout, legacy_layout_ref=None):
        """
        Measures the density of the specified layout by dividing the layout in
        smaller regions, calculating the control-occupied area, and calculating
        the Gini coefficient for those.

        Arguments:
        layout -- Layout to measure. Must be a legacy or a grid layout.
        legacy_layout_ref -- A reference legacy layout used to span the grid
        layout over the area of the legacy layout. Required if a grid layout is
        to be measured.

        Returns:
        Gini coefficient between 0 and 1

        Remarks:
        Compare with https://de.wikipedia.org/wiki/Gini-Koeffizient
        """

        # Check arguments
        OptimizationMeasurement.argument_check(layout, legacy_layout_ref)

        # Each part is 1/N'th of the layout area
        relative_part_area = numpy.reciprocal(
            numpy.float64(config.DENSITY_LAYOUT_DIVISIONS)
        )

        # Divide and measure occupied areas
        (entire_layout_control_occupation, control_occupations) = \
            DensityMeasurement._divide_and_measure_controls(
                layout, legacy_layout_ref
            )

        #
        # Create the tuple array (b_i, v_i), where v_i is the area occupied by
        # controls in a part of the layout, and b_i is the size of the area
        # part.
        #
        # Normalize by dividing b_i with the total area occupied by controls in
        # the entire layout, and by replacing v_i with the portion of the
        # layout it occupies (example: if there are 16 parts, then v_i = 1/16).
        #
        distribution_mapping = [
            (relative_part_area, o / entire_layout_control_occupation)
            for o in control_occupations
        ]

        # Sort the mapping so that a Lorenz curve can be created using the
        # division v_i / b_i as sort key, and convert it into a numpy array.
        distribution_mapping.sort(
            key=lambda m: 0 if m[0] == 0 else m[1] / m[0]
        )
        distribution_mapping = numpy.array(distribution_mapping,
                                           dtype=numpy.float64)

        # From the mapping, sum the b_i and v_i values cumulatively (in the
        # array, they're arranged along the 0 axis) to create the X and Y
        # coordinates of the Lorenz curve.
        lorenz_xy = numpy.cumsum(distribution_mapping, axis=0)

        # Merge the Lorenz curve with the mapping values in a single array.
        # The columns are: lorenz_xy[0], lorenz_xy[1], distmap[0], distmap[1]
        merged = numpy.c_[lorenz_xy, distribution_mapping]

        # Calculate the Gini coefficient
        gini_B = numpy.sum(numpy.apply_along_axis(
            lambda row: (row[1] - row[3] / 2) * row[2], 1, merged
        ))
        gini = numpy.float64(1) - numpy.float64(2) * gini_B

        # Due to limitations in the floating point representation, allow small
        # errors below 0 and above 1 and round these to 0 and 1, respectively.
        epsilon_min = numpy.float64(-0.000001)
        epsilon_max = numpy.float64(1.000001)
        if gini >= epsilon_min and gini <= epsilon_max:
            gini = numpy.clip(gini, numpy.float64(0), numpy.float64(1))
        else:
            raise RuntimeError("gini %f out of range" % gini)

        return gini


class ControlOrderMeasurement(OptimizationMeasurement):
    """
    This class measures the order of controls a layout. The order is defined by
    the starting point and directions of an iteration over all cells/pixels in
    a layout.

    In a layout, there are four corners where the iteration can start. For each
    corner, it can be iterated in column-first or row-first oder, 8 directions
    in total, therefore 8 different orders are measured by this algorithm.
    The iteration is inspired by the various reading orders people use in the
    world, which are covered by some of the orders -- not all of them make
    sense, though.

    To compare an order between two layouts, the Levenshtein distance between
    those orders should be calculated by a metric. Given that only few orders
    are used by humans for reading, the distances should also be weighted to
    prefer orders like this kind over unnatural ones.
    """

    @staticmethod
    def _measure_order(layout, width, height, order_fn, expected_parent=None):
        """
        Measures the order of controls in the specified layout in the iteration
        defined by the specified parameters.

        Arguments:
        layout -- Layout to measure. Must be a legacy or a grid layout.
        width -- Width of the layout or parent container. For a grid layout,
        this value is always config.GRID_LAYOUT_COLUMN_COUNT.
        height -- Height of the layout or parent container
        order_fn -- Function which maps each pixel/cell a unique identifier
        based on the order of a manual (O^3) iteration.
        expected_parent -- Controls in the layout are filtered by their parent

        Returns:
        Array containing control IDs as iterated by this function
        """

        # Sort controls using the order function
        indexed_controls = [
            (c, order_fn(width, height, c.rc.h_start, c.rc.v_start))
            for c in layout.controls_by_parent(expected_parent)
        ]
        sorted_controls = sorted(indexed_controls, key=operator.itemgetter(1))
        is_legacy_layout = type(layout) is LegacyLayoutModel

        # Iterate again, this time with index. Find container controls and
        # extend their order into the control list.
        extended_order = []
        for c, _ in sorted_controls:
            extended_order.append(c.internal_id)
            if c.is_container():
                # Adjust width and height before recursing
                (new_w, new_h) = (c.rc.h_span, c.rc.v_span) \
                                 if is_legacy_layout \
                                 else (width, c.rc.v_span)

                extended_order.extend(ControlOrderMeasurement._measure_order(
                    layout, new_w, new_h, order_fn, c.internal_id
                ))

        # Extract internal IDs
        return extended_order

    @staticmethod
    def measure(layout, legacy_layout_ref=None):
        """
        Measures the order of controls in the specified layout.

        Arguments:
        layout -- Layout to measure. Must be a legacy or a grid layout.
        legacy_layout_ref -- A reference legacy layout used to span the grid
        layout over the area of the legacy layout. Required if a grid layout is
        to be measured.

        Returns:
        8 arrays of control orders, where each order is a list of internal
        control IDs from the specified layout. The control orders are:
         - Top-left corner, column-first to the bottom (Western)
         - Top-left corner, row-first to the right (Mongolian)
         - Top-right corner, column-first to the bottom (Hebrew, Arabic, ...)
         - Top-right corner, row-first to the left (East Asia outside PRC)
         - Bottom-left corner, column-first to the top
         - Bottom-left corner, row-first to the right
         - Bottom-right corner, column-first to the top
         - Bottom-right corner, row-first to the left
        """

        # Check arguments
        OptimizationMeasurement.argument_check(layout, legacy_layout_ref)

        # For a legacy layout, take its dimensions as-is
        if type(layout) is LegacyLayoutModel:
            layout_width = layout.rc.h_span
            layout_height = layout.rc.v_span

        # Otherwise, determine the number of rows for a grid layout. It is not
        # necessary to translate any coordinates.
        else:
            layout_width = config.GRID_LAYOUT_COLUMN_COUNT
            layout_height = int(numpy.ceil(
                numpy.float64(legacy_layout_ref.rc.v_span) /
                numpy.float64(legacy_layout_ref.line_height)
            ))

        # Define the orders as lambda functions which generate unique indices
        # based on the position of the control.
        order_fns = (
            lambda w, h, c_x, c_y:
                         c_x      +          c_y  * w,  # noqa: E221,E222
            lambda w, h, c_x, c_y:
                         c_x  * h +          c_y,       # noqa: E221,E222
            lambda w, h, c_x, c_y:
                (w - 1 - c_x)     +          c_y  * w,  # noqa: E221,E222
            lambda w, h, c_x, c_y:
                (w - 1 - c_x) * h +          c_y,       # noqa: E221,E222
            lambda w, h, c_x, c_y:
                         c_x      + (h - 1 - c_y) * w,  # noqa: E221,E222
            lambda w, h, c_x, c_y:
                         c_x  * h + (h - 1 - c_y),      # noqa: E221,E222
            lambda w, h, c_x, c_y:
                (w - 1 - c_x)     + (h - 1 - c_y) * w,  # noqa: E221,E222
            lambda w, h, c_x, c_y:
                (w - 1 - c_x) * h + (h - 1 - c_y)       # noqa: E221,E222
        )

        # Determine the control order using each of the functions above
        orders = [
            ControlOrderMeasurement._measure_order(layout, layout_width,
                                                   layout_height, x)
            for x in order_fns
        ]
        return orders


class ControlAlignmentMeasurement(OptimizationMeasurement):
    """
    This class measures the alignment of controls in a layout. For a legacy
    layout, a mapping from starting/ending horizontal/vertical coordinates to
    the controls is established. For grid layouts, a mapping in the reverse
    direction is established. Concatenating two mappings should lead to the
    coordinate value in all cases.
    """

    @staticmethod
    def _measure_legacy(layout, expected_parent, property_extract_fn):
        mapping = {}

        # Create the coordinate-to-control mapping
        for c in layout.controls_by_parent(expected_parent):
            key = property_extract_fn(c)
            if key in mapping:
                mapping[key].append(c.internal_id)
            else:
                mapping[key] = [c.internal_id]

        return mapping

    @staticmethod
    def _measure_grid(layout, expected_parent, property_extract_fn):
        mapping = {}

        # Create the control-to-coordinate mapping
        for c in layout.controls_by_parent(expected_parent):
            value = property_extract_fn(c)
            if c.internal_id in mapping:
                assert False
            else:
                mapping[c.internal_id] = value

        return mapping

    @staticmethod
    def _measure_single_alignment(layout, property_extract_fn):
        # Determine all parent IDs encountered in the layout
        all_parent_ids = set([
            c.parent_control for c in layout.controls
        ])

        # Select measurement function
        if type(layout) == LegacyLayoutModel:
            measure_func = ControlAlignmentMeasurement._measure_legacy
        elif type(layout) == GridLayoutModel:
            measure_func = ControlAlignmentMeasurement._measure_grid
        else:
            raise TypeError("unsupported layout")

        # Measure controls in each layer individually
        by_parent = {}
        for expected_parent in all_parent_ids:
            by_parent[expected_parent] = measure_func(
                layout, expected_parent, property_extract_fn
            )
        return by_parent

    @staticmethod
    def measure(layout, legacy_layout_ref=None):
        """
        Establishes the coordinate-to-control / control-to-coordinate mappings
        required for the control alignment metric.

        Arguments:
        layout -- Layout to measure. Must be a legacy or a grid layout.
        legacy_layout_ref -- A reference legacy layout which is required to
        pass the argument check. It is not necessary for this measurement.

        Returns:
        A tuple of four dictionaries mapping (starting X, ending X, starting Y,
        ending Y) to the controls (if layout is a legacy layout)
        A tuple of four dictionaries mapping the controls to (starting X,
        ending X, starting Y, ending Y) (if layout is a grid layout)
        """

        # Check arguments
        OptimizationMeasurement.argument_check(layout, legacy_layout_ref)

        # Run for each function
        property_extract_fns = (
            lambda c: c.rc.h_start,     # Left alignment
            lambda c: c.rc.h_end,       # Right alignment
            lambda c: c.rc.v_start,     # Top alignment
            lambda c: c.rc.v_end,       # Bottom alignment
        )
        return tuple([
            ControlAlignmentMeasurement._measure_single_alignment(
                layout, extract_fn
            )
            for extract_fn in property_extract_fns
        ])


class RelativeSizeMeasurement(OptimizationMeasurement):
    """
    This class measures the relative size of controls in a grid layout. Sizes
    of controls are internally specified as absolute values, which are to be
    divided by the size of the control's parent (either a container control or
    the layout itself) by the algorithms in this class and returned.
    """

    @staticmethod
    def measure(layout, legacy_layout_ref=None):
        """
        Computes the relative size of each control in the layout in relation to
        its parent.

        Arguments:
        layout -- Layout to measure. Must be a grid layout.
        legacy_layout_ref -- A reference legacy layout used to span the grid
        layout over the area of the legacy layout.

        Returns:
        Dictionary mapping each control (by its internal ID) to a 2-tuple
        (relative width, relative height)
        """

        # Check arguments
        OptimizationMeasurement.argument_check(layout, legacy_layout_ref)

        # For a legacy layout, take its dimensions as-is
        if type(layout) is LegacyLayoutModel:
            layout_width = layout.rc.h_span
            layout_height = layout.rc.v_span

        # Otherwise, determine the number of rows for a grid layout. It it not
        # necessary to translate any coordinates.
        else:
            layout_width = config.GRID_LAYOUT_COLUMN_COUNT
            layout_height = int(numpy.ceil(
                numpy.float64(legacy_layout_ref.rc.v_span) /
                numpy.float64(legacy_layout_ref.line_height)
            ))

        # Iterate over each control and compute its relative size. Watch out
        # for controls with a parent container.
        relative_sizes = {}
        for c in layout.controls:
            if c.parent_control is not None:
                parent_control = layout.get_control_by_id(c.parent_control)
                parent_width = parent_control.rc.h_span
                parent_height = parent_control.rc.v_span
            else:
                parent_width = layout_width
                parent_height = layout_height

            relative_sizes[c.internal_id] = (
                numpy.float64(c.rc.h_span) / numpy.float64(parent_width),
                numpy.float64(c.rc.v_span) / numpy.float64(parent_height)
            )

        return relative_sizes
