/*
 * Copyright (c) 2018 Thomas Weber <pokehaxco@gmail.com>
 *
 * SPDX-License-Identifier: MIT
 */

#include "metrics.h"
#include "measurements.h"
#include "../configurables.h"

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>


/*
 * Whitespace ratio metric
 */

float uit_xforms_metric_whitespace_ratio(struct Layout *grid_layout,
    struct Layout *legacy_layout)
{
    /* XXX: currently unimplemented */
    (void) grid_layout;
    (void) legacy_layout;
    fprintf(stderr, "fatal: sorry, this metric is currently not implemented; "
        "use the Python optimizer if you want to use this metric\n");
    abort();

    /* unreachable code, but I like old compilers */
    return 0.f;
}


/*
 * Gini density metric
 */

float uit_xforms_metric_density_gini(struct Layout *grid_layout,
    struct Layout *legacy_layout)
{
    /* XXX: currently unimplemented */
    (void) grid_layout;
    (void) legacy_layout;
    fprintf(stderr, "fatal: sorry, this metric is currently not implemented; "
        "use the Python optimizer if you want to use this metric\n");
    abort();

    /* unreachable code, but I like old compilers */
    return 0.f;
}


/*
 * Herfindahl density metric
 */

float uit_xforms_metric_density_herfindahl(struct Layout *grid_layout,
    struct Layout *legacy_layout)
{
    /* XXX: currently unimplemented */
    (void) grid_layout;
    (void) legacy_layout;
    fprintf(stderr, "fatal: sorry, this metric is currently not implemented; "
        "use the Python optimizer if you want to use this metric\n");
    abort();

    /* unreachable code, but I like old compilers */
    return 0.f;
}


/*
 * Control order metric
 */

/*
 * This function is an adaptation of the dynamic programming algorithm for the
 * Levenshtein distance to sequences of controls.
 */
static size_t _control_order_levenshtein(
    struct Control **restrict s1, const size_t l1,
    struct Control **restrict s2, const size_t l2)
{
#define s1i(i) s1[(i)]->internal_id
#define s2i(i) s2[(i)]->internal_id
#define Cij(i, j) C[(i) * (l1 + 1) + (j)]

    /* Costs of operations can be configured; a cost of 1 for all operations
       matches python-Levenshtein in the Python implementation. */
    const size_t substitute_cost = 1;
    const size_t insertion_cost = 1;
    const size_t deletion_cost = 1;

    /* Prevent overflow of allocation size */
    if (l1 > SIZE_MAX - 1 || l2 > SIZE_MAX - 1)
        return SIZE_MAX;
    if ((l1 + 1) > (SIZE_MAX / sizeof(size_t) / (l2 + 1)))
        return SIZE_MAX;

    size_t *C = malloc((l1 + 1) * (l2 + 1) * sizeof(size_t));
    if (!C)
        return SIZE_MAX;

    /* Initialize first row and first column */
    Cij(0, 0) = 0;
    for (size_t i = 1; i <= l2; ++i) Cij(i, 0) = i;
    for (size_t i = 1; i <= l1; ++i) Cij(0, i) = i;

    /* Fill the matrix. Don't do any fancy optimizations, let the compiler
       vectorize this algorithm. */
    for (size_t i = 1; i <= l2; ++i) {
        for (size_t j = 1; j <= l1; ++j) {
            size_t min, v;

            /* Same character? If not, an edit is necessary */
            bool chars_equal = s1i(j - 1) == s2i(i - 1);

            /* Minimum of the following three values */
            min = Cij(i - 1, j - 1) + (chars_equal ? 0 : substitute_cost);
            v = Cij(i - 1, j) + insertion_cost;
            if (v < min)
                min = v;
            v = Cij(i, j - 1) + deletion_cost;
            if (v < min)
                min = v;
            Cij(i, j) = min;
        }
    }

    /* Save current distance */
    size_t distance = Cij(l2, l1);
    free(C);
    return distance;

#undef Cij
#undef s2i
#undef s1i
}

float uit_xforms_metric_control_order(struct Layout *grid_layout,
    struct Layout *legacy_layout)
{
    (void) grid_layout;
    (void) legacy_layout;

    /* Determine the largest internal ID encountered in the layout */
    unsigned int largest_id = legacy_layout->controls[0].internal_id;
    for (unsigned int i = 1; i < legacy_layout->control_count; ++i) {
        const struct Control *c = &legacy_layout->controls[i];
        if (c->internal_id > largest_id)
            largest_id = c->internal_id;
    }

    /* Determine all orders for the grid layout */
    struct Control ***grid_orders =
        uit_xforms_measure_control_order(grid_layout);
    if (!grid_orders)
        goto error;

    /* Determine all orders for the legacy layout */
    struct Control ***legacy_orders =
        uit_xforms_measure_control_order(legacy_layout);
    if (!legacy_orders)
        goto error;

    /*
     * For each order iteration, compute the Levenshtein distance between
     * pairs. As the values end up in a weighted average calculation anyway,
     * it can be merged with the calculation of dividend and divisor for the
     * weighted average.
     */
    static const float weights[UIT_XFORMS_MEASURE_CONTROL_ORDER_COUNT] = {
        CONFIG_CONTROL_ORDER_METRIC_WEIGHTS
    };
    float dividend = 0.f, divisor = 0.f;

    for (unsigned int i = 0; i < UIT_XFORMS_MEASURE_CONTROL_ORDER_COUNT; ++i) {
        unsigned long v = _control_order_levenshtein(
            grid_orders[i], legacy_layout->control_count,
            legacy_orders[i], legacy_layout->control_count
        );
        divisor += weights[i];
        dividend += weights[i] * v;
    }

    /* Negative and zero weights are a program configuration error */
    if (divisor <= 0.f) {
        fprintf(stderr, "error: sum of weights must be a positive value\n");
        abort();
    }

    /* Division part of the weighted average */
    const float ret = dividend / divisor;

    /* Clean up */
    uit_xforms_measure_control_order_free(grid_orders);
    uit_xforms_measure_control_order_free(legacy_orders);
    return ret;

error:
    fprintf(stderr, "error: out of memory\n");
    abort();
}


/*
 * Control alignment metric
 */

float uit_xforms_metric_control_alignment(struct Layout *grid_layout,
    struct Layout *legacy_layout)
{
    /* XXX: currently unimplemented */
    (void) grid_layout;
    (void) legacy_layout;
    fprintf(stderr, "fatal: sorry, this metric is currently not implemented; "
        "use the Python optimizer if you want to use this metric\n");
    abort();

    /* unreachable code, but I like old compilers */
    return 0.f;
}


/*
 * Control size loss metric
 */

float uit_xforms_metric_size_loss(struct Layout *grid_layout,
    struct Layout *legacy_layout)
{
    /* XXX: currently unimplemented */
    (void) grid_layout;
    (void) legacy_layout;
    fprintf(stderr, "fatal: sorry, this metric is currently not implemented; "
        "use the Python optimizer if you want to use this metric\n");
    abort();

    /* unreachable code, but I like old compilers */
    return 0.f;
}
