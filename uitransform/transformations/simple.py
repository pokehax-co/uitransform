#
# Copyright (c) 2018 Thomas Weber <pokehaxco@gmail.com>
#
# SPDX-License-Identifier: MIT
#

import copy
import numpy
import uitransform.configurables as config

from uitransform.concepts import Rectangle, GridLayoutModel


class SimpleLayoutTransformation(object):
    """
    This class implements a primitive transformation of legacy layouts into a
    grid layout.

    In the vertical direction, the legacy layout is divided into rows with a
    height equal to the line height specified in the legacy layout (and rounded
    up if the legacy layout height is not a multiple of the line height).
    The simple transformation rounds vertical coordinates to the nearest
    multiple of the line height.

    In the horizontal direction, the legacy layout is divided into a fixed
    number of columns. Horizontal coordinates are rounded to the nearest row.

    The row and column spans of all controls will be set to 1, discarding the
    original dimensions of a control in the legacy layout. This worsens the
    quality of this transformation to demonstrate the effect of optimizing the
    result, as the simple transformation can be used as starting point for an
    optimization process which requires such.
    """

    BETTER_THAN_IT_SHOULD_BE = True

    def transform(self, legacy_layout):
        """
        Transforms a legacy layout into a grid layout.

        Arguments:
        legacy_layout -- Legacy layout to transform

        Returns:
        Grid layout from the specified legacy layout
        """

        # Determine column width for the legacy layout
        global_column_width = numpy.float64(legacy_layout.rc.h_span) / \
            config.GRID_LAYOUT_COLUMN_COUNT

        # Copy identifier and title into the grid layout
        grid_layout = GridLayoutModel(legacy_layout.name, legacy_layout.title)

        # Calculate new coordinates and sizes for each control
        for control in legacy_layout.controls:
            #
            # If the control is inside a container, the legacy layout
            # coordinates are relative to the container. Also, a container
            # creates a new grid layout inside a cell, so we need to operate
            # with the column width for that layout instead of the global
            # width.
            #
            if control.parent_control is not None:
                parent = legacy_layout.get_control_by_id(
                    control.parent_control
                )
                column_width = numpy.float64(parent.rc.h_span) / \
                    config.GRID_LAYOUT_COLUMN_COUNT
            else:
                column_width = global_column_width

            # Divide the X coordinate by the column width and round to the
            # nearest integer, but stay within the [0..columncount-1] interval
            h_start = int(numpy.rint(
                numpy.float64(control.rc.h_start) / column_width
            ))
            h_start = min(config.GRID_LAYOUT_COLUMN_COUNT - 1,
                          max(0, h_start))

            # Round the Y coordinate to the nearest multiple of the line height
            v_start = int(numpy.rint(
                numpy.float64(control.rc.v_start) /
                numpy.float64(legacy_layout.line_height)
            ))

            #
            # To deliberately worsen the quality of the transformation, cell
            # spans of all controls are set to 1, except when the
            # BETTER_THAN_IT_SHOULD_BE configurable is activated.
            #
            if not SimpleLayoutTransformation.BETTER_THAN_IT_SHOULD_BE:
                h_span = 1
                v_span = 1
            else:
                # h_start + h_span should not exceed column count
                h_span = int(numpy.rint(
                    numpy.float64(control.rc.h_span) / column_width
                ))
                h_span = min(config.GRID_LAYOUT_COLUMN_COUNT - h_start,
                             max(1, h_span))

                v_span = int(numpy.rint(
                    numpy.float64(control.rc.v_span) /
                    numpy.float64(legacy_layout.line_height)
                ))

            # Make a copy of the control and modify the rectangle
            new_control = copy.deepcopy(control)
            new_control.rc = Rectangle(h_start, v_start, h_span, v_span)
            grid_layout.add_control(new_control)

        # We're finished
        return grid_layout
