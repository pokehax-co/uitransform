#
# Copyright (c) 2018 Thomas Weber <pokehaxco@gmail.com>
#
# SPDX-License-Identifier: MIT
#

#
# This module contains metrics which calculate the degree of difference between
# a legacy layout and a grid layout (which is created through optimization
# against a metric) by combining various measurements on layouts.
#

import abc
import collections
import Levenshtein
import numpy
import uitransform.configurables as config

from .measurements import (
    WhitespaceRatioMeasurement, HerfindahlDensityMeasurement,
    GiniDensityMeasurement, ControlOrderMeasurement,
    ControlAlignmentMeasurement, RelativeSizeMeasurement
)


class OptimizationMetric(abc.ABC):
    """
    This abstract class is the base of all metric calculators.
    """

    @staticmethod
    @abc.abstractmethod
    def calculate(legacy_layout, grid_layout, precalculated_legacy=None,
                  debug=False):
        """
        Calculates a non-negative metric from the original legacy layout and
        the specified grid layout.

        Arguments:
        legacy_layout -- Original legacy layout
        grid_layout -- A grid layout created from the legacy layout
        precalculated_legacy -- If a derived class implements the method
        precalculate_legacy() to partially compute results for a legacy layout,
        its return value is passed here.
        debug -- Whether to print debug output

        Returns:
        A non-negative number indicating the degree of difference between the
        layouts
        """

        raise NotImplementedError(
            "Method called from the OptimizationMetric abstract class"
        )


class WhitespaceRatioMetric(OptimizationMetric):
    """
    This metric measures the whitespace ratio of both layouts and calculates
    the absolute difference between the two as their degree of difference.
    """

    @staticmethod
    def precalculate_legacy(legacy_layout):
        """
        Pre-calculates information from the legacy layout, which remains
        constant throughout the entire optimization.

        Arguments:
        legacy_layout -- Original legacy layout

        Returns:
        Something measured over the legacy layout
        """

        # Whitespace ratio of the legacy layout
        return WhitespaceRatioMeasurement.measure(legacy_layout)

    @staticmethod
    def calculate(legacy_layout, grid_layout, precalculated_legacy=None,
                  debug=False):
        """
        Calculates a non-negative metric from the original legacy layout and
        the specified grid layout.

        Arguments:
        legacy_layout -- Original legacy layout
        grid_layout -- A grid layout created from the legacy layout
        precalculated_legacy -- If a derived class implements the method
        precalculate_legacy() to partially compute results for a legacy layout,
        its return value is passed here.
        debug -- Whether to print debug output

        Returns:
        A non-negative number indicating the degree of difference between the
        layouts
        """

        # Determine whitespace ratio for the legacy layout
        if precalculated_legacy is not None:
            legacy_ratio = precalculated_legacy
        else:
            legacy_ratio = WhitespaceRatioMetric.precalculate_legacy(
                legacy_layout
            )

        return numpy.absolute(
            legacy_ratio -
            WhitespaceRatioMeasurement.measure(grid_layout, legacy_layout)
        )


class HerfindahlDensityMetric(OptimizationMetric):
    """
    This metric measures the density (Herfindahl index) of both layouts and
    calculates the absolute difference between the two as their degree of
    difference.
    """

    @staticmethod
    def precalculate_legacy(legacy_layout):
        """
        Pre-calculates information from the legacy layout, which remains
        constant throughout the entire optimization.

        Arguments:
        legacy_layout -- Original legacy layout

        Returns:
        Something measured over the legacy layout
        """

        # Herfindahl-based density of the legacy layout
        return HerfindahlDensityMeasurement.measure(legacy_layout)

    @staticmethod
    def calculate(legacy_layout, grid_layout, precalculated_legacy=None,
                  debug=False):
        """
        Calculates a non-negative metric from the original legacy layout and
        the specified grid layout.

        Arguments:
        legacy_layout -- Original legacy layout
        grid_layout -- A grid layout created from the legacy layout
        precalculated_legacy -- If a derived class implements the method
        precalculate_legacy() to partially compute results for a legacy layout,
        its return value is passed here.
        debug -- Whether to print debug output

        Returns:
        A non-negative number indicating the degree of difference between the
        layouts
        """

        # Determine Herfindahl-based density for the legacy layout
        if precalculated_legacy is not None:
            legacy_density = precalculated_legacy
        else:
            legacy_density = HerfindahlDensityMetric.precalculate_legacy(
                legacy_layout
            )

        return numpy.absolute(
            legacy_density -
            HerfindahlDensityMeasurement.measure(grid_layout, legacy_layout)
        )


class GiniDensityMetric(OptimizationMetric):
    """
    This metric measures the density (Gini coefficient) of both layouts and
    calculates the absolute difference between the two as their degree of
    difference.
    """

    @staticmethod
    def precalculate_legacy(legacy_layout):
        """
        Pre-calculates information from the legacy layout, which remains
        constant throughout the entire optimization.

        Arguments:
        legacy_layout -- Original legacy layout

        Returns:
        Something measured over the legacy layout
        """

        # Gini-based density of the legacy layout
        return GiniDensityMeasurement.measure(legacy_layout)

    @staticmethod
    def calculate(legacy_layout, grid_layout, precalculated_legacy=None,
                  debug=False):
        """
        Calculates a non-negative metric from the original legacy layout and
        the specified grid layout.

        Arguments:
        legacy_layout -- Original legacy layout
        grid_layout -- A grid layout created from the legacy layout
        precalculated_legacy -- If a derived class implements the method
        precalculate_legacy() to partially compute results for a legacy layout,
        its return value is passed here.
        debug -- Whether to print debug output

        Returns:
        A non-negative number indicating the degree of difference between the
        layouts
        """

        # Determine Gini-based density for the legacy layout
        if precalculated_legacy is not None:
            legacy_density = precalculated_legacy
        else:
            legacy_density = GiniDensityMetric.precalculate_legacy(
                legacy_layout
            )

        return numpy.absolute(
            legacy_density -
            GiniDensityMeasurement.measure(grid_layout, legacy_layout)
        )


class ControlOrderMetric(OptimizationMetric):
    """
    This metric measures the difference between orders of controls. There are
    8 different orders (4 corners, column-first or row-first iteration), of
    which only 4 are actual reading orders used by humans, and from those, just
    two of them have widespread use (left-to-right and right-to-left) as
    vertical text has only been widespread among browsers in the last years.

    Differences in the control order of layouts are measured using the
    Levenshtein distance, which is actually operated on strings (lists are
    converted to strings), and the 8 differences are combined using weighted
    average.
    """

    @staticmethod
    def _order_list_to_string(order_list):
        """
        Converts the order list to a string. To make two such string comparable
        within the Levenshtein distance calculation, the largest number in the
        list is used to determine the string length of each number in the list,
        which is then concatenated.

        Argument:
        order_list -- List of control IDs

        Returns:
        String representing order_list
        """

        maxval = max(order_list)
        maxval_length = 1 if maxval == 0 else (int(numpy.log10(maxval)) + 1)
        format_string = ('{:%dd}' % maxval_length) * len(order_list)
        return format_string.format(*order_list)

    @staticmethod
    def precalculate_legacy(legacy_layout):
        """
        Pre-calculates information from the legacy layout, which remains
        constant throughout the entire optimization.

        Arguments:
        legacy_layout -- Original legacy layout

        Returns:
        Something measured over the legacy layout
        """

        # Determine orders, and convert each to a string
        orders_legacy = [
            ControlOrderMetric._order_list_to_string(order)
            for order in ControlOrderMeasurement.measure(legacy_layout)
        ]
        return orders_legacy

    @staticmethod
    def calculate(legacy_layout, grid_layout, precalculated_legacy=None,
                  debug=False):
        """
        Calculates a non-negative metric from the original legacy layout and
        the specified grid layout.

        Arguments:
        legacy_layout -- Original legacy layout
        grid_layout -- A grid layout created from the legacy layout

        Returns:
        A non-negative number indicating the degree of difference between the
        layouts
        """

        # Determine orders, and convert each to a string
        if precalculated_legacy is not None:
            orders_legacy = precalculated_legacy
        else:
            orders_legacy = ControlOrderMetric.precalculate_legacy(
                legacy_layout
            )

        orders_grid = [
            ControlOrderMetric._order_list_to_string(order)
            for order in ControlOrderMeasurement.measure(
                grid_layout, legacy_layout)
        ]

        if debug:
            print("(COM) Legacy TLCF length: %u" % len(orders_legacy[0]))
            print("(COM) Legacy order string: '%s'" % orders_legacy)
            print("(COM) Grid order string: '%s'" % orders_grid)

        # Determine the Levenshtein distances. The maximum distance is the
        # length of that string which is the longer of the two, so the
        # distances can be normalized to the [0; 1] interval.
        distances = [
            Levenshtein.distance(x, y)
            for (x, y) in zip(orders_legacy, orders_grid)
        ]
        if debug:
            print("(COM) Individual distances:" % distances)

        # The length of the strings is identical in all orders and in both
        # layouts
        string_length = len(orders_grid[0])

        # Compute the weighted average
        return numpy.average(
            numpy.divide(distances, string_length),
            weights=config.CONTROL_ORDER_METRIC_WEIGHTS
        )


class ControlAlignmentMetric(OptimizationMetric):
    """
    Given both a legacy and a grid layout, this metric determines the number of
    controls which differ in left, right, top, and bottom alignment between the
    layouts, and computes the arithmetic mean.
    """

    @staticmethod
    def precalculate_legacy(legacy_layout):
        """
        Pre-calculates information from the legacy layout, which remains
        constant throughout the entire optimization.

        Arguments:
        legacy_layout -- Original legacy layout

        Returns:
        Something measured over the legacy layout
        """

        # Create coordinate-to-control mapping
        legacy_mapping = ControlAlignmentMeasurement.measure(legacy_layout)
        return legacy_mapping

    @staticmethod
    def calculate(legacy_layout, grid_layout, precalculated_legacy=None,
                  debug=False):
        """
        Calculates a non-negative metric from the original legacy layout and
        the specified grid layout.

        Arguments:
        legacy_layout -- Original legacy layout
        grid_layout -- A grid layout created from the legacy layout

        Returns:
        A non-negative number indicating the degree of difference between the
        layouts
        """

        # Create mappings from both layouts
        if precalculated_legacy is not None:
            legacy_mapping = precalculated_legacy
        else:
            legacy_mapping = ControlAlignmentMeasurement.measure(legacy_layout)

        grid_mapping = ControlAlignmentMeasurement.measure(
            grid_layout, legacy_layout)

        assert len(legacy_mapping) == len(grid_mapping)

        # For each alignment direction
        violations_per_direction = []
        for legacy, grid in zip(legacy_mapping, grid_mapping):
            violations = 0

            # Zip the legacy and grid dictionaries (which have the group as
            # key)
            per_group_zip = [
                (key, value, grid[key])
                for (key, value) in legacy.items()
                if key in grid
            ]
            assert len(grid) == len(legacy)
            assert len(per_group_zip) == len(legacy)

            # Go through each control group
            for k, legacy_group, grid_group in per_group_zip:
                # Check per coordinate in the legacy group
                for coord, controls in legacy_group.items():
                    # Determine the corresponding value in the grid layout
                    in_grid_coords = [grid_group[c] for c in controls]
                    in_grid_coord_counts = collections.Counter(in_grid_coords)

                    # Should be only one key
                    if len(in_grid_coord_counts.keys()) == 1:
                        continue

                    # Ok, we got a violation. Take the coordinate which has the
                    # majority and blame those which don't have that coordinate
                    majority = max(in_grid_coord_counts,
                                   key=lambda k: in_grid_coord_counts[k])

                    violations += sum([
                        (1 if grid_group[c] == majority else 0)
                        for c in controls
                    ])

            violations_per_direction.append(violations)

        if debug:
            print("(CAM New) Measurements:", violations_per_direction)

        # The maximum number of violations is the number of controls, which
        # allows for normalization to the [0; 1] interval.
        # Use the arithmetic mean over the violations as violations in all
        # four directions are equally grave.
        max_violations = len(grid_layout.controls)
        return numpy.mean(
            numpy.divide(violations_per_direction, max_violations)
        )


class SizeLossMetric(OptimizationMetric):
    """
    Computes the loss of width and height in a grid layout compared to its
    legacy layout and sums it.
    """

    @staticmethod
    def precalculate_legacy(legacy_layout):
        """
        Pre-calculates information from the legacy layout, which remains
        constant throughout the entire optimization.

        Arguments:
        legacy_layout -- Original legacy layout

        Returns:
        Something measured over the legacy layout
        """

        # Relative sizes of controls in the legacy layout
        return RelativeSizeMeasurement.measure(legacy_layout)

    @staticmethod
    def calculate(legacy_layout, grid_layout, precalculated_legacy=None,
                  debug=False):
        """
        Calculates a non-negative metric from the original legacy layout and
        the specified grid layout.

        Arguments:
        legacy_layout -- Original legacy layout
        grid_layout -- A grid layout created from the legacy layout
        precalculated_legacy -- If a derived class implements the method
        precalculate_legacy() to partially compute results for a legacy layout,
        its return value is passed here.
        debug -- Whether to print debug output

        Returns:
        A non-negative number indicating the degree of difference between the
        layouts
        """

        # Determine relative sizes of controls in the legacy layout
        if precalculated_legacy is not None:
            legacy_sizes = precalculated_legacy
        else:
            legacy_sizes = SizeLossMetric.precalculate_legacy(legacy_layout)

        # Determine relative sizes of controls in the grid layout
        grid_sizes = RelativeSizeMeasurement.measure(
            grid_layout, legacy_layout
        )

        # Compute width and height losses and sum them up
        losses = [
            (max(legacy_sizes[i][0] - grid_sizes[i][0], 0),
             max(legacy_sizes[i][1] - grid_sizes[i][1], 0))
            for i in set(legacy_sizes) & set(grid_sizes)
        ]
        if debug:
            print("(SLM) Size losses: %s" % losses)
        return numpy.sum(losses)


class AverageOfAllMetric(OptimizationMetric):
    """
    Computes the weighted average of the following metrics: Whitespace ratio,
    density (Herfindahl index), density (Gini coefficient), order of controls,
    alignment of controls, loss of size.
    """

    METRIC_WEIGHTS = (
        1,  # Whitespace ratio
        3,  # Density (Herfindahl)
        3,  # Density (Gini)
        7,  # Control order
        2,  # Control alignment
        4   # Size loss
    )

    _metric_classes = (
        WhitespaceRatioMetric,
        HerfindahlDensityMetric,
        GiniDensityMetric,
        ControlOrderMetric,
        ControlAlignmentMetric,
        SizeLossMetric
    )

    @staticmethod
    def precalculate_legacy(legacy_layout):
        """
        Pre-calculates information from the legacy layout, which remains
        constant throughout the entire optimization.

        Arguments:
        legacy_layout -- Original legacy layout

        Returns:
        Something measured over the legacy layout
        """

        # Cached values for all metrics
        return [
            cls.precalculate_legacy(legacy_layout)
            for cls in AverageOfAllMetric._metric_classes
        ]

    @staticmethod
    def calculate(legacy_layout, grid_layout, precalculated_legacy=None,
                  debug=False):
        """
        Calculates a non-negative metric from the original legacy layout and
        the specified grid layout.

        Arguments:
        legacy_layout -- Original legacy layout
        grid_layout -- A grid layout created from the legacy layout
        precalculated_legacy -- If a derived class implements the method
        precalculate_legacy() to partially compute results for a legacy layout,
        its return value is passed here.
        debug -- Whether to print debug output

        Returns:
        A non-negative number indicating the degree of difference between the
        layouts
        """

        # Compute metrics
        if precalculated_legacy is not None:
            metrics = [
                cls.calculate(legacy_layout, grid_layout,
                              precalculated_legacy[i])
                for i, cls in enumerate(AverageOfAllMetric._metric_classes)
            ]
        else:
            metrics = [
                cls.calculate(legacy_layout, grid_layout)
                for cls in AverageOfAllMetric._metric_classes
            ]

        # Now the weighted average
        return numpy.average(metrics,
                             weights=AverageOfAllMetric.METRIC_WEIGHTS)


# Mapping of metrics to the C API
metrics_c_mapping = {
    WhitespaceRatioMetric: 0,
    HerfindahlDensityMetric: 1,
    GiniDensityMetric: 2,
    ControlOrderMetric: 3,
    ControlAlignmentMetric: 4,
    SizeLossMetric: 5
}
