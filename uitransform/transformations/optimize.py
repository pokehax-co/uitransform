#
# Copyright (c) 2018 Thomas Weber <pokehaxco@gmail.com>
#
# SPDX-License-Identifier: MIT
#

#
# This module contains implementations of layout optimizers.
#
# The implementation of the genetic algorithm is inspired by this article:
# https://lethain.com/genetic-algorithms-cool-name-damn-simple/
#

import copy
import numpy
import sys
import uitransform.configurables as config

from .metrics import OptimizationMetric
from uitransform.concepts import Rectangle, LegacyLayoutModel, GridLayoutModel


class EvolutionaryOptimizer(object):
    """
    This class implements an optimizer which uses genetic algorithms to produce
    an optimal solution. It operates exclusively on grid layouts (but can be
    given a legacy layout as input to derive a grid layout from). A fitness
    function assessing an intermediate layout must be provided externally by
    the caller.

    At the beginning of a genetic algorithm, an initial population of a larger
    number of grid layouts within the search space (the dimensions of the grid
    layout) is (randomly) generated.

    In each iteration (generation), a subset of the population is selected for
    creating a new population generation based on their fitness (but sometimes
    a random population member as well).

    Two operations are performed on the subset: first, a subset of that subset
    is being mutated, and second, two members of the subset are crossed over to
    create a new member of the population.

    This process is repeated for each iteration. The optimization stops once
    the maximum number of iterations is reached.
    """

    DEBUG = False

    def __init__(self, metrics, input_layout, population_seed=None):
        """
        Initializes a new instance of the genetic optimization algorithm.

        Arguments:
        metrics -- Metrics to optimize grid layout against. This is a class
        derived from OptimizationMetric, not an instance.
        input_layout -- Legacy layout to work on. It must have at least once
        control in it.
        population_seed (optional) -- A grid layout to add as seed to the
        initial population
        """

        # Must be a single metric or a tuple of metrics
        if isinstance(metrics, tuple):
            for m in metrics:
                if not issubclass(m, OptimizationMetric):
                    raise TypeError(
                        "Metrics are not derived from OptimizationMetric"
                    )
            self._metric_info = [
                [metric, None]
                for metric in metrics
            ]
        elif issubclass(metrics, OptimizationMetric):
            self._metric_info = [[metrics, None]]
        else:
            raise TypeError(
                "Metrics are not derived from OptimizationMetric"
            )

        # Input layout must be a legacy layout
        if not isinstance(input_layout, LegacyLayoutModel):
            raise TypeError("Input layout must be a legacy layout")
        self._input_layout = input_layout

        # Determine the randomization limits for this layout
        row_count = int(numpy.ceil(
            numpy.float64(input_layout.rc.v_span) /
            numpy.float64(input_layout.line_height)
        ))
        self._randomize_limits = Rectangle(
            0, 0, config.GRID_LAYOUT_COLUMN_COUNT, row_count
        )

        # If we have a seed, then it must be a grid layout
        self._population_input = None
        if population_seed is not None:
            if isinstance(population_seed, GridLayoutModel):
                # Copy it because it might be mutated later
                self._population_input = copy.deepcopy(population_seed)
            else:
                raise TypeError(
                    "Population seed must be a grid layout, or None"
                )

    @staticmethod
    def randint(start, end):
        """
        numpy's randint() method doesn't include the end value in its range.
        Fix this.
        """
        return numpy.random.randint(start, end + 1)

    def _mutate_control_xpos(self, layout, c):
        """
        Mutates the horizontal position of a control by either subtracting or
        adding 1 to it.

        Arguments:
        layout -- Layout the control belongs to
        c -- Control to mutate

        Returns:
        True if the property could be mutated without violating grid layout
        definition
        False otherwise
        """

        # Perform addition or subtraction
        new_value = c.rc.h_start + int(numpy.random.choice((-1, 1)))

        # If we leave the interval, abort
        if new_value < 0:
            return False
        if new_value > config.GRID_LAYOUT_COLUMN_COUNT - c.rc.h_span:
            return False

        # Write back
        c.rc.h_start = new_value
        return True

    def _mutate_control_width(self, layout, c):
        """
        Mutates the width of a control by either subtracting or adding 1 to it.

        Arguments:
        layout -- Layout the control belongs to
        c -- Control to mutate

        Returns:
        True if the property could be mutated without violating grid layout
        definition
        False otherwise
        """

        # Perform addition or subtraction
        new_value = c.rc.h_span + int(numpy.random.choice((-1, 1)))

        # If we leave the interval, abort
        if new_value < 1:
            return False
        if new_value > config.GRID_LAYOUT_COLUMN_COUNT - c.rc.h_start:
            return False

        # Write back
        c.rc.h_span = new_value
        return True

    def _mutate_control_ypos(self, layout, c):
        """
        Mutates the vertical position of a control by either subtracting or
        adding 1 to it.

        Arguments:
        layout -- Layout the control belongs to
        c -- Control to mutate

        Returns:
        True if the property could be mutated without violating grid layout
        definition
        False otherwise
        """

        # Take the height of the control's parent into account to avoid
        # vertical overflows
        if c.parent_control is not None:
            parent = self._input_layout.get_control_by_id(c.parent_control)
            total_v_span = parent.rc.v_span
        else:
            total_v_span = self._randomize_limits.v_span

        # Perform addition or subtraction
        new_value = c.rc.v_start + int(numpy.random.choice((-1, 1)))

        # If we leave the interval, abort
        if new_value < 0:
            return False
        if new_value > total_v_span - c.rc.v_span:
            return False

        # Write back
        c.rc.v_start = new_value
        return True

    def _mutate_control_height(self, layout, c):
        """
        Mutates the height of a control by either subtracting or adding 1 to
        it.

        Arguments:
        layout -- Layout the control belongs to
        c -- Control to mutate

        Returns:
        True if the property could be mutated without violating grid layout
        definition
        False otherwise
        """

        # Take the height of the control's parent into account to avoid
        # vertical overflows
        if c.parent_control is not None:
            parent = self._input_layout.get_control_by_id(c.parent_control)
            total_v_span = parent.rc.v_span
        else:
            total_v_span = self._randomize_limits.v_span

        # Perform addition or subtraction
        new_value = c.rc.v_span + int(numpy.random.choice((-1, 1)))

        #
        # If the control is a container, perform an additional check. If the
        # container's children would overflow (because of their coordinates)
        # when decreasing the container's size, block this change.
        #
        if new_value < c.rc.v_span and c.is_container():
            max_v_span = 1

            # Determine the row count inside the sublayout
            for child in layout.controls_by_parent(c.internal_id):
                v = child.rc.v_end
                if v > max_v_span:
                    max_v_span = v

            if new_value < max_v_span:
                return False

        # If we leave the interval, abort
        if new_value < 1:
            return False
        if new_value > total_v_span - c.rc.v_start:
            return False

        # Write back
        c.rc.v_span = new_value
        return True

    def _randomize_control_rcs(self, layout, expected_parent):
        """
        Randomizes the position and size of the specified control.

        Arguments:
        layout -- Layout whose controls to randomize positions and sizes
        expected_parent -- Only process controls with this parent
        """

        # Take the height of the control's parent into account to avoid
        # vertical overflows
        if expected_parent is not None:
            parent = layout.get_control_by_id(expected_parent)
            v_start = 0
            v_span = parent.rc.v_span
        else:
            v_start = self._randomize_limits.v_start
            v_span = self._randomize_limits.v_span

        # Randomize positions and sizes of all controls with the same parent
        for c in layout.controls_by_parent(expected_parent):
            # Span minus 1 is the last column of the grid, while the span value
            # is outside the grid
            c.rc.h_start = self.randint(
                self._randomize_limits.h_start,
                self._randomize_limits.h_span - 1
            )
            c.rc.v_start = self.randint(v_start, v_span - 1)

            # Keep the spans within the limits of the rectangle
            c.rc.h_span = self.randint(
                1, self._randomize_limits.h_span - c.rc.h_start
            )
            c.rc.v_span = self.randint(1, v_span - c.rc.v_start)

            # Is this a container? Continue recursively
            if c.is_container():
                self._randomize_control_rcs(layout, c.internal_id)

            assert c.rc.h_start + c.rc.h_span <= self._randomize_limits.h_span
            assert c.rc.v_start + c.rc.v_span <= v_span

    def _create_individual(self):
        """
        Creates a new individual of the population. This is a grid layout based
        off the input, with random values for position and size.

        Returns:
        The new individual
        """

        # Perform a manual copy because the template is a legacy layout, and
        # the individuals are grid layouts.
        new_individual = GridLayoutModel(self._input_layout.name,
                                         self._input_layout.title)
        for c in self._input_layout.controls:
            new_individual.add_control(copy.deepcopy(c))

        # Randomize all positions and sizes entirely
        self._randomize_control_rcs(new_individual, None)

        return new_individual

    def _mutate_individual(self, individual, all_controls=False):
        """
        Mutates the specified individual by assigning new random positions and
        sizes within the limits given by the input layout.

        Arguments:
        individual -- Individual to mutate

        Returns:
        The same individual, with mutated coordinates
        """

        # Select a random control
        control_count = len(individual.controls)
        control_index = self.randint(0, control_count - 1)
        control = individual.controls[control_index]

        # Try to perform one of the four property mutations until one succeeds
        mutations = [self._mutate_control_xpos, self._mutate_control_ypos,
                     self._mutate_control_width, self._mutate_control_height]
        numpy.random.shuffle(mutations)

        for mut in mutations:
            if mut(individual, control):
                break

        return individual

    def _crossover_individuals(self, individual1, individual2):
        """
        Performs a crossover of two individuals. It creates a new individual
        where half of the to-be-optimized properties are taken from individual
        1, and the other half from individual 2.

        Arguments:
        individual1 -- First individual
        individual2 -- Second individual

        Returns:
        New individual
        """

        # The new individual is still a grid layout. Copy the input layout's
        # name and title, but no controls from it.
        new_individual = GridLayoutModel(self._input_layout.name,
                                         self._input_layout.title)

        # Convert the controls of both individuals into dictionaries and
        # merge them into a list of tuples (stackoverflow.com/a/29645538),
        # which is faster than nesting two 'for' loops.
        controls1 = dict([(c.internal_id, c) for c in individual1.controls])
        controls2 = dict([(c.internal_id, c) for c in individual2.controls])
        controls_zip = [
            (c1, controls2[key])
            for (key, c1) in controls1.items()
            if key in controls2
        ]

        # Take horizontal position and span from the first individual, and
        # vertical position and span from the second
        for (c1, c2) in controls_zip:
            c = copy.deepcopy(c1)
            c.rc.v_start = c2.rc.v_start
            c.rc.v_span = c2.rc.v_span
            new_individual.add_control(c)

        return new_individual

    def _calculate_metrics(self, population):
        """
        Calculates the differences between each individual of the population
        (grid layout) and the legacy layout. Lower values are better.

        Arguments:
        population -- Population to calculate metrics for

        Returns:
        List of tuples (individual, (m1, m2, ...)) where m1, m2, ... are the
        results of each metric on the individual
        """

        result = []
        for individual in population:
            result.append((individual, tuple(
                m[0].calculate(self._input_layout, individual, m[1])
                for m in self._metric_info
            )))

        return result

    def _assign_fitnesses(self, population):
        """
        Counts the number of times where a population is prevailed by another.
        """

        # Do we have a population at all?
        if not population:
            return []

        # Verify that all individuals have the same number of metrics.
        # https://stackoverflow.com/a/3844948 is faster than creating a set.
        metric_counts = [len(metric) for (p, metric) in population]
        assert metric_counts.count(metric_counts[0]) == len(metric_counts)

        # We need to compare every individual p against every other individual
        # q for prevalence.
        counts = {}
        for i, (p, metrics_p) in enumerate(population):
            counts[p] = 0
            for j, (q, metrics_q) in enumerate(population):
                if i == j:
                    continue

                # p is prevailed by q if all metrics of q are not larger than p
                if all([y <= x for x, y in zip(metrics_p, metrics_q)]):
                    counts[p] += 1

        # Use the counted prevalences as basis for sorting the population
        return sorted(
            [p for (p, _) in population],
            key=lambda p: counts[p]
        )

    def _create_population(self):
        """
        Creates the initial population for the optimization process. The number
        of individuals in this population is specified by the constant
        config.OPTIMIZE_POPULATION_SIZE.

        Returns:
        The new population
        """

        # Add the input layout as individual if there is any
        if self._population_input is not None:
            population = [self._population_input]
        else:
            population = []

        # Now generate more individuals with much worse properties, namely
        # random coordinates and sizes
        population.extend([
            self._create_individual()
            for i in range(config.OPTIMIZE_POPULATION_SIZE - len(population))
        ])
        return population

    def _evolve_population(self, population):
        """
        Performs the selection, mutation, and crossover operations over the
        current population generation.

        Arguments:
        population -- Current population

        Returns:
        The next population generation
        """

        # Calculate the metrics for all individuals
        population_with_metrics = self._calculate_metrics(population)
        if self.DEBUG:
            for i, (p, m) in enumerate(population_with_metrics):
                print("Individual #%u's metrics:" % (i + 1), m)

        # Assign a fitness value to the individuals. It is already sorted.
        population_by_fitness = self._assign_fitnesses(population_with_metrics)

        # Truncation selection: Fill the mating pool by selecting the first k
        # elements (if necessary, repeatedly).
        truncation_k = int(numpy.floor(
            numpy.divide(len(population_by_fitness), 2)
        ))
        mating_pool = [
            population_by_fitness[i % truncation_k]
            for i in range(0, config.OPTIMIZE_MATING_POOL_SIZE)
        ]

        # Tryout of Neighborhood Cultivation Genetic Algorithm

        # Sort the mating pool against the current objective function. It is
        # rotated among the objectives supplied by the caller.
        current_objective = self._metric_info[
            self._current_iteration % len(self._metric_info)
        ]
        sorted_mates = sorted(mating_pool, key=lambda i: (
            current_objective[0].calculate(
                self._input_layout, i, current_objective[1]
            )
        ))

        # Go through the population. When recombining, only select
        # neighboring individuals.
        new_population = []
        mate_index = 0
        while len(new_population) < config.OPTIMIZE_POPULATION_SIZE:
            added = False
            next_mate = (
                mate_index + 1 if mate_index < len(sorted_mates) - 1 else 0
            )

            # Generate random values
            random_values = numpy.random.random(2)

            # Perform a recombination
            if random_values[0] < config.OPTIMIZE_RECOMBINATION_CHANCE:
                if self.DEBUG:
                    print("Mate #%u: recombining with next" % (mate_index + 1))
                new_population.append(self._crossover_individuals(
                    mating_pool[mate_index],
                    mating_pool[next_mate]
                ))
                added = True

            # Perform a mutation. This can also happen after a
            # recombination.
            # Because the chance of multiple consecutive small random
            # numbers is low, check against the upper range towards 1.
            if random_values[1] > 1.0 - config.OPTIMIZE_MUTATION_CHANCE:
                if self.DEBUG:
                    print("Mate #%u: mutating" % (mate_index + 1))
                new_population.append(self._mutate_individual(
                    copy.deepcopy(mating_pool[mate_index])
                ))
                added = True

            # If not processed, add as-is
            if not added:
                if self.DEBUG:
                    print("Mate #%u: carrying over" % (mate_index + 1))
                new_population.append(mating_pool[mate_index])

            # Go to the next mate
            mate_index = next_mate

        return new_population

    def optimize(self, iterations, silent=False):
        """
        Optimizes the grid layout through mutation so that the difference
        against the original legacy layout is minimized.

        Arguments:
        iterations -- Number of iterations to run the optimizer for

        Return value:
        The optimized grid layout
        """

        #
        # Defend against legacy layouts without any controls. As both input and
        # output are empty, there is nothing to do at this point other than to
        # copy title and identifier.
        #
        if len(self._input_layout.controls) == 0:
            return GridLayoutModel(self._input_layout.name,
                                   self._input_layout.title)

        # Create the population
        population = self._create_population()

        # Try to cache legacy layout measurements before starting
        for i, m in enumerate(self._metric_info):
            m[1] = m[0].precalculate_legacy(self._input_layout)

        # Run the genetic evolution for the specified number of times
        for i in range(iterations):
            if not silent:
                sys.stdout.write("Optimizing %s: iteration %u of %u\r" % (
                    self._input_layout.name, i + 1, iterations))

            self._current_iteration = i
            population = self._evolve_population(population)

        # Add a newline to keep the last iteration print
        if not silent:
            sys.stdout.write('\n')

        # Perform a final selection
        population_with_metrics = self._calculate_metrics(population)

        # Calculate distance of metrics to the null vector
        population_with_distance = [
            (p, numpy.linalg.norm(metrics))
            for (p, metrics) in population_with_metrics
        ]

        # Take the individual with the lowest distance
        chosen = min(population_with_distance, key=lambda x: x[1])
        return chosen[0]
