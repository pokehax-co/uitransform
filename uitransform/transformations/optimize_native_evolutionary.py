#
# Copyright (c) 2018 Thomas Weber <pokehaxco@gmail.com>
#
# SPDX-License-Identifier: MIT
#

#
# This file implements a binding to an evolutionary optimizer implemented in
# native code.
#

import ctypes
import os

from .metrics import metrics_c_mapping, OptimizationMetric
from uitransform.concepts import (
    LegacyLayoutModel, GridLayoutModel, NativeCodeLayout
)


# Handle to libtransform library
_lt = None


class NativeEvolutionaryOptimizer(object):
    """
    Python wrapper for an evolutionary optimizer written in C.
    """

    def __init__(self, metrics, input_layout, population_seed=None):
        """
        Initializes a new instance of the genetic optimization algorithm.

        Arguments:
        metrics -- Metrics to optimize grid layout against. This is a class
        derived from OptimizationMetric, not an instance.
        input_layout -- Legacy layout to work on. It must have at least once
        control in it.
        population_seed (optional) -- A grid layout to add as seed to the
        initial population
        """

        # Must be a single metric or a tuple of metrics
        if isinstance(metrics, tuple):
            for m in metrics:
                if not issubclass(m, OptimizationMetric):
                    raise TypeError(
                        "Metrics are not derived from OptimizationMetric"
                    )
            self._metrics = metrics
        elif issubclass(metrics, OptimizationMetric):
            self._metrics = (metrics,)
        else:
            raise TypeError(
                "Metrics are not derived from OptimizationMetric"
            )

        # Input layout must be a legacy layout
        if not isinstance(input_layout, LegacyLayoutModel):
            raise TypeError("Input layout must be a legacy layout")
        self._input_layout = input_layout

        # If we have a seed, then it must be a grid layout
        self._population_input = None
        if population_seed is not None:
            if isinstance(population_seed, GridLayoutModel):
                self._population_input = population_seed
            else:
                raise TypeError(
                    "Population seed must be a grid layout, or None"
                )

    def optimize(self, iterations, silent=False):
        """
        Optimizes the grid layout through mutation so that the difference
        against the original legacy layout is minimized.

        Arguments:
        iterations -- Number of iterations to run the optimizer for

        Return value:
        The optimized grid layout
        """

        #
        # If not already happened, load the libtransform library
        #
        global _lt
        if _lt is None:
            lt_path = os.path.dirname(__file__) + '/libtransform.so'
            try:
                _lt = ctypes.CDLL(lt_path)
            except OSError as e:
                # Make it user-readable
                raise RuntimeError(
                    "Cannot load libtransform (expected at %s), C optimizer "
                    "is not available" % lt_path
                )

            # Load signature of 'evolutionary_optimizer_run_python'
            _lt.evolutionary_optimizer_run_python.restype = ctypes.POINTER(
                NativeCodeLayout
            )
            _lt.evolutionary_optimizer_run_python.argtypes = (
                ctypes.c_int, ctypes.POINTER(NativeCodeLayout),
                ctypes.POINTER(NativeCodeLayout), ctypes.c_uint, ctypes.c_bool
            )

        #
        # Defend against legacy layouts without any controls. As both input and
        # output are empty, there is nothing to do at this point other than to
        # copy title and identifier.
        #
        if len(self._input_layout.controls) == 0:
            return GridLayoutModel(self._input_layout.name,
                                   self._input_layout.title)

        # Translate metrics to a bitmask
        metrics_integer = 0
        for m in self._metrics:
            if m in metrics_c_mapping:
                metrics_integer |= 1 << metrics_c_mapping[m]
        if metrics_integer == 0:
            raise ValueError("Unknown or no metrics specified")

        # Translate input layout to a bitmask
        input_layout = NativeCodeLayout.convert_to(self._input_layout)
        population_seed = (
            NativeCodeLayout.convert_to(self._population_input)
            if self._population_input else None
        )

        # Perform the optimization in native code
        result = _lt.evolutionary_optimizer_run_python(
            ctypes.c_int(metrics_integer),
            input_layout,
            population_seed,
            ctypes.c_uint(iterations),
            ctypes.c_bool(silent)
        )
        if not result:
            raise RuntimeError("Optimization of layout failed")

        # Translate optimized layout back to a grid layout
        return NativeCodeLayout.convert_from(result.contents, True)
