/*
 * Copyright (c) 2018 Thomas Weber <pokehaxco@gmail.com>
 *
 * SPDX-License-Identifier: MIT
 */

#ifndef OPTIMIZE_EVOLUTIONARY_H
#define OPTIMIZE_EVOLUTIONARY_H

#include "../concepts/layouts.h"

/*
 * Bitmask of available target functions (which are to be minimized)
 */
enum OptimizerTargetFunctionMask
{
    OPTIMIZE_TARGET_FUNCTION_WHITESPACE_RATIO   = (1u << 0),
    OPTIMIZE_TARGET_FUNCTION_DENSITY_HERFINDAHL = (1u << 1),
    OPTIMIZE_TARGET_FUNCTION_DENSITY_GINI       = (1u << 2),
    OPTIMIZE_TARGET_FUNCTION_CONTROL_ORDER      = (1u << 3),
    OPTIMIZE_TARGET_FUNCTION_CONTROL_ALIGNMENT  = (1u << 4),
    OPTIMIZE_TARGET_FUNCTION_CONTROL_SIZE_LOSS  = (1u << 5)
};
#define OPTIMIZE_TARGET_FUNCTION_COUNT 6


#ifdef __WIN32__
    #error FIXME: Windows not yet supported
#else
    /* On Unix, all symbols will be hidden by default, so re-enable it for the
       function exposed to Python */
    #define PUBLIC_API __attribute__ ((visibility ("default")))
#endif


/*
 * This function is called from Python
 */
PUBLIC_API struct Layout *evolutionary_optimizer_run_python(
    enum OptimizerTargetFunctionMask functions, struct Layout *legacy_layout,
    struct Layout *grid_layout_seed, unsigned int iteration_loops,
    bool silent);

#endif
