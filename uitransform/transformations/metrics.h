/*
 * Copyright (c) 2018 Thomas Weber <pokehaxco@gmail.com>
 *
 * SPDX-License-Identifier: MIT
 */

#ifndef TRANSFORMATIONS_METRICS_H
#define TRANSFORMATIONS_METRICS_H

#include "../concepts/layouts.h"


/* All target functions must have this signature */
typedef float (*OptimizerTargetFunction)(struct Layout *grid_layout,
    struct Layout *legacy_layout);


/*
 * Difference between ratios of area occupied by controls to total layout area
 * in both layouts
 */
float uit_xforms_metric_whitespace_ratio(struct Layout *grid_layout,
    struct Layout *legacy_layout);

/*
 * Difference between Gini inequality distributions of whitespace ratio in
 * subdivisions of each layout
 */
float uit_xforms_metric_density_gini(struct Layout *grid_layout,
    struct Layout *legacy_layout);

/*
 * Difference between Herfindahl indices of whitespace ratio in subdivisions
 * of each layout
 */
float uit_xforms_metric_density_herfindahl(struct Layout *grid_layout,
    struct Layout *legacy_layout);

/*
 * Weighted average of Levenshtein distances between several orders of controls
 * in both layouts
 */
float uit_xforms_metric_control_order(struct Layout *grid_layout,
    struct Layout *legacy_layout);

/*
 * Sum of alignment errors between controls with a common alignment in a legacy
 * layout instance
 */
float uit_xforms_metric_control_alignment(struct Layout *grid_layout,
    struct Layout *legacy_layout);

/*
 * Sum of differences between relative control sizes in both layouts, while
 * ignoring controls which are larger in the grid instance than in the legacy
 * one
 */
float uit_xforms_metric_size_loss(struct Layout *grid_layout,
    struct Layout *legacy_layout);

#endif
