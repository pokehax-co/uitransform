/*
 * Copyright (c) 2018 Thomas Weber <pokehaxco@gmail.com>
 *
 * SPDX-License-Identifier: MIT
 */

#include "measurements.h"
#include "../configurables.h"

#include <stdlib.h>
#include <limits.h>
#include <assert.h>


/*
 * Generate the functions which calculate an index from the control's position
 * and the parent's size so that when ordered, the controls are in the same
 * order as if we had iterated each cell. This way, we can reduce the runtime
 * of determining the controls from O(n^2) to O(n + n log n) [assigning indices
 * in linear time, then sorting the array].
 */

typedef unsigned long (*_ControlOrderFunction)(unsigned long pw,
    unsigned long ph, unsigned long cx, unsigned long cy);

#define DEFINE_ORDER_FUNC(name, expr) \
    static unsigned long _order_##name(unsigned long pw, unsigned long ph, \
        unsigned long cx, unsigned long cy) \
    { (void) pw; (void) ph; (void) cx; (void) cy; return (expr); }

DEFINE_ORDER_FUNC(tl_cf,           cx       + cy * pw)
DEFINE_ORDER_FUNC(tl_rf,           cx  * ph + cy)
DEFINE_ORDER_FUNC(tr_cf, (pw - 1 - cx)      + cy * pw)
DEFINE_ORDER_FUNC(tr_rf, (pw - 1 - cx) * ph + cy)
DEFINE_ORDER_FUNC(bl_cf,           cx       + (ph - 1 - cy) * pw)
DEFINE_ORDER_FUNC(bl_rf,           cx  * ph + (ph - 1 - cy))
DEFINE_ORDER_FUNC(br_cf, (pw - 1 - cx)      + (ph - 1 - cy) * pw)
DEFINE_ORDER_FUNC(br_rf, (pw - 1 - cx) * ph + (ph - 1 - cy))


/*
 * Single entry of an order array. Contains the control itself, the index in
 * the iteration (for sorting), and a nested order array if it's a container.
 */
struct _ControlOrder
{
    struct Control *control;
    unsigned long index;
    struct Control **order_within;
};


/*
 * qsort() callback to sort controls after their order index.
 */
static int _cmp_control_order(const void *_l, const void *_r)
{
    const struct _ControlOrder *l = _l;
    const struct _ControlOrder *r = _r;

    /* Don't overflow. https://stackoverflow.com/a/27284248 */
    return (l->index > r->index) - (l->index < r->index);
}


/*
 * Measures the order of controls in a layout according to a particular
 * iteration structure mandated by an order function.
 */
static struct Control **_measurement_control_order_single(
    struct Layout *restrict layout, int expected_parent,
    _ControlOrderFunction order_fn)
{
    struct Control **control_order = NULL;


    /*
     * Choose width and height relevant for all controls in this layer
     */
    bool is_grid = layout_is_grid(layout);
    int parent_h_span, parent_v_span;

    if (expected_parent > -1) {
        struct Control *parent = layout_find_control_by_id(layout,
            expected_parent);
        assert(parent && "can't find parent");

        parent_h_span = is_grid ?
            CONFIG_GRID_LAYOUT_COLUMN_COUNT : parent->rc.h_span;
        parent_v_span = parent->rc.v_span;
    } else {
        parent_h_span = is_grid ?
            CONFIG_GRID_LAYOUT_COLUMN_COUNT : layout->rc.h_span;
        parent_v_span = layout->rc.v_span;
    }

    /*
     * Determine the order of all controls in this layer
     */
    struct _ControlOrder *this_layer_order = calloc(layout->control_count,
        sizeof(*this_layer_order));
    if (!this_layer_order)
        return NULL;

    /* Go through all controls */
    for (unsigned int i = 0; i < layout->control_count; ++i) {
        struct Control *c = &layout->controls[i];

        /* Only for controls with matching parent */
        this_layer_order[i].control = c;
        if (c->parent_control == expected_parent) {
            /* Determine order */
            this_layer_order[i].index = order_fn(
                parent_h_span, parent_v_span, c->rc.h_start, c->rc.v_start
            );

            /* Is this a container? Determine its order recursively */
            if (control_is_container(c)) {
                this_layer_order[i].order_within =
                    _measurement_control_order_single(layout, c->internal_id,
                        order_fn);
                if (!this_layer_order[i].order_within)
                    goto cleanup;
            }
        } else {
            this_layer_order[i].index = ULONG_MAX;
        }
    }

    /* Sort values by increasing index */
    qsort(this_layer_order, layout->control_count,
        sizeof(*this_layer_order), _cmp_control_order);

    /* Restore order */
    control_order = malloc(layout->control_count * sizeof(*control_order));
    if (!control_order)
        goto cleanup;

    unsigned int order_index = 0;
    for (unsigned int i = 0; i < layout->control_count; ++i) {
        /* Skip control without valid index */
        if (this_layer_order[i].index == ULONG_MAX)
            continue;

        /* Insert the current control */
        control_order[order_index++] = this_layer_order[i].control;

        if (!this_layer_order[i].order_within)
            continue;

        /* Copy all controls from the container's order while not changing
           that order because it's sorted properly */
        for (unsigned int j = 0; j < layout->control_count &&
                order_index < layout->control_count; ++j) {
            struct Control *current = this_layer_order[i].order_within[j];
            if (!current)
                break;
            control_order[order_index++] = current;
        }
    }

    /* For containers, fill up unused entries with null pointers */
    while (order_index < layout->control_count)
        control_order[order_index++] = NULL;

cleanup:
    for (unsigned int i = 0; i < layout->control_count; ++i)
        free(this_layer_order[i].order_within);
    free(this_layer_order);
    return control_order;
}


/*
 * Measures the order of controls in a legacy layout instance. It returns 8
 * arrays containing pointers to the controls of the layout as encountered by
 * the iteration orders.
 */
struct Control ***uit_xforms_measure_control_order(
    struct Layout *restrict layout)
{
  struct Control ***orders = calloc(UIT_XFORMS_MEASURE_CONTROL_ORDER_COUNT,
    sizeof(*orders));

  /* There cannot be more than 8 orders anyway, so unroll a loop-would-be */
  if (orders) {
    orders[0] =
      _measurement_control_order_single(layout, -1, _order_tl_cf);
    if (orders[0]) {
      orders[1] =
        _measurement_control_order_single(layout, -1, _order_tl_rf);
      if (orders[1]) {
        orders[2] =
          _measurement_control_order_single(layout, -1, _order_tr_cf);
        if (orders[2]) {
          orders[3] =
            _measurement_control_order_single(layout, -1, _order_tr_rf);
          if (orders[3]) {
            orders[4] =
              _measurement_control_order_single(layout, -1, _order_bl_cf);
            if (orders[4]) {
              orders[5] =
                _measurement_control_order_single(layout, -1, _order_bl_rf);
              if (orders[5]) {
                orders[6] =
                  _measurement_control_order_single(layout, -1, _order_br_cf);
                if (orders[6]) {
                  orders[7] =
                    _measurement_control_order_single(layout, -1, _order_br_rf);
                  if (orders[7])
                    return orders;
                  free(orders[6]);
                }
                free(orders[5]);
              }
              free(orders[4]);
            }
            free(orders[3]);
          }
          free(orders[2]);
        }
        free(orders[1]);
      }
      free(orders[0]);
    }
    free(orders);
  }
  return NULL;
}

/*
 * Frees the data allocated by uit_xforms_measure_control_order().
 */
void uit_xforms_measure_control_order_free(struct Control ***order_array)
{
    free(order_array[0]); free(order_array[1]); free(order_array[2]);
    free(order_array[3]); free(order_array[4]); free(order_array[5]);
    free(order_array[6]); free(order_array[7]); free(order_array);
}
