/*
 * Copyright (c) 2018 Thomas Weber <pokehaxco@gmail.com>
 *
 * SPDX-License-Identifier: MIT
 */

/* #define ENABLE_TIME_TRACKING */

#include "optimize.h"
#include "../configurables.h"
#include "../tools.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <limits.h>
#include <assert.h>

#if defined(ENABLE_TIME_TRACKING) && defined(__linux__)
#include <time.h>
#endif

#include "measurements.h"
#include "metrics.h"


/*
 * Target function mask to function pointer translation table. If the algorithm
 * has to run on 32-bit systems, then there should not be more than 32
 * function pointers.
 */
static const OptimizerTargetFunction _target_functions
        [OPTIMIZE_TARGET_FUNCTION_COUNT] = {
    /* OPTIMIZE_TARGET_FUNCTION_WHITESPACE_RATIO (0x1) */
    uit_xforms_metric_whitespace_ratio,
    /* OPTIMIZE_TARGET_FUNCTION_DENSITY_HERFINDAHL (0x2) */
    uit_xforms_metric_density_herfindahl,
    /* OPTIMIZE_TARGET_FUNCTION_DENSITY_GINI (0x4) */
    uit_xforms_metric_density_gini,
    /* OPTIMIZE_TARGET_FUNCTION_CONTROL_ORDER (0x8) */
    uit_xforms_metric_control_order,
    /* OPTIMIZE_TARGET_FUNCTION_CONTROL_ALIGNMENT (0x10) */
    uit_xforms_metric_control_alignment,
    /* OPTIMIZE_TARGET_FUNCTION_CONTROL_SIZE_LOSS (0x20) */
    uit_xforms_metric_size_loss,
};


/*
 * Member of the population. We store layout instances as individuals, and
 * save the values for all target functions as well as the fitness to allow
 * sorting.
 */
struct PopulationMember
{
    struct Layout *individual;
    float evaluations[OPTIMIZE_TARGET_FUNCTION_COUNT];
    unsigned int prevailed_count;
};


/*
 * Allows mappings from an individual to a floating-point value independently
 * from the PopulationMember structure.
 */
struct PopulationFloat
{
    struct Layout *individual;
    float value;
};


/*
 * All data required during the optimization process
 */
struct EvolutionaryOptimizerContext
{
    /* Caller parameters */
    struct Layout *legacy_layout;
    struct Layout *grid_layout_seed;

    /* Template to derive new individuals from */
    struct Layout *layout_template;

    /* Grid layout dimensions for population generation and mutation */
    struct Rectangle randomize_limits;

    /* Current iteration number */
    unsigned int current_iteration;

    /* Selected target functions */
    unsigned int function_count;
    OptimizerTargetFunction functions[OPTIMIZE_TARGET_FUNCTION_COUNT];

    /* Population on which optimization is performed */
    struct PopulationMember population[CONFIG_OPTIMIZE_POPULATION_SIZE];
};


/*
 * qsort() callback which sorts the population list by a floating-point value
 * in increasing order.
 */
static int evolutionary_optimizer_sort_float(const void *_l,
    const void *_r)
{
    const struct PopulationFloat *l = _l;
    const struct PopulationFloat *r = _r;

    /* Don't overflow. https://stackoverflow.com/a/27284248 */
    return (l->value > r->value) - (l->value < r->value);
}


/*
 * qsort() callbacks to sort a mating pool by a particular target function.
 * There must be as many callbacks as there are functions.
 */
#define GENERATE_MATING_POOL_COMPARATOR(idx) \
    static int _compare_mating_pool_##idx(const void *_l, const void *_r) \
    { \
        const struct PopulationMember *l = \
            *(const struct PopulationMember **) _l; \
        const struct PopulationMember *r = \
            *(const struct PopulationMember **) _r; \
        return (l->evaluations[idx] > r->evaluations[idx]) - \
            (l->evaluations[idx] < r->evaluations[idx]); \
    }
GENERATE_MATING_POOL_COMPARATOR(0)
GENERATE_MATING_POOL_COMPARATOR(1)
GENERATE_MATING_POOL_COMPARATOR(2)
GENERATE_MATING_POOL_COMPARATOR(3)
GENERATE_MATING_POOL_COMPARATOR(4)
GENERATE_MATING_POOL_COMPARATOR(5)

static int (*const _compare_mating_pool[OPTIMIZE_TARGET_FUNCTION_COUNT])
        (const void *, const void *) = {
    _compare_mating_pool_0,
    _compare_mating_pool_1,
    _compare_mating_pool_2,
    _compare_mating_pool_3,
    _compare_mating_pool_4,
    _compare_mating_pool_5,
};


/*
 * qsort() callback which sorts the population list by their prevalence count
 * (fitness value) in increasing order.
 */
static int evolutionary_optimizer_sort_fitnesses(const void *_l,
    const void *_r)
{
    const struct PopulationMember *l = _l;
    const struct PopulationMember *r = _r;

    /* Don't overflow. https://stackoverflow.com/a/27284248 */
    return (l->prevailed_count > r->prevailed_count) -
        (l->prevailed_count < r->prevailed_count);
}


/*
 * Makes a no-string copy of the layout template, with sizes set to the limits
 * stored in the context, and a 'line height' of 0. The result is always a
 * grid layout.
 */
static struct Layout *evolutionary_optimizer_duplicate_template(
    struct EvolutionaryOptimizerContext *restrict context)
{
    struct Layout *new_layout = layout_duplicate_no_strings(
        context->layout_template);
    if (new_layout) {
        new_layout->rc = context->randomize_limits;
        new_layout->line_height = 0;
    }
    return new_layout;
}


/*
 * Randomizes the positions and sizes of controls inside the current layout,
 * respecting the limits of their parents.
 */
static void evolutionary_optimizer_randomize_control_rcs(
    struct EvolutionaryOptimizerContext *restrict context,
    struct Layout *new_layout, int expected_parent)
{
    /*
     * Get parent dimensions (either the layout itself or a container control)
     */
    int v_start, v_span;

    if (expected_parent > -1) {
        struct Control *parent = layout_find_control_by_id(new_layout,
            expected_parent);
        assert(parent && "parent control not in layout");

        v_start = 0;
        v_span = parent->rc.v_span;
    } else {
        v_start = context->randomize_limits.v_start;
        v_span = context->randomize_limits.v_span;
    }

    /* Randomize positions and sizes of all controls with the same parent */
    for (unsigned int i = 0; i < new_layout->control_count; ++i) {
        struct Control *c = &new_layout->controls[i];
        if (c->parent_control != expected_parent)
            continue;

        /* Span minus 1 is the last column of the grid, while the span value
           itself is outside the grid */
        c->rc.h_start = tools_randint(context->randomize_limits.h_start,
            context->randomize_limits.h_span - 1);
        c->rc.v_start = tools_randint(v_start, v_span - 1);

        /* Keep the spans within the limits of the parent rectangle */
        c->rc.h_span = tools_randint(1, context->randomize_limits.h_span -
            c->rc.h_start);
        c->rc.v_span = tools_randint(1, v_span - v_start);

        /* Is this a container? Continue recursively */
        if (control_is_container(c)) {
            evolutionary_optimizer_randomize_control_rcs(context, new_layout,
                c->internal_id);
        }
    }
}


/*
 * Creates a new individual of the population. This is a grid layout based off
 * the input, which entirely random values for position and size.
 */
static struct Layout *evolutionary_optimizer_create_individual(
    struct EvolutionaryOptimizerContext *restrict context)
{
    /* Derive from the template */
    struct Layout *new_layout = evolutionary_optimizer_duplicate_template(
        context);
    if (!new_layout)
        return NULL;

    /* Randomize all positions and sizes */
    evolutionary_optimizer_randomize_control_rcs(context, new_layout, -1);
    return new_layout;
}


/*
 * Creates the initial population for the optimization process. The number of
 * individuals in the population is fixed at CONFIG_OPTIMIZE_POPULATION_SIZE.
 */
static bool evolutionary_optimizer_create_population(
    struct EvolutionaryOptimizerContext *restrict context)
{
    /* Add the seed, if present. Copy it because we might free it randomly */
    if (context->grid_layout_seed) {
        context->population[0].individual = layout_duplicate_no_strings(
            context->grid_layout_seed);
        if (!context->population[0].individual)
            return false;

        /* Copy the layout size back, because the Python side has blanked it */
        context->population[0].individual->rc = context->randomize_limits;
    }

    /* Generate the remaining individuals */
    bool failed = false;
    for (unsigned int i = context->grid_layout_seed ? 1 : 0;
            i < CONFIG_OPTIMIZE_POPULATION_SIZE; ++i) {
        context->population[i].individual =
            evolutionary_optimizer_create_individual(context);
        if (!context->population[i].individual)
            failed = true;
    }
    return !failed;
}


/*
 * Calls each selected target function for all individuals in the population
 * and saves their result.
 */
static void evolutionary_optimizer_evaluate_population(
    struct EvolutionaryOptimizerContext *restrict context)
{
    for (unsigned int i = 0; i < CONFIG_OPTIMIZE_POPULATION_SIZE; ++i) {
        for (unsigned int f = 0; f < context->function_count; ++f) {
            context->population[i].evaluations[f] = context->functions[f](
                context->population[i].individual,
                context->legacy_layout);
        }
    }
}


/*
 * Counts the number of times where a population is prevailed by another.
 */
static void evolutionary_optimizer_assign_fitnesses_and_sort(
    struct EvolutionaryOptimizerContext *restrict context)
{
    for (unsigned int p = 0; p < CONFIG_OPTIMIZE_POPULATION_SIZE; ++p) {
        struct PopulationMember *restrict pp = &context->population[p];

        pp->prevailed_count = 0;
        for (unsigned int q = 0; q < CONFIG_OPTIMIZE_POPULATION_SIZE; ++q) {
            if (p == q)
                continue;

            /* p is prevailed by q if all evaluations of q are not larger than
               those of p */
            unsigned int prevailed_check = 0;
            for (unsigned int i = 0; i < context->function_count; ++i) {
                float eval_q = context->population[q].evaluations[i];
                float eval_p = pp->evaluations[i];
                if (eval_q <= eval_p)
                    ++prevailed_check;
            }
            if (prevailed_check == context->function_count)
                ++pp->prevailed_count;
        }
    }

    /* Sort population by fitness (prevailed_count) */
    qsort(context->population, CONFIG_OPTIMIZE_POPULATION_SIZE,
        sizeof(struct PopulationMember),
        evolutionary_optimizer_sort_fitnesses);
}


/*
 * Recombines two individuals into a new individual taking half of the
 * properties to optimize from both.
 */
static struct Layout *evolutionary_optimizer_recombine(
    const struct Layout *restrict left,
    const struct Layout *restrict right)
{
    /* Copy the left one as basis */
    struct Layout *new_individual = layout_duplicate_no_strings(left);
    if (!new_individual)
        return NULL;

    /* The control order in both layouts is identical. Therefore, just
       overwrite the vertical position and size of all controls with the ones
       specified in the right layout */
    for (unsigned int i = 0; i < new_individual->control_count; ++i) {
        struct Control *cl = &new_individual->controls[i];
        struct Control *cr = &right->controls[i];

        cl->rc.v_start = cr->rc.v_start;
        cl->rc.v_span = cr->rc.v_span;
    }
    return new_individual;
}


/*
 * Flips the horizontal position of a control by subtracting or adding 1.
 */
static bool evolutionary_optimizer_mutate_h_start(struct Control *restrict c)
{
    /* Perform addition or subtraction */
    int new_value = c->rc.h_start + (tools_randbool() ? 1 : -1);

    /* If we leave the interval, abort */
    if (new_value < 0)
        return false;
    if (new_value > CONFIG_GRID_LAYOUT_COLUMN_COUNT - c->rc.h_span)
        return false;

    c->rc.h_start = new_value;
    return true;
}


/*
 * Flips the horizontal size of a control by subtracting or adding 1.
 */
static bool evolutionary_optimizer_mutate_h_span(struct Control *restrict c)
{
    /* Perform addition or subtraction */
    int new_value = c->rc.h_span + (tools_randbool() ? 1 : -1);

    /*
     * In containers, a horizontal overflow of its children can only happen in
     * legacy layouts because in grid layouts, all layouts have the same width
     * (the same number of columns). But as we optimize grid layouts, this will
     * never happen.
     */

    /* If we leave the interval, abort */
    if (new_value < 1)
        return false;
    if (new_value > CONFIG_GRID_LAYOUT_COLUMN_COUNT - c->rc.h_start)
        return false;

    c->rc.h_span = new_value;
    return true;
}


/*
 * Flips the vertical position of a control by subtracting or adding 1.
 */
static bool evolutionary_optimizer_mutate_v_start(
    struct EvolutionaryOptimizerContext *restrict context,
    const struct Layout *restrict layout,
    struct Control *restrict c)
{
    /* Get vertical size of parent */
    int parent_v_span;
    if (c->parent_control > -1) {
        struct Control *parent = layout_find_control_by_id(layout,
            c->parent_control);
        assert(parent && "parent control not in layout");

        parent_v_span = parent->rc.v_span;
    } else {
        parent_v_span = context->randomize_limits.v_span;
    }

    /* Perform addition or subtraction */
    int new_value = c->rc.v_start + (tools_randbool() ? 1 : -1);

    /* If we leave the interval, abort */
    if (new_value < 0)
        return false;
    if (new_value > parent_v_span - c->rc.v_span)
        return false;

    c->rc.v_start = new_value;
    return true;
}


/*
 * Flips the vertical size of a control by subtracting or adding 1.
 */
static bool evolutionary_optimizer_mutate_v_span(
    struct EvolutionaryOptimizerContext *restrict context,
    const struct Layout *restrict layout,
    struct Control *restrict c)
{
    /* Get vertical size of parent */
    int parent_v_span;
    if (c->parent_control > -1) {
        struct Control *parent = layout_find_control_by_id(layout,
            c->parent_control);
        assert(parent && "parent control not in layout");

        parent_v_span = parent->rc.v_span;
    } else {
        parent_v_span = context->randomize_limits.v_span;
    }

    /* Perform addition or subtraction */
    int new_value = c->rc.v_span + (tools_randbool() ? 1 : -1);

    /*
     * If the control is a container, perform an additional check. If the
     * container's children would overflow (because of their coordinates) when
     * decreasing the container's size, block this change.
     */
    if (new_value < c->rc.v_span && control_is_container(c)) {
        int max_v_span = 1;

        /* Determine the row count inside the sublayout */
        for (unsigned int i = 0; i < layout->control_count; ++i) {
            struct Control *child = &layout->controls[i];

            if ((unsigned int)child->parent_control != c->internal_id)
                continue;

            int v = rectangle_v_end(&child->rc);
            if (v > max_v_span)
                max_v_span = v;
        }

        /* Block the attempt */
        if (new_value < max_v_span)
            return false;
    }

    /* If we leave the interval, abort */
    if (new_value < 1)
        return false;
    if (new_value > parent_v_span - c->rc.v_start)
        return false;

    c->rc.v_span = new_value;
    return true;
}


/*
 * Flips a single property of a single control in an individual.
 */
static struct Layout *evolutionary_optimizer_mutate(
    struct EvolutionaryOptimizerContext *restrict context,
    const struct Layout *restrict origin)
{
    /* Copy the individual */
    struct Layout *new_individual = layout_duplicate_no_strings(origin);
    if (!new_individual)
        return NULL;

    /* Select a random control */
    unsigned int control_index = random() % new_individual->control_count;
    struct Control *c = &new_individual->controls[control_index];

    /* Try to perform a property mutation until one succeeds */
    for (unsigned int i = 0, success = 0; !success && i < 10; ++i) {
        switch (random() % 4) {
            case 0:
                success = evolutionary_optimizer_mutate_h_start(c);
                break;
            case 1:
                success = evolutionary_optimizer_mutate_h_span(c);
                break;
            case 2:
                success = evolutionary_optimizer_mutate_v_start(context,
                    new_individual, c);
                break;
            case 3:
                success = evolutionary_optimizer_mutate_v_span(context,
                    new_individual, c);
                break;
        }
    }
    return new_individual;
}


/*
 * Performs the selection, mutation, and crossover operations over the current
 * population generation.
 */
static bool evolutionary_optimizer_evolve_population(
    struct EvolutionaryOptimizerContext *restrict context)
{
    /*
     * Evaluation and fitness determination
     */

    /* Evaluate all individuals */
    evolutionary_optimizer_evaluate_population(context);

    /* Assign fitnesses and sort after them */
    evolutionary_optimizer_assign_fitnesses_and_sort(context);

    /*
     * Truncation selection: Fill the mating pool by selecting the first k
     * elements (if necessary, repeatedly).
     */
    const unsigned int truncation_k = CONFIG_OPTIMIZE_POPULATION_SIZE / 2;
    struct PopulationMember *mating_pool[CONFIG_OPTIMIZE_MATING_POOL_SIZE];

    for (unsigned int i = 0; i < CONFIG_OPTIMIZE_MATING_POOL_SIZE; ++i)
        mating_pool[i] = &context->population[i % truncation_k];

    /*
     * Reproduction through the Neighborhood Cultivation Genetic Algorithm
     */
    unsigned int current_objective = context->current_iteration %
        context->function_count;

    /* qsort_r is a GNU extension, sorry */
    qsort(mating_pool, CONFIG_OPTIMIZE_MATING_POOL_SIZE,
        sizeof(struct PopulationMember *),
        _compare_mating_pool[current_objective]);

    /* Go through the mating pool, and create so many individuals to fill the
       population again */
    struct Layout *new_population[CONFIG_OPTIMIZE_POPULATION_SIZE];
    unsigned int m = 0;
    for (unsigned int p = 0; p < CONFIG_OPTIMIZE_POPULATION_SIZE; ++p) {
        unsigned int m_next =
            m < (CONFIG_OPTIMIZE_MATING_POOL_SIZE - 1) ? m + 1 : 0;

        /* Perform a recombination */
        bool added = false;
        if (tools_random() < CONFIG_OPTIMIZE_RECOMBINATION_CHANCE) {
            new_population[p] = evolutionary_optimizer_recombine(
                mating_pool[m]->individual, mating_pool[m_next]->individual);
            if (!new_population[p])
                return false;

            added = true;
        }

        /* Perform a mutation. This can also happen after a recombination.
           Because the chance of multiple consecutive small random numbers is
           low, check against the upper range towards 1. */
        if (tools_random() > 1.f - CONFIG_OPTIMIZE_MUTATION_CHANCE) {
            new_population[p] = evolutionary_optimizer_mutate(context,
                mating_pool[m]->individual);
            if (!new_population[p])
                return false;

            added = true;
        }

        /* If not processed, add as-is */
        if (!added) {
            new_population[p] = layout_duplicate_no_strings(
                context->population[p].individual);
            if (!new_population[p])
                return false;
        }

        /* Next mate */
        m = m_next;
    }

    /* Overwrite the population. We don't do it in-place as we still want to
       use all old individuals from the mating pool */
    for (unsigned int i = 0; i < CONFIG_OPTIMIZE_POPULATION_SIZE; ++i) {
        layout_free_duplicate_no_strings(context->population[i].individual);
        context->population[i].individual = new_population[i];
    }
    return true;
}


/*
 * After the optimization loop ends, this function selects the individual whose
 * vector of evaluation values has the smallest norm.
 */
struct Layout *evolutionary_optimizer_select_final(
    struct EvolutionaryOptimizerContext *restrict context)
{
    struct PopulationFloat pop_distance[CONFIG_OPTIMIZE_POPULATION_SIZE];

    /* Evaluate all individuals */
    evolutionary_optimizer_evaluate_population(context);

    /* For each individual, calculate the norm of the evaluation value
       (distance to the null vector, the absolute minimum) */
    for (unsigned int p = 0; p < CONFIG_OPTIMIZE_POPULATION_SIZE; ++p) {
        pop_distance[p].individual = context->population[p].individual;
        float distance = 0.f;
        for (unsigned int i = 0; i < context->function_count; ++i) {
            const float v = context->population[p].evaluations[i];
            distance += v * v;
        }
        pop_distance[p].value = sqrtf(distance);
    }

    /* Sort population by distance and return the individual with the lowest
       distance */
    qsort(pop_distance, CONFIG_OPTIMIZE_POPULATION_SIZE,
        sizeof(struct PopulationFloat), evolutionary_optimizer_sort_float);

    return layout_duplicate_no_strings(pop_distance[0].individual);
}


static void evolutionary_optimizer_destroy(
    struct EvolutionaryOptimizerContext *restrict context)
{
    /* Free the population */
    for (unsigned int i = 0; i < CONFIG_OPTIMIZE_POPULATION_SIZE; ++i)
        layout_free_duplicate_no_strings(context->population[i].individual);
}


/*
 * Given a legacy layout, create a grid layout through an evolutionary
 * algorithm which closely resembles the legacy layout from the perspective
 * of one or more metrics.
 *
 * Arguments:
 * functions -- Bitmask of metrics checked during optimization
 * legacy_layout -- Legacy layout which is used as basis for all grid layouts
 * grid_layout_seed -- An initial grid layout derived from the legacy layout
 * which is placed into the initial population
 * iteration_loops -- Number of optimization iterations
 * silent -- If true, no debug prints will be written to standard output.
 * If false and running on Linux, the time required for the optimization
 * process will be measured.
 *
 * Return value:
 * A grid layout with the lowest metric values evolved by the algorithm
 */
PUBLIC_API struct Layout *evolutionary_optimizer_run_python(
    enum OptimizerTargetFunctionMask functions, struct Layout *legacy_layout,
    struct Layout *grid_layout_seed, unsigned int iteration_loops,
    bool silent)
{
    struct Layout *chosen = NULL;
#if defined(ENABLE_TIME_TRACKING) && defined(__linux__)
    struct timespec start, end;
#endif

    /* Initialize PRNG */
    tools_initialize_random();

    /* Initialize the context */
    struct EvolutionaryOptimizerContext context = {
        .legacy_layout = legacy_layout,
        .grid_layout_seed = grid_layout_seed,

        /*
         * Entirely new individuals are derived either from the layout seed
         * (which is expected to be derived from the legacy layout on the
         * Python side) or from the legacy layout.
         */
        .layout_template = grid_layout_seed ?
            grid_layout_seed : legacy_layout
    };

    /* Decode the function bitmask */
    for (unsigned int i = 0; i < OPTIMIZE_TARGET_FUNCTION_COUNT; ++i) {
        if (functions & (1u << i)) {
            context.functions[context.function_count++] =
                _target_functions[i];
        }
    }
    if (context.function_count == 0) {
        fprintf(stderr, "error: no evaluation functions specified\n");
        return NULL;
    }

    /* Determine limits of population randomization (= number of rows and
       columns in the layout). Must use the same derivation as the Python
       code. */
    context.randomize_limits.h_span = CONFIG_GRID_LAYOUT_COLUMN_COUNT;
    context.randomize_limits.v_span = (int) ceilf(
        ((float) legacy_layout->rc.v_span) /
        ((float) legacy_layout->line_height)
    );

#if defined(ENABLE_TIME_TRACKING) && defined(__linux__)
    /* Start measuring performance */
    clock_gettime(CLOCK_MONOTONIC, &start);
#endif

    /* Create the initial population */
    if (!evolutionary_optimizer_create_population(&context))
        goto cleanup;

    /* Run the genetic evolution for the specified number of times */
    for (unsigned int i = 0; i < iteration_loops; ++i) {
        if (!silent) {
            printf("Optimizing %s: iteration %u of %u\r", legacy_layout->name,
                i + 1, iteration_loops);
            fflush(stdout);
        }

        context.current_iteration = i;
        if (!evolutionary_optimizer_evolve_population(&context))
            goto cleanup;
    }
    if (!silent)
        fputc('\n', stdout);

    /* Post-optimization selection of a result */
    chosen = evolutionary_optimizer_select_final(&context);

cleanup:
    /* Clean up */
    evolutionary_optimizer_destroy(&context);

#if defined(ENABLE_TIME_TRACKING) && defined(__linux__)
    /* End measuring performance */
    clock_gettime(CLOCK_MONOTONIC, &end);

    /* Calculate difference */
    if (!silent) {
        double runtime = (double)(end.tv_sec - start.tv_sec) * 1.0e9 +
            (double)(end.tv_nsec - start.tv_nsec);
        printf("Optimization completed in %f nanoseconds (%f seconds)\n",
            runtime, runtime / 1.0e9);
    }
#endif
    return chosen;
}
