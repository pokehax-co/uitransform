/*
 * Copyright (c) 2018 Thomas Weber <pokehaxco@gmail.com>
 *
 * SPDX-License-Identifier: MIT
 */

#ifndef TRANSFORMATIONS_MEASUREMENTS_H
#define TRANSFORMATIONS_MEASUREMENTS_H

#include "../concepts/layouts.h"

/*
 * Control order measurement
 */

/* Number of possible iteration orders */
#define UIT_XFORMS_MEASURE_CONTROL_ORDER_COUNT 8

/*
 * Measures the order of controls in a legacy layout instance. It returns 8
 * arrays containing pointers to the controls of the layout as encountered by
 * the iteration orders.
 */
struct Control ***uit_xforms_measure_control_order(
    struct Layout *restrict layout);

/*
 * Frees the data allocated by uit_xforms_measure_control_order().
 */
void uit_xforms_measure_control_order_free(struct Control ***order_array);

#endif
