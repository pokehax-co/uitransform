#
# Copyright (c) 2017-2018 Thomas Weber <pokehaxco@gmail.com>
#
# SPDX-License-Identifier: MIT
#

import abc
import io
import os


class InputParser(abc.ABC):
    """
    This abstract class provides support for parsing input files. A parser can
    create more than one legacy layout model from a single file. One legacy
    layout (like dialogs or windows) corresponds to one such model.
    """

    # Derived classes must provide a tuple SUPPORTED_FILE_EXTENSIONS of file
    # extensions the parser accepts.

    def __init__(self):
        self._legacy_layout_models = []

    @abc.abstractmethod
    def parse_file_content(self, id_prefix, file_content):
        """
        Parses the content 'file_content' of a source file and generates
        models for each legacy layout found in the source code.

        Arguments:
        id_prefix -- Prefix string which should be added to legacy layout
        identifiers
        source_file -- Content of the source file to parse
        """

        pass

    def parse_file(self, source_file, file_encoding='UTF-8'):
        """
        Parses the source file 'source_file' and generates models for each
        legacy layout found in the source code.

        Arguments:
        source_file -- File name of the source file to parse
        file_encoding -- Text encoding of the source file (UTF-8 is default)
        """

        # Use basename of source file as prefix
        id_prefix = os.path.splitext(os.path.split(source_file)[1])[0] + "_"

        with io.open(source_file, 'r', encoding=file_encoding) as f:
            self.parse_file_content(id_prefix, f.read())

    def get_layout_models(self):
        """
        Returns a tuple of generated legacy layout models. Before calling this
        method, parse a source file using the parse_file() or
        parse_file_content() methods first.
        """

        return tuple(self._legacy_layout_models)
