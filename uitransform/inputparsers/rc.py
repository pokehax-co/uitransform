#
# Copyright (c) 2017-2018 Thomas Weber <pokehaxco@gmail.com>
#
# SPDX-License-Identifier: MIT
#

import itertools
import re

import uitransform.configurables as config

from .common import InputParser
from uitransform.concepts import (
    Rectangle, ControlType, HorizontalAlignment, ButtonContent,
    TextboxContent,
    PositionedControl, LegacyLayoutModel
)
from uitransform.tools import log_warning, log_note


#
# This regular expression matches DIALOG and DIALOGEX blocks in a resource
# script. I did not write this beast; therefore, many thanks to the person who
# developed it.
#
# A typical DIALOG(EX) block has the following syntax:
#     <resource identifier> DIALOG(EX) <X>, <Y>, <Width>, <Height>
#     <option list>
#     {
#        <control statements>
#     }
#
# where <option list> consists of statements like
#     CAPTION "window title"
# or
#     FONT 8, "MS Shell Dlg"
#
# Limitations:
#  - The regular expression r'(?<=^\s*)' had to be removed because it uses
#    a variable-width lookbehind, which only supported by very few regex
#    engines, including .NET's, where this regex was used in.
#  - No legacy 16-bit attributes (MOVEABLE, PURE, DISCARDABLE) are supported.
#  - At least one option has to be speified.
#
_regex_dialog = re.compile(
    r'^'  # replacement for the lookbehind regex
    r'(?P<name>[A-Za-z0-9_]+)\s+DIALOG(EX)?'  # match name and resource type
    r'\s+(?P<x>[0-9]+)\s*,\s*(?P<y>[0-9]+)\s*,'  # match X + Y position
    r'\s*(?P<width>[0-9]+)\s*,\s*(?P<height>[0-9]+)\s+'  # match width + height
    r'(?P<optns>""[^""\\]*(?:\\.[^""\\]*)*""|[^{]+?)^\s*'  # match options
    r'((?P<beginend>BEGIN)|(?P<braces>{))'  # match block open
    r'\s*(?P<body>""[^""\\]*(?:\\.[^""\\]*)*""|[^}]*?)^\s*'  # match content
    r'((END(?(braces)(?!)))|(}(?(beginend)(?!))))',  # match block end
    re.MULTILINE)

#
# This regular expression matches a dialog ID and the DIALOG/DIALOGEX keyword.
# It is only used for finding statements; the statements itself are matched
# using _regex_dialog.
#
_regex_dialog_start = re.compile(r'[A-Za-z0-9_]+\s+DIALOG(EX)?', re.MULTILINE)

#
# This regular expression matches the CAPTION statement of a dialog resource.
#
_regex_dialog_caption = re.compile(r'CAPTION\s+"([^"]*)"', re.MULTILINE)

#
# This regular expression matches a control statement keyword.
#
_regex_dialog_control_keyword = re.compile(
    r'^(?P<keyword>AUTO3STATE|AUTOCHECKBOX|AUTORADIOBUTTON|CHECKBOX|COMBOBOX|'
    r'CONTROL|CTEXT|DEFPUSHBUTTON|EDITTEXT|GROUPBOX|ICON|LISTBOX|LTEXT|'
    r'PUSHBOX|PUSHBUTTON|RADIOBUTTON|RTEXT|SCROLLBAR|STATE3)\s',
    re.MULTILINE)


class ResourceControl(object):
    """
    This class saves the information of a control definition in the dialog
    body. A definition begins with a keyword identifying the declaration type,
    followed by a list of parameters for the current definition. It can be
    translated into a PositionedControl.
    """

    def __init__(self, type_, parameters):
        self.type = type_
        self.parameters = parameters

    def _sanitize_caption_value(self, text):
        # Replace newlines with spaces. Newlines were used to enforce line
        # breaking in a legacy layout.
        text = re.sub(r'\n|\\n', r' ', text, flags=re.M)

        # Remove &, which are used to declare shortcut keys which can be used
        # with the Alt modifier. They don't work in web browsers.
        text = re.sub(r'&(.)', r'\1', text, flags=re.M)

        return text

    def _extract_control_position(self, control_stmt, index):
        """
        Reads 4 parameters which make up the control's rectangle from the
        control statement as 0-indexed position 'index'.
        """

        # We need at least two parameters
        if index + 1 >= len(control_stmt.parameters):
            raise ValueError("Not enough parameters to read control position")

        x = int(control_stmt.parameters[index], 10)
        y = int(control_stmt.parameters[index + 1], 10)

        # Have a size?
        if index + 3 <= len(control_stmt.parameters):
            width = int(control_stmt.parameters[index + 2], 10)
            height = int(control_stmt.parameters[index + 3], 10)
        else:
            width = None
            height = None

        return Rectangle(x, y, width, height)

    def _make_groupbox(self, parameters):
        """
        Creates a group box from the current control statement.

        Arguments:
        parameters -- Parameters of the current control statement (this is not
        necessarily equal to self.parameters)

        Returns:
        A tuple (ctype, class_string, properties)
        """

        class_string = "Group box %s" % parameters[1]
        properties = {
            'captionValue': self._sanitize_caption_value(parameters[0])
        }
        return (ControlType.GROUPBOX, class_string, properties)

    def _make_checkbox(self, parameters, is_tristate, is_disabled=False):
        """
        Creates a checkbox from the current control statement.

        Arguments:
        parameters -- Parameters of the current control statement (this is not
        necessarily equal to self.parameters)
        is_tristate -- Configures whether the checkbox has three states instead
        of two.
        is_disabled -- Configures whether the checkbox can be interacted with.

        Returns:
        A tuple (ctype, class_string, properties)
        """

        class_string = (
            "Tri-state checkbox %s" if is_tristate else "Checkbox %s"
        ) % parameters[1]
        properties = {
            'captionValue': self._sanitize_caption_value(parameters[0]),
            'isTriState': is_tristate,
            'isDisabled': is_disabled
        }
        return (ControlType.CHECKBOX, class_string, properties)

    def _make_radiobutton(self, parameters, is_disabled=False):
        """
        Creates a radio button from the current control statement.

        Arguments:
        parameters -- Parameters of the current control statement (this is not
        necessarily equal to self.parameters)
        is_disabled -- Configures whether the radio button can be interacted
        with.

        Returns:
        A tuple (ctype, class_string, properties)
        """

        class_string = "Radio button %s" % parameters[1]
        properties = {
            'captionValue': self._sanitize_caption_value(parameters[0]),
            'isDisabled': is_disabled
        }
        return (ControlType.RADIOBUTTON, class_string, properties)

    def _make_button(self, parameters, is_default=False, is_disabled=False,
                     button_content=ButtonContent.TEXT):
        """
        Creates a button from the current control statement.

        Arguments:
        parameters -- Parameters of the current control statement (this is not
        necessarily equal to self.parameters)
        is_default -- Configures whether the button is selected when the layout
        is opened.
        is_disabled -- Configures whether the button can be interacted with.
        button_content -- Configures what type of content is displayed inside
        the button.

        Returns:
        A tuple (ctype, class_string, properties)

        Remarks:
        Images cannot be parsed from resource statements; they will be replaced
        by a placeholder image.
        """

        class_string = "Button %s" % parameters[1]
        properties = {
            'captionValue': self._sanitize_caption_value(parameters[0]),
            'isDefault': is_default,
            'isDisabled': is_disabled,
            'buttonContent': button_content
        }
        return (ControlType.BUTTON, class_string, properties)

    def _make_textbox(self, parameters):
        """
        Creates a plaintext control from the current control statement.

        Arguments:
        parameters -- Parameters of the current control statement (this is not
        necessarily equal to self.parameters)

        Returns:
        A tuple (ctype, class_string, properties)
        """

        # If we have 7 parameters, pop the text off
        if len(parameters) == 7:
            text_value = parameters.pop(0)
        else:
            text_value = ""

        # If we have 6 parameters, determine styles
        textbox_content = TextboxContent.TEXT
        is_multiline = False
        is_readonly = False
        if len(parameters) == 6:
            window_styles = [x.strip() for x in parameters[-1].split('|')]

            # Numbers only and password only are also supported
            if "ES_NUMBER" in window_styles:
                textbox_content = TextboxContent.NUMBER
            elif "ES_PASSWORD" in window_styles:
                textbox_content = TextboxContent.PASSWORD

            # Textboxes can also be multiline
            multiline_styles = set(["SS_EDITCONTROL", "ES_MULTILINE"])
            is_multiline = not multiline_styles.isdisjoint(window_styles)

            # And made read-only
            is_readonly = "ES_READONLY" in window_styles

        class_string = "Textbox %s" % parameters[0]
        properties = {
            'textValue': self._sanitize_caption_value(text_value),
            'textboxContent': textbox_content,
            'isMultiline': is_multiline,
            'isDisabled': is_readonly
        }
        return (ControlType.TEXTBOX, class_string, properties)

    def _make_plaintext(self, parameters, alignment):
        """
        Creates a plaintext control from the current control statement.

        Arguments:
        parameters -- Parameters of the current control statement (this is not
        necessarily equal to self.parameters)
        alignment -- Alignment of the text displayed in the control

        Returns:
        A tuple (ctype, class_string, properties)
        """

        class_string = "Plaintext %s" % parameters[1]
        properties = {
            'textValue': self._sanitize_caption_value(parameters[0]),
            'alignment': alignment
        }
        return (ControlType.PLAINTEXT, class_string, properties)

    def _make_image(self, parameters, is_disabled=False, image_content=None):
        """
        Creates an image from the current control statement.

        Arguments:
        parameters -- Parameters of the current control statement (this is not
        necessarily equal to self.parameters)
        is_disabled -- Configures whether the image can be interacted with.
        image_content -- Configures what type of content is displayed inside
        the image.

        Returns:
        A tuple (ctype, class_string, properties)

        Remarks:
        Images cannot be parsed from resource statements; they will be replaced
        by a placeholder image.
        """

        assert image_content in (ButtonContent.BITMAP, ButtonContent.ICON)

        class_string = "Image %s" % parameters[1]
        properties = {
            'captionValue': self._sanitize_caption_value(parameters[0]),
            'isDisabled': is_disabled,
            'buttonContent': image_content
        }
        return (ControlType.IMAGE, class_string, properties)

    def _make_combobox(self, parameters):
        """
        Creates a combobox from the current control statement.

        Arguments:
        parameters -- Parameters of the current control statement in the
        following order: title, id, x, y, width, height[, style[, exstyle]]

        Returns:
        A tuple (ctype, class_string, properties)
        """

        # Have window styles? Parse them
        is_disabled = False
        is_multiline = False
        if len(parameters) >= 7:
            window_styles = [x.strip() for x in parameters[6].split('|')]
            for style in window_styles:
                if style == "WS_DISABLED":
                    is_disabled = True
                elif style == "CBS_DROPDOWNLIST":
                    pass
                elif style == "CBS_DROPDOWN":
                    # FIXME: datalist not supported by Safari/WebKit
                    log_warning("CBS_DROPDOWN is currently unsupported")
                elif style == "CBS_SIMPLE":
                    is_multiline = True
                elif style.startswith("CBS_"):
                    log_warning("unrecognized combobox style " + style)

        class_string = "Combobox %s" % parameters[1]
        properties = {
            'isDisabled': is_disabled,
            'isMultiline': is_multiline
        }
        return (ControlType.COMBOBOX, class_string, properties)

    def _parse_control(self):
        """
        Parses a control statement where the control has been declared using
        the CONTROL keyword.

        Returns:
        A tuple (ctype, class_string, properties)
        """

        # Extract window styles
        window_styles = [x.strip() for x in self.parameters[3].split('|')]

        # Modify parameter list. Remove class name and window styles, and add
        # the window styles at the end.
        modified_parameters = [
            self.parameters[0], self.parameters[1],  # title and ID
            self.parameters[4], self.parameters[5],  # X and Y
            self.parameters[6], self.parameters[7],  # width and height
            self.parameters[3]                       # window style
        ]
        if len(self.parameters) >= 9:
            # Append extended style if present
            modified_parameters.append(self.parameters[8])

        # Create fallback control
        ctype = ControlType.PLACEHOLDER
        properties = {}

        # Use case-independent comparison against class name
        control_class = self.parameters[2].lower()

        # Compute commonly present styles
        is_disabled = "WS_DISABLED" in window_styles

        #
        # The Button class represents many controls through the large number of
        # button styles provided by the Windows API.
        #
        if control_class == "button":
            # Style groups mapping to the same control
            vista_buttons = set([
                'BS_COMMANDLINK',
                'BS_DEFCOMMANDLINK',
                'BS_SPLITBUTTON',
                'BS_DEFSPLITBUTTON'
            ])
            checkboxes_2state = set(["BS_CHECKBOX", "BS_AUTOCHECKBOX"])
            checkboxes_3state = set(["BS_3STATE", "BS_AUTO3STATE"])
            radiobuttons = set(["BS_AUTORADIOBUTTON", "BS_RADIOBUTTON"])

            is_default = any(map(
                lambda x: x.startswith("BS_DEF"), window_styles
            ))

            # Owner-draw controls are not supported
            if "BS_OWNERDRAW" in window_styles:
                # Create fallback control for unrecognized classes
                class_string = "Owner-draw button %s" % self.parameters[1]

            # 3-state checkboxes
            elif not checkboxes_3state.isdisjoint(window_styles):
                ctype, class_string, properties = self._make_checkbox(
                    self.parameters, True, is_disabled)

            # 2-state checkboxes
            elif not checkboxes_2state.isdisjoint(window_styles):
                ctype, class_string, properties = self._make_checkbox(
                    self.parameters, False, is_disabled)

            # Radio buttons
            elif not radiobuttons.isdisjoint(window_styles):
                ctype, class_string, properties = self._make_radiobutton(
                    self.parameters, is_disabled)

            # Bitmap button
            elif "BS_IMAGE" in window_styles:
                ctype, class_string, properties = self._make_button(
                    self.parameters, is_default, is_disabled,
                    button_content=ButtonContent.BITMAP)

            # Map command links and split buttons to standard buttons
            elif not vista_buttons.isdisjoint(window_styles):
                ctype, class_string, properties = self._make_button(
                    self.parameters, is_default, is_disabled)

            # Group box
            elif "BS_GROUPBOX" in window_styles:
                return self._make_groupbox(modified_parameters)

            # Bitmap button
            elif "BS_ICON" in window_styles:
                ctype, class_string, properties = self._make_button(
                    self.parameters, is_default, is_disabled,
                    button_content=ButtonContent.ICON)

            # Normal button
            else:
                ctype, class_string, properties = self._make_button(
                    modified_parameters, is_default, is_disabled)

        #
        # The Static class also represents different controls, but only such
        # which cannot be interacted with. The most popular use of this class
        # is to print plain text.
        #
        elif control_class == "static":
            # Bitmap box
            if "SS_BITMAP" in window_styles:
                ctype, class_string, properties = self._make_image(
                    modified_parameters, is_disabled,
                    image_content=ButtonContent.BITMAP)

            # Text box (intended without botder)
            elif "SS_EDITCONTROL" in window_styles:
                ctype, class_string, properties = self._make_textbox(
                    modified_parameters)

            # Icon box
            elif "SS_ICON" in window_styles:
                ctype, class_string, properties = self._make_image(
                    modified_parameters, is_disabled,
                    image_content=ButtonContent.ICON)

            # Treat everything else as plain text
            else:
                if "SS_RIGHT" in window_styles:
                    alignment = HorizontalAlignment.RIGHT
                elif "SS_CENTER" in window_styles:
                    alignment = HorizontalAlignment.CENTER
                else:
                    alignment = HorizontalAlignment.LEFT

                ctype, class_string, properties = self._make_plaintext(
                    modified_parameters, alignment)

        #
        # The Edit class implements text boxes.
        #
        elif control_class == "edit":
            ctype, class_string, properties = self._make_textbox(
                modified_parameters)

        #
        # The Combobox class implements comboboxes.
        #
        elif control_class == "combobox":
            ctype, class_string, properties = self._make_combobox(
                modified_parameters
            )

        else:
            # Create fallback control for unrecognized classes
            class_string = "Control (class %s) %s" % (self.parameters[2],
                                                      self.parameters[1])

        return (ctype, class_string, properties)

    def parse_statement(self, internal_id_counter):
        """
        Parses a control statement and generates PositionedControl instances
        from them.

        Returns:
        A PositionedControl instance representing the current control
        """

        # Fall back on a placeholder if the statement cannot be understood
        ctype = ControlType.PLACEHOLDER
        properties = {}

        #
        # Three-state check box managed by Windows:
        # AUTO3STATE text, id, x, y, width, height
        #
        if self.type == "AUTO3STATE":
            ctype, class_string, properties = self._make_checkbox(
                self.parameters, True, False)
            id_index = 1
            pos_index = 2

        #
        # Three-state check box managed by the programmer (requiring a custom
        # click event):
        # STATE3 text, id, x, y, width, height
        #
        elif self.type == "STATE3":
            ctype, class_string, properties = self._make_checkbox(
                self.parameters, True, False)
            id_index = 1
            pos_index = 2

        #
        # Two-state check box managed by Windows:
        # AUTOCHECKBOX text, id, x, y, width, height
        #
        elif self.type == "AUTOCHECKBOX":
            ctype, class_string, properties = self._make_checkbox(
                self.parameters, False, False)
            id_index = 1
            pos_index = 2

        #
        # Two-state check box managed by the programmer (requiring a custom
        # click event):
        # CHECKBOX text, id, x, y, width, height
        #
        elif self.type == "CHECKBOX":
            ctype, class_string, properties = self._make_checkbox(
                self.parameters, False, False)
            id_index = 1
            pos_index = 2

        #
        # Radio button managed by Windows:
        # AUTORADIOBUTTON text, id, x, y, width, height
        #
        # Radio button managed by the programmer (requiring a custom click
        # event):
        # RADIOBUTTON text, id, x, y, width, height
        #
        elif self.type in ("AUTORADIOBUTTON", "RADIOBUTTON"):
            ctype, class_string, properties = self._make_radiobutton(
                self.parameters, False)
            id_index = 1
            pos_index = 2

        #
        # A drop-down box:
        # COMBOBOX id, x, y, width, height[, style[, exstyle]]
        #
        elif self.type == "COMBOBOX":
            ctype, class_string, properties = self._make_combobox(
                [""] + self.parameters
            )
            id_index = 0
            pos_index = 1

        #
        # Any control, specified by the combination of window class and window
        # styles, including those which have their own keyword in control
        # statements.
        # CONTROL text, id, class, style, x, y, width, height
        #
        elif self.type == "CONTROL":
            ctype, class_string, properties = self._parse_control()
            id_index = 1
            pos_index = 4

        #
        # Labels displaying plain text. Three statements, one for each
        # horizontal alignment (left, center, right).
        # {C,L,R}TEXT text, id, x, y, width, height
        #
        elif self.type == "CTEXT":
            ctype, class_string, properties = self._make_plaintext(
                self.parameters, HorizontalAlignment.CENTER)
            id_index = 1
            pos_index = 2
        elif self.type == "LTEXT":
            ctype, class_string, properties = self._make_plaintext(
                self.parameters, HorizontalAlignment.LEFT)
            id_index = 1
            pos_index = 2
        elif self.type == "RTEXT":
            ctype, class_string, properties = self._make_plaintext(
                self.parameters, HorizontalAlignment.RIGHT)
            id_index = 1
            pos_index = 2

        #
        # Clickable buttons. Three statements for different behaviors (which
        # we treat the same way to simplify parsing):
        # - PUSHBUTTON creates a normal button.
        # - DEFPUSHBUTTON makes the button the control selected by
        #   default.
        # - PUSHBOX creates a button with text only (no borders).
        # <statement> text, id, x, y, width, height
        #
        elif self.type in ("PUSHBUTTON", "DEFPUSHBUTTON", "PUSHBOX"):
            ctype, class_string, properties = self._make_button(
                self.parameters, self.type == "DEFPUSHBUTTON")
            id_index = 1
            pos_index = 2

        #
        # A text box with border. Can be influenced by additional styles.
        # EDITTEXT id, x, y, width, height [, style]
        #
        elif self.type == "EDITTEXT":
            ctype, class_string, properties = self._make_textbox(
                self.parameters)
            id_index = 0
            pos_index = 1

        #
        # A border box which groups controls visually. During the parsing
        # process, controls visually positioned inside group boxes will be
        # reassigned as the group boxes' children, acting as a container.
        # GROUPBOX text, id, x, y, width, height
        #
        elif self.type == "GROUPBOX":
            ctype, class_string, properties = self._make_groupbox(
                self.parameters)
            id_index = 1
            pos_index = 2

        #
        # A label control which displays an icon stored as Win32 resource
        # instead of text. Width and height are ignored, they are determined by
        # the icon displayed.
        # ICON text, id, x, y
        #
        elif self.type == "ICON":
            ctype, class_string, properties = self._make_image(
                self.parameters, False, image_content=ButtonContent.ICON)
            id_index = 1
            pos_index = 2

        #
        # A single-column list box showing multiple lines of content, with the
        # rest overflowing and accessible via scrolling.
        # LISTBOX id, x, y, width, height
        #
        elif self.type == "LISTBOX":
            # FIXME: parse it
            class_string = "LISTBOX %s" % self.parameters[1]
            id_index = 0
            pos_index = 1

        #
        # A stand-alone scroll bar. These cannot be represented as-is on the
        # Web, therefore added as undefined element.
        # SCROLLBAR id, x, y, width, height
        #
        elif self.type == "SCROLLBAR":
            class_string = "Scroll bar %s" % self.parameters[1]
            id_index = 0
            pos_index = 1

        #
        # Unrecognized control statement, this is actually a syntax error
        # because it has not been defined by Microsoft.
        #
        else:
            raise RuntimeError("unrecognized control statement '%s'" %
                               self.type)

        # Parse position
        rc = self._extract_control_position(self, pos_index)

        #
        # Apply a fix to combo boxes. Their height must be greater than it
        # looks like because any excess height is used for the popup box
        # (which sounds absurd, but was already necessary in the earliest
        # Windows versions). According to the Windows user experience
        # guidelines for desktop applications, the visual bar has a height of
        # 14 dialog units (Vista+) / 10 dialog units (<= 2000).
        #
        if ctype == ControlType.COMBOBOX and \
           not properties.get('isMultiline', False):
            # Use 10 dialog units (I assume that legacy layouts have not been
            # designed deliberately for Vista or newer Windows systems)
            rc.v_span = 10

        # Read control ID
        ident = self.parameters[id_index]

        # Create new control instance. Assign an internal identifier (which is
        # unique through a single file and in turn for each legacy layout in
        # that file) for future purposes.
        # At this stage, we cannot assign a "parent" for now because of the
        # flat hierarchy. It will be reconstructed later.
        return PositionedControl(
            next(internal_id_counter), ident, class_string, rc, None, ctype,
            properties
        )


class RCInputParser(InputParser):
    """
    This class implements a parser for Win32 resource scripts. They have the
    file extension '.rc' and may contain dialog windows, strings, application
    metadata, and graphical resources (bitmaps or icons), for example.

    Controls in a dialog window are positioned using X and Y coordinates and
    have a fixed size. Therefore, parser trees created by this parser cannot be
    output directly into HTML, and must be transformed using an algorithm from
    the original legacy layout into a new grid layout.

    The parser follows a rather primitive approach by making use of regular
    expressions. A real parser preprocesses the script as if it were C code and
    operates on the preprocessed output -- this way, however, resource IDs and
    control attributes will be replaced by numbers whose meaning heavily
    depends on the context, and the goal of this parser is to preserve the
    original IDs so they can be reused in a new codebase.

    The file format is officially documented by Microsoft for use by
    developers (link valid as of 2017-12-07):
    https://msdn.microsoft.com/en-us/library/windows/desktop/aa380599
    """

    SUPPORTED_FILE_EXTENSIONS = (".rc",)

    def __init__(self):
        super().__init__()

        # Create a counter to generate internal IDs
        self._internal_id_counter = itertools.count()

        # Depth tracking
        self._max_depth = None
        self._depth_counter = None

    def _determine_visual_container_relationship(self, parent_id, controls):
        """
        Determines visual relationships between controls in a legacy layout.
        In this parser, it determines which controls belong into a group box.
        Group boxes act as containers which contain their own grid layout in
        the output.

        Arguments:
        FIXME!

        Returns:
        FIXME!
        """

        # Create a shallow copy of the control list, so we can delete controls
        # temporarily while the relationship is determined.
        controls_work = controls[:]
        box_groups = {}

        while True:
            # Find the largest group box which has not been processed yet
            try:
                box = max(
                    [x for x in controls_work
                     if x.ctype == ControlType.GROUPBOX and
                     box_groups.get(x.internal_id, None) is None],
                    key=lambda x: x.rc.h_span * x.rc.v_span
                )
            except ValueError:
                # No more group boxes, stop
                break

            # Go through the control list, and create a list of controls
            # contained in this group box
            box_controls = []
            while True:
                # Find a control Y whose rectangle lies entirely within the
                # current group box X
                for c in controls_work:
                    if box != c and box.rc.contains(c.rc):
                        control_in_box = c
                        break
                else:
                    # No more controls
                    break

                # Remove the current control from the control list copy and
                # add it to the controls contained in the current box. While
                # all controls will remain in the same legacy layout, we need
                # to adjust positions for controls inside group boxes because
                # a group box (or any container control in general) contains
                # a new grid with the same number of columns as a top-level
                # grid.
                box_controls.append(control_in_box)
                controls_work.remove(control_in_box)

                # Make position relative to that of the group box
                control_in_box.rc.h_start -= box.rc.h_start
                control_in_box.rc.v_start -= box.rc.v_start

            # Save the box
            box_groups[box.internal_id] = box_controls

        # Assign all controls which are not part of a group box the identifier
        # of its parent box (unless the parent is the legacy layout itself).
        if parent_id is not None:
            for i, c in enumerate(controls_work):
                # Because the parent is immutable in controls, and we already
                # created it with a None parent, recreate it.
                controls_work[i] = PositionedControl(
                    c.internal_id, c.original_id, c.class_string, c.rc,
                    parent_id, c.ctype, c.properties
                )

        # Iterate recursively on all group boxes determined in the loop above.
        # Extend controls in those groups into controls_work.
        for k in box_groups.keys():
            self._depth_counter += 1
            if self._depth_counter > self._max_depth:
                self._max_depth = self._depth_counter

            controls_work.extend(self._determine_visual_container_relationship(
                                 k, box_groups[k]))

            self._depth_counter -= 1

        # Return the new control list
        return controls_work

    def _process_dialog(self, dialog_id, dialog_caption, dialog_rc,
                        dialog_controls):
        """
        Creates a legacy layout from the dialog information passed to this
        function.

        Arguments:
        dialog_id -- Textual identifier of the dialog (can be a macro like
                     IDD_ABC or a number)
        dialog_caption -- Window title of the dialog
        dialog_rc -- Original dimensions of the dialog, in dialog units
        dialog_controls -- List of control statements found in the dialog body
        """

        #
        # Create a top-level legacy layout. It has a line height of 8 dialog
        # units (which are treated as pixels here, even though they're not
        # equal), which is the per-line height of text labels or other screen
        # text according to "Microsoft Windows User Experience" (1999).
        #
        layout = LegacyLayoutModel(dialog_id, dialog_caption, dialog_rc, 8)

        # Parse the control statements. As all controls are assigned fixed
        # coordinates and sizes, the resulting objects are always instances of
        # PositionedControl, not Control.
        controls = []
        for x in dialog_controls:
            controls.append(x.parse_statement(self._internal_id_counter))

        # FIXME: Handle up-down controls

        # Determine visual associations between group boxes and the controls
        # it "contains".
        self._max_depth = 0
        self._depth_counter = 0
        controls = self._determine_visual_container_relationship(
            None, controls)

        if config.LAYOUT_ANALYSIS_STATISTICS:
            log_note("Depth of layout '{0}': {1}", dialog_id, self._max_depth)
            log_note("Number of controls in layout '{0}': {1}", dialog_id,
                     len(controls))

        # Add all controls to the legacy layout
        for c in controls:
            layout.add_control(c)

        # Add the legacy layout to the results
        self._legacy_layout_models.append(layout)

    def _parse_control_statements(self, dialog_body):
        """
        Parses the dialog body to extract individual controls. A control
        statement begins with a known uppercase type, followed by parameters.
        (Python's regex implementation doesn't allow group reuse and only
        matches the last match when attempting to create a variable number of
        groups, so resort to parsing instead.

        The function acts as a generator, where each new control is yielded.
        The generator stops when the dialog body has been fully parsed.

        Arguments:
        dialog_body -- Content of the current dialog's body

        Yields:
        A ResourceControl instance representing a single control statement
        """

        state = 0
        state_type = ""
        state_params = []
        state_param = ""

        pos = 0
        while pos < len(dialog_body):
            # State 0: Skip whitespace
            if state == 0:
                if not dialog_body[pos].isspace():
                    state = 1
                    continue

            # State 1: Match a control definition keyword, they're all known
            elif state == 1:
                m = _regex_dialog_control_keyword.match(dialog_body[pos:])
                if m:
                    # Extract keyword and continue with parameters
                    state_type = m.group('keyword')
                    pos += len(state_type)
                    state = 2
                    continue
                else:
                    raise RuntimeError("Syntax error in control statements")

            # State 2: Skip whitespace before parsing a parameter
            elif state == 2:
                if not dialog_body[pos].isspace():
                    state = 3
                    continue

            # State 3: Parse a parameter
            elif state == 3:
                if dialog_body[pos] == '"':
                    # Don't add the quote character. Handle it separately.
                    state = 4
                elif dialog_body[pos] == ',':
                    # Current parameter has ended
                    state_params.append(state_param)
                    state_param = ""
                    state = 2
                elif dialog_body[pos].isspace():
                    # Current control might end, perform a check
                    state = 5
                    continue
                else:
                    state_param += dialog_body[pos]

            # State 4: Parse a quoted parameter
            elif state == 4:
                if dialog_body[pos] == '"':
                    # Two consecutive quote characters are used to write a
                    # literal quote. Detect this case, otherwise continue with
                    # treating it as normal parameter
                    if pos + 1 < len(dialog_body) and \
                       dialog_body[pos + 1] == '"':
                        state_param += dialog_body[pos + 1]
                        pos += 1
                    else:
                        state = 3
                else:
                    state_param += dialog_body[pos]

            # State 5: Check for parameter continuation
            elif state == 5:
                # Skip whitespace first
                pos1 = pos
                while pos1 < len(dialog_body):
                    if dialog_body[pos1].isspace():
                        pos1 += 1
                    else:
                        break

                # Try to match a new control definition keyword. If so (or if
                # the input ends), close the current control and begin a new
                # one.
                m = _regex_dialog_control_keyword.match(dialog_body[pos1:])
                if m or pos1 == len(dialog_body):
                    state_params.append(state_param)
                    state_param = ""

                    yield ResourceControl(state_type, state_params)

                    state_type = ""
                    state_params = []
                    state = 0
                elif dialog_body[pos1] == ',':
                    # We got a comma, this is the end of this parameter.
                    state_params.append(state_param)
                    state_param = ""
                    state = 2
                else:
                    # No control definition found, the next word is part of
                    # the current parameter. Preserve the whitespace and
                    # continue as normal parameter (but in append mode).
                    state_param += dialog_body[pos:pos1+1]
                    pos = pos1
                    state = 3

            else:
                raise RuntimeError("unexpected state while parsing controls")

            # Next character
            pos += 1

        # Syntax error if we do not return to state 0
        if state != 0:
            raise RuntimeError("Syntax error in control statements")

        # EOS, we're finished
        return

    def parse_file_content(self, id_prefix, file_content):
        """
        Parses the content 'file_content' of a source file and generates
        models for each legacy layout found in the source code.

        Arguments:
        id_prefix -- Prefix string which should be added to legacy layout
        identifiers
        source_file -- Content of the source file to parse
        """

        # Require non-empty string
        if not isinstance(id_prefix, str) or len(id_prefix) == 0:
            raise ValueError("Non-empty ID prefix required")

        # Find all dialogs in the file
        for start_match in _regex_dialog_start.finditer(file_content):

            # Cut off all text up to this point, and match it against the huge
            # block regex. This way, no variable-width lookbehind is required.
            dialog_content = file_content[start_match.start():]
            dialog_match = _regex_dialog.match(dialog_content)
            if not dialog_match:
                continue

            # From the options, we're only interested in the window title
            dialog_options = dialog_match.group('optns')
            dialog_caption = None
            for match in _regex_dialog_caption.finditer(dialog_options):
                if dialog_caption is None:
                    dialog_caption = match.group(1)
                else:
                    raise RuntimeError('multiple CAPTION options detected')

            # Parse control statements
            dialog_body = dialog_match.group('body')
            dialog_controls = self._parse_control_statements(dialog_body)

            # Extract dialog identifier and dimensions (for informational
            # purposes)
            dialog_id = id_prefix + dialog_match.group('name')
            dialog_rc = Rectangle(
                0,
                0,
                int(dialog_match.group('width'), 10),
                int(dialog_match.group('height'), 10)
            )

            # Process the dialog further
            self._process_dialog(dialog_id, dialog_caption, dialog_rc,
                                 dialog_controls)
