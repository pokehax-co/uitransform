#
# Copyright (c) 2017-2018 Thomas Weber <pokehaxco@gmail.com>
#
# SPDX-License-Identifier: MIT
#

from .rc import RCInputParser  # noqa: F401


# Collect concrete parsers imported in this file
_imported_parsers = [
    klass
    for name, klass in globals().items()
    if name.endswith("InputParser") and name != "InputParser"
]


def get_supported_extensions():
    """
    Returns a list of file extensions supported by any available parser.
    """
    return [
        ext
        for p in _imported_parsers
        for ext in p.SUPPORTED_FILE_EXTENSIONS
    ]


def get_parser_for_extension(file_extension):
    """
    Given a file extension, creates a new instance of a source file parser
    suited for the file's content.

    Arguments:
    file_extension -- File extension of the source file ('.txt', for example)

    Returns:
    Instance of a parser which can understand the file contents
    """

    for p in _imported_parsers:
        if file_extension in p.SUPPORTED_FILE_EXTENSIONS:
            return p()

    raise ValueError("No parser available for files of type %s" %
                     file_extension)
