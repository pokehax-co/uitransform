/*
 * Copyright (c) 2018 Thomas Weber <pokehaxco@gmail.com>
 *
 * SPDX-License-Identifier: MIT
 */

#include "tools.h"

#include <stdlib.h>
#include <time.h>

/*
 * Initializes the random number generator.
 */
void tools_initialize_random(void)
{
#ifdef __unix__
    /* Use PRNG specified in the POSIX standard */
    struct timespec ts;
    unsigned int seed;

    /* SEI CERT C Coding Standard, MSC32-C */
    if (timespec_get(&ts, TIME_UTC) < 0)
        seed = time(NULL);
    else
        seed = ts.tv_sec ^ ts.tv_nsec;
    srandom(seed);
#else
    /* Use C library PRNG */
    srand(time(NULL));
#endif
}

/*
 * Generates a random boolean value (true or false).
 */
bool tools_randbool(void)
{
#ifdef __unix__
    long v = random();
#else
    int v = rand();
#endif
    return (v & 1) == 1;
}

/*
 * Generates a random integer between low and high.
 */
int tools_randint(int low, int high)
{
#ifdef __unix__
    long v = random();
#else
    int v = rand();
#endif
    return v % (high + 1 - low) + low;
}

/*
 * Generates a random floating-point value between 0 and 1.
 */
float tools_random(void)
{
#ifdef __unix__
    long v = random() * 100l + random();
    long l = (RAND_MAX + 1l) * 100l + RAND_MAX;
#else
    int v = rand() * 100 + rand();
    int l = (RAND_MAX + 1) * 100 + RAND_MAX;
#endif
    return ((float) v) / ((float) l);
}
