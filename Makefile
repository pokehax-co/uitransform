#
# Copyright (c) 2018 Thomas Weber <pokehaxco@gmail.com>
#
# SPDX-License-Identifier: MIT
#

#
# Makefile to compile the C optimizer for POSIX-compatible environments, such
# as Linux.
#

CC:=gcc
LD:=gcc

CPPFLAGS:=-D_POSIX_C_SOURCE=200809L -D_XOPEN_SOURCE=700
OPTFLAGS:=-Ofast -flto -s -fPIC -DPIC -pthread -fvisibility=hidden
CFLAGS:=$(CPPFLAGS) $(OPTFLAGS) -Wall -Wextra -std=c11
LDFLAGS:=$(OPTFLAGS) -shared -Wl,-z,defs
LIBS:=-lm

OBJS:=\
    uitransform/concepts/controls.o \
    uitransform/concepts/layouts.o \
    uitransform/transformations/measurements.o \
    uitransform/transformations/metrics.o \
    uitransform/transformations/optimize.o \
    uitransform/tools.o

.PHONY: all clean
.SUFFIXES:

all: uitransform/transformations/libtransform.so
clean:
	rm -f uitransform/transformations/libtransform.so $(OBJS)

uitransform/transformations/libtransform.so: $(OBJS)
	$(LD) $(LDFLAGS) -o $@ $^ $(LIBS)

%.o: %.c
	$(CC) $(CFLAGS) -c -o $@ $<
