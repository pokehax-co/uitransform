#
# Copyright (c) 2018 Thomas Weber <pokehaxco@gmail.com>
#
# SPDX-License-Identifier: MIT
#

import unittest
from uitransform.inputparsers import RCInputParser


class TestRcParsing(unittest.TestCase):
    INPUT = '''
    1 DIALOG 0, 0, 123, 321
    CAPTION "TestRcParsing"
    {
        CONTROL """Special"" Events", -1, STATIC, SS_LEFT | WS_CHILD |
            WS_VISIBLE | WS_GROUP, 11, 22, 33, 44
    }
    '''

    def test(self):
        parser = RCInputParser()
        parser.parse_file_content('TestRcParsing_', self.INPUT)
        layouts = parser.get_layout_models()
        self.assertEqual(len(layouts), 1)
        layout = layouts[0]

        controls = layout.controls
        self.assertEqual(len(controls), 1)

        self.assertEqual(controls[0].properties['textValue'],
            "\"Special\" Events")


if __name__ == '__main__':
    unittest.main()
