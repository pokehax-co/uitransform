#
# Copyright (c) 2018 Thomas Weber <pokehaxco@gmail.com>
#
# SPDX-License-Identifier: MIT
#

import unittest
from uitransform.concepts import ControlType, HorizontalAlignment
from uitransform.inputparsers import RCInputParser


class TestRcStatic(unittest.TestCase):
    INPUT = '''
    1 DIALOG 0, 0, 123, 321
    CAPTION "TestRcStatic"
    {
        CONTROL "C1", 11, STATIC, SS_LEFT, 1, 2, 3, 4
        CONTROL "C2", 12, STATIC, SS_CENTER, 1, 2, 3, 4
        CONTROL "C3", 13, STATIC, SS_RIGHT, 1, 2, 3, 4
        LTEXT "C1", 11, 1, 2, 3, 4
        CTEXT "C2", 12, 1, 2, 3, 4
        RTEXT "C3", 13, 1, 2, 3, 4
    }
    '''

    def test(self):
        parser = RCInputParser()
        parser.parse_file_content('TestRcStatic_', self.INPUT)
        layouts = parser.get_layout_models()
        self.assertEqual(len(layouts), 1)
        layout = layouts[0]

        controls = layout.controls
        self.assertEqual(len(controls), 6)

        self.assertEqual(controls[0].ctype, ControlType.PLAINTEXT)
        self.assertEqual(controls[0].properties['alignment'],
            HorizontalAlignment.LEFT)
        self.assertEqual(controls[1].properties['alignment'],
            HorizontalAlignment.CENTER)
        self.assertEqual(controls[2].properties['alignment'],
            HorizontalAlignment.RIGHT)
        self.assertEqual(controls[3].properties['alignment'],
            HorizontalAlignment.LEFT)
        self.assertEqual(controls[4].properties['alignment'],
            HorizontalAlignment.CENTER)
        self.assertEqual(controls[5].properties['alignment'],
            HorizontalAlignment.RIGHT)


if __name__ == '__main__':
    unittest.main()
