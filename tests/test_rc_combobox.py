#
# Copyright (c) 2018 Thomas Weber <pokehaxco@gmail.com>
#
# SPDX-License-Identifier: MIT
#

import unittest
from uitransform.concepts import ControlType
from uitransform.inputparsers import RCInputParser


class TestRcCombobox(unittest.TestCase):
    def _test_with_input(self, script):
        parser = RCInputParser()
        parser.parse_file_content('TestRcCombobox_', script)
        layouts = parser.get_layout_models()
        self.assertEqual(len(layouts), 1)
        layout = layouts[0]

        controls = layout.controls
        self.assertEqual(len(controls), 3)

        encountered = 3 * [False]
        for c in controls:
            if c.original_id == '1':
                self.assertFalse(encountered[0])
                encountered[0] = True

                self.assertEqual(c.rc.h_start, 1)
                self.assertEqual(c.rc.v_start, 3)
                self.assertEqual(c.rc.h_span, 33)
                self.assertEqual(c.rc.v_span, 27)  # not fixed by parser
                self.assertEqual(c.ctype, ControlType.COMBOBOX)

            elif c.original_id == '2':
                self.assertFalse(encountered[1])
                encountered[1] = True

                self.assertEqual(c.rc.h_start, 36)
                self.assertEqual(c.rc.v_start, 2)
                self.assertEqual(c.rc.h_span, 32)
                self.assertNotEqual(c.rc.v_span, 28)  # fixed by parser
                self.assertEqual(c.ctype, ControlType.COMBOBOX)

            elif c.original_id == '3':
                self.assertFalse(encountered[2])
                encountered[2] = True

                self.assertEqual(c.rc.h_start, 69)
                self.assertEqual(c.rc.v_start, 1)
                self.assertEqual(c.rc.h_span, 31)
                self.assertNotEqual(c.rc.v_span, 29)  # fixed by parser
                self.assertEqual(c.ctype, ControlType.COMBOBOX)

            else:
                self.assertFalse(True)


class TestRcComboboxDirective(TestRcCombobox):
    INPUT = '''
    1 DIALOG 0, 0, 100, 30
    CAPTION "TestRcComboboxVariant1"
    {
        COMBOBOX 1, 1, 3, 33, 27, CBS_SIMPLE | WS_VSCROLL
        COMBOBOX 2, 36, 2, 32, 28, CBS_DROPDOWN | WS_VSCROLL
        COMBOBOX 3, 69, 1, 31, 29, CBS_DROPDOWNLIST | WS_VSCROLL
    }
    '''

    def test(self):
        self._test_with_input(self.INPUT)


class TestRcComboboxDirectiveExStyle(TestRcCombobox):
    INPUT = '''
    1 DIALOG 0, 0, 100, 30
    CAPTION "TestRcComboboxVariant1"
    {
        COMBOBOX 1, 1, 3, 33, 27, CBS_SIMPLE | WS_VSCROLL, 0x00000000
        COMBOBOX 2, 36, 2, 32, 28, CBS_DROPDOWN | WS_VSCROLL, 0x00000000
        COMBOBOX 3, 69, 1, 31, 29, CBS_DROPDOWNLIST | WS_VSCROLL, 0x00000000
    }
    '''

    def test(self):
        self._test_with_input(self.INPUT)


class TestRcComboboxClass(TestRcCombobox):
    INPUT = '''
    1 DIALOG 0, 0, 100, 30
    CAPTION "TestRcComboboxVariant1"
    {
        CONTROL "", 1, "COMBOBOX", CBS_SIMPLE | WS_VSCROLL,
            1, 3, 33, 27
        CONTROL "", 2, "COMBOBOX", CBS_DROPDOWN | WS_VSCROLL,
            36, 2, 32, 28
        CONTROL "", 3, "COMBOBOX", CBS_DROPDOWNLIST | WS_VSCROLL,
            69, 1, 31, 29
    }
    '''

    def test(self):
        self._test_with_input(self.INPUT)


class TestRcComboboxClassExStyle(TestRcCombobox):
    INPUT = '''
    1 DIALOG 0, 0, 100, 30
    CAPTION "TestRcComboboxVariant1"
    {
        CONTROL "", 1, "COMBOBOX", CBS_SIMPLE | WS_VSCROLL,
            1, 3, 33, 27, 0x00000000
        CONTROL "", 2, "COMBOBOX", CBS_DROPDOWN | WS_VSCROLL,
            36, 2, 32, 28, 0x00000000
        CONTROL "", 3, "COMBOBOX", CBS_DROPDOWNLIST | WS_VSCROLL,
            69, 1, 31, 29, 0x00000000
    }
    '''

    def test(self):
        self._test_with_input(self.INPUT)


if __name__ == '__main__':
    unittest.main()
