# UITransform

UITransform is a development tool, written in Python and C, to migrate layouts, such as popup windows from legacy Desktop applications, into responsive web pages through the use of optimization techniques.

The input format supported by UITransform is the Win32 resource script (.rc).

In its current state, UITransform is only intended for demonstration purposes and should not be used in production environments.

UITransform has been tested on all operating systems or distributions for which
specific dependency installation steps have been provided.

# Dependencies

Version numbers are those which were tested; higher versions may work most of the time.

* [Python](https://www.python.org/) 3.6 or [PyPy3.5](http://pypy.org/download.html) 5.10.1
* GCC 7.3
* [SortedContainers](http://www.grantjenks.com/docs/sortedcontainers/) 1.5.9
* [NumPy](http://www.numpy.org/) 1.14
* [Mako](http://www.makotemplates.org/) 1.0.7
* [Levenshtein](https://pypi.python.org/pypi/python-Levenshtein) 0.12.0

Optional dependencies:
* [Beautiful Soup](https://www.crummy.com/software/BeautifulSoup/) 4.6 (to beautify the generated HTML output)
* [html5lib](https://github.com/html5lib/html5lib-python) 1.0.1 (dependency for *Beautiful Soup*)

## Install dependencies from PyPI

```sh
pip install sortedcontainers numpy Mako python-Levenshtein
pip install html5lib beautifulsoup4
```

## Install dependencies on Arch Linux

```sh
pacman -S python python-sortedcontainers python-numpy python-mako python-levenshtein
pacman -S python-html5lib python-beautifulsoup4
```

# Installation and usage

System installation is currently unsupported.

```sh
git clone https://gitlab.com/pokehax-co/uitransform.git UITransform
cd UITransform/
make  # to compile some C code
python -m uitransform
```

# Configurables in the source code

All configurables can be found in the file `uitransform/configurables.py` with detailed descriptions. A subset of these configurables is also present for the C portion of UITransform, see `uitransform/configurables.h` for more details.
