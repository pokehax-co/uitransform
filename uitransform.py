#!/usr/bin/python3
#
# Copyright (c) 2018 Thomas Weber <pokehaxco@gmail.com>
#
# SPDX-License-Identifier: MIT
#

#
# Allows execution using the command './uitransform.py'
#

import os
import sys

from uitransform import UITransform, log_error

# Handle keyboard interrupts in a clean matter.
# https://stackoverflow.com/questions/21120947
try:
    sys.exit(UITransform().main(sys.argv))
except KeyboardInterrupt:
    log_error("program interrupted via Ctrl-C")
    try:
        sys.exit(1)
    except SystemExit:
        os._exit(1)
